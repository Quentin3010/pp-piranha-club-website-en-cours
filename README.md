
# Piranha Club Website

Quentin BERNARD

## Description du projet

REFAIRE

Lien du site : www.piranha-club.fr

## Fonctionnalités

REFAIRE

## Mon rôle

REFAIRE

## DOCUMENT REQUETES API

| **TYPE** | **ROUTER**                               | **DESCRIPTION**                                              | **TOKEN** | 
|----------|------------------------------------------|--------------------------------------------------------------|-----------| 
| GET      | /competition/getAll                      | Récupère toutes les compétitions.                            |           |
| POST     | /groupe/associate                        | Associe un nageur à un groupe et une année.                  |     X     |
| POST     | /groupe/deleteAssociation                | Supprime une association.                                    |     X     |
| GET      | /groupe/getAllYear                       | Récupère toutes les années d'association.                    |           |
| GET      | /groupe/getAllAssociation                | Récupère toutes les associations.                            |           |
| GET      | /login/connect/:login/:mdp               | Vérifie la connection avec un login et un mot de passe.      |           |
| GET      | /loisir/getPhotos/:annee                 | Récupère les photos d'une année spécifique.                  |           |
| POST     | /nageur/add                              | Ajoute un nageur.                                            |     X     |
| POST     | /nageur/delete                           | Supprime un nageur.                                          |     X     |
| POST     | /nageur/update                           | Met à jour un nageur.                                        |     X     |
| GET      | /nageur/getAll/:orderById                | Récupère tous les nageurs, triés par ID.                     |           |
| GET      | /nageur/getAll/:groupe/:annee            | Récupère tous les nageurs d'un groupe pour une année donnée. |           |
| GET      | /nageur/get/:id                          | Récupère un nageur par ID.                                   |           |
| GET      | /nageur/getGroupe/:id                    | Récupère le groupe d'un nageur par ID.                       |           |
| GET      | /nageur/getId/:nom/:prenom               | Récupère l'ID d'un nageur par son nom et prénom.             |           |
| POST     | /news/add                                | Ajoute une nouvelle.                                         |     X     |
| POST     | /news/delete                             | Supprime une nouvelle.                                       |     X     |
| POST     | /news/update                             | Met à jour une nouvelle.                                     |     X     |
| GET      | /news/getPreviews/:X/:Y                  | Récupère les aperçus de X nouvelles en commençant par la Yème. |           |
| GET      | /news/getPreviewsExcept/:X/:Y            | Récupère les aperçus de X nouvelles les plus récentes à l'exception de Y. |           |
| GET      | /news/get/:id                            | Récupère une nouvelle par ID.                                |           |
| GET      | /news/getAll                             | Récupère toutes les nouvelles.                               |           |
| POST     | /performance/add                         | Ajoute une performance.                                      |     X     |
| POST     | /performance/delete                      | Supprime une performance.                                    |     X     |
| POST     | /performance/update                      | Met à jour une performance.                                  |     X     |
| GET      | /performance/get/:id_nageur/:nage/:distance | Récupère une performance d'un nageur par nage et distance. |           |
| GET      | /performance/getAllNbPerformanceByAnnee/:year | Compte le nombre de compétition auquel à participé chaque nageurs pour une année donnée. |           |
| GET      | /performance/getBestByYear/:id_nageur/:year/:nage/:distance | Récupère la meilleure performance par année, nage, distance pour un nageur. |           |
| GET      | /performance/getAll                      | Récupère toutes les performances.                            |           |
| GET      | /records/getRecords/:nage                | Récupère les records par nage (50m, 100m, 200m, 400m, 800m). |           |
| POST     | /resultat/add                            | Ajoute un résultat.                                          |     X     |
| POST     | /resultat/delete                         | Supprime un résultat.                                        |     X     |
| POST     | /resultat/update                         | Met à jour un résultat.                                      |     X     |
| GET      | /resultat/getAll                         | Récupère tous les résultats.                                 |           |
| POST     | /stats/visite/add                        | Ajoute une visite.                                           |     X     |
| GET      | /stats/visite/:date/monthly              | Récupère les visites mensuelles à partir d'une date.         |     X     |
| GET      | /stats/visite/:date/yearly               | Récupère les visites annuelles à partir d'une date.          |     X     |
| GET      | /stats/visite/:date/page/:page           | Récupère les visites mensuelle d'une page à partir d'une date. |     X     |
| GET      | /stats/visite/profils                    | Récupère le nombre de visite de chaque profil.               |       X      |
| GET      | /stats/visite/news                       | Récupère le nombre de visite de chaque nouvelles.            |       X      |
| GET      | /stats/visite/qrcode                     | Récupère le nombre de visite depuis le QR code.              |       X      |
| GET      | /stats/visite/facebook                   | Récupère le nombre de visite provenant de Facebook.          |       X      |
| GET      | /stats/visite                            | Récupère toutes les visites uniques.                         |     X     |