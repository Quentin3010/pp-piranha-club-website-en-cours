Bien sûr, je peux te fournir un exemple simplifié pour configurer Apache avec deux noms de domaine pointant vers la même IP sur ton VPS. Garde à l'esprit que les chemins de fichiers et les noms de domaine utilisés dans l'exemple sont fictifs et doivent être adaptés à ta configuration réelle. Voici comment procéder :

## Étape 1 : Configuration des Virtual Hosts

Connecte-toi à ton VPS via SSH.

Ouvre le fichier de configuration d'Apache pour les virtual hosts. Pour Apache2, cela pourrait être :
```
sudo nano /etc/apache2/sites-available/000-default.conf
```

Ajoute les blocs VirtualHost pour chaque nom de domaine que tu souhaites configurer. Par exemple :
```
<VirtualHost *:80>
    ServerName premier-domaine.com
    DocumentRoot /var/www/premier-domaine
</VirtualHost>

<VirtualHost *:80>
    ServerName deuxieme-domaine.com
    DocumentRoot /var/www/deuxieme-domaine
</VirtualHost>
```

Crée les répertoires pour chaque site :
```
sudo mkdir /var/www/premier-domaine
sudo mkdir /var/www/deuxieme-domaine
```

Assure-toi que les permissions sont correctes pour les répertoires :
```
sudo chown -R www-data:www-data /var/www/premier-domaine
sudo chown -R www-data:www-data /var/www/deuxieme-domaine
```

## Étape 2 : Configuration de DNS

Accède à ton compte de gestion de domaines (OVH ou tout autre fournisseur) et crée deux enregistrements de type A qui pointent vers l'adresse IP de ton VPS pour chaque domaine (premier-domaine.com et deuxieme-domaine.com).

Voici comment tu peux configurer les enregistrements DNS pour ton nom de domaine fraîchement acheté sur OVH :

Connecte-toi à ton compte OVH : Accède à ton espace client OVH en utilisant tes identifiants.

Accède à la gestion de ton domaine : Une fois connecté, cherche la section "Domaines" ou "Mes domaines" et sélectionne le domaine que tu viens d'acheter.

Gestion des DNS : Cherche une option comme "Zone DNS" ou "Gestion des DNS". C'est ici que tu pourras configurer les enregistrements DNS pour ton domaine.

Ajoute un enregistrement A : Dans la zone DNS, cherche l'option pour ajouter un enregistrement de type A. Un enregistrement de type A associe un nom de domaine à une adresse IP. Tu devras entrer l'adresse IP de ton VPS.
```
    Type : A
    Nom : (laisse vide pour le domaine principal, ou entre "www" pour le sous-domaine www)
    Cible : L'adresse IP de ton VPS (par exemple, 193.70.40.178)
    TTL : Laisse la valeur par défaut ou ajuste selon tes besoins
```

Enregistre : Une fois que tu as rempli les champs nécessaires, enregistre l'enregistrement DNS.

Redirige le sous-domaine www (si nécessaire) : Si tu veux également que ton domaine soit accessible via "www.ton-domaine.com", ajoute un deuxième enregistrement A pour le sous-domaine www avec la même adresse IP.

C'est à peu près tout ! Une fois les enregistrements DNS configurés, ils dirigeront les requêtes vers ton domaine vers l'adresse IP de ton VPS. Garde à l'esprit que les changements dans les enregistrements DNS peuvent prendre un certain temps pour se propager à travers l'Internet, donc il se peut que ton domaine ne pointe pas immédiatement vers ton VPS.

## Étape 3 : Redémarrage et vérification

Redémarre Apache pour appliquer les changements :
```
sudo service apache2 restart
```

Vérifie que tes virtual hosts sont actifs en utilisant :
```
sudo apache2ctl -S
```

Tu devrais voir une liste de tous les virtual hosts actifs.

Assure-toi que les noms de domaine pointent vers l'IP de ton VPS en utilisant la commande ping ou en visitant les sites via les navigateurs.

Site pour vérifier la propagation du DNS : https://dnschecker.org/#A/www.piranha-club.fr/193.70.40.178