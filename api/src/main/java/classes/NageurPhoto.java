package classes;

public class NageurPhoto {
	public int id_nageur;
	public String photo;
	
	public NageurPhoto() {
    }
	
	public NageurPhoto(int id_nageur, String photo) {
		this.id_nageur = id_nageur;
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "NageurPhoto [id_nageur=" + id_nageur + ", photo=" + photo + "]";
	}
	
	
	
}
