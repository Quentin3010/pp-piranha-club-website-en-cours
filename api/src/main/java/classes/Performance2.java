package classes;

public class Performance2 {
	public int id_performance;
	
	public String nom_complet;
	public String sexe;
	public String categorie_nageur;
	
	public String nage;
	public int distance;
	
	public String temps;
	public String temps_50m;
	public String temps_100m;

	public String date_competition;
	public String type_competition;
	
	public String lien_video;
	
	public Performance2() {
	}

	public Performance2(int id_performance, String nom_complet, String sexe, String categorie_nageur, String nage, int distance,
			String temps, String temps_50m, String temps_100m, String date_competition, String type_competition,
			String lien_video) {
		super();
		this.id_performance = id_performance;
		this.nom_complet = nom_complet;
		this.sexe = sexe;
		this.categorie_nageur = categorie_nageur;
		this.nage = nage;
		this.distance = distance;
		this.temps = temps;
		this.temps_50m = temps_50m;
		this.temps_100m = temps_100m;
		this.date_competition = date_competition;
		this.type_competition = type_competition;
		this.lien_video = lien_video;
	}
	

}
