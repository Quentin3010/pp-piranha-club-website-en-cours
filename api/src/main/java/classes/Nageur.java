package classes;

public class Nageur {
	public int id_nageur;
	public String nom;
	public String prenom;
	public String date_naiss;
	public String sexe;
	public String photo;
	
	public Nageur() {
    }
	
	public Nageur(int id_nageur, String nom, String prenom, String date_naiss, String sexe, String photo) {
		this.id_nageur = id_nageur;
		this.nom = nom;
		this.prenom = prenom;
		this.date_naiss = date_naiss;
		this.sexe = sexe;
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "Nageur [id_nageur=" + id_nageur + ", nom=" + nom + ", prenom=" + prenom + ", date_naiss=" + date_naiss
				+ ", sexe=" + sexe + ", photo=" + photo + "]";
	}
	
	
}
