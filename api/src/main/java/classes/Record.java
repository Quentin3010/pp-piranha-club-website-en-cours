package classes;

public class Record {
	public int id_nageur;
	public String prenom;
	public String nom;
	public int annee;
	public String temps;
	
	public Record(int id_nageur, String prenom, String nom, int annee, String temps) {
		this.id_nageur = id_nageur;
		this.prenom = prenom;
		this.nom = nom;
		this.annee = annee;
		this.temps = temps;
	}

	@Override
	public String toString() {
		return "Record [id_nageur=" + id_nageur + ", prenom=" + prenom + ", nom=" + nom + ", annee=" + annee
				+ ", temps=" + temps + "]";
	}
	
	
}
