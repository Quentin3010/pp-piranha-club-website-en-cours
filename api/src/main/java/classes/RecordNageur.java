package classes;

public class RecordNageur {
	public int id_nageur;
	public int id_performance;
	public int distance;
	public String nage;
	public String temps;
	public String date_competition;
	
	public RecordNageur() {
    }
	
	public RecordNageur(int id_nageur, int id_performance, int distance, String nage, String temps, String date_competition) {
		this.id_nageur = id_nageur;
		this.id_performance = id_performance;
		this.distance = distance;
		this.nage = nage;
		this.temps = temps;
		this.date_competition = date_competition;
	}

	@Override
	public String toString() {
		return "RecordNageur [id_nageur=" + id_nageur + ", id_performance=" + id_performance + ", distance=" + distance
				+ ", nage=" + nage + ", temps=" + temps + ", date_competition=" + date_competition + "]";
	}
}
