package classes;

public class StatCountVisite {
	public String date;
	public String page;
	public int visite_unique;
	public int visite_totale;
	
	public StatCountVisite() {
	}

	public StatCountVisite(String date, String page, int visite_unique, int visite_totale) {
		this.date = date;
		this.page = page;
		this.visite_unique = visite_unique;
		this.visite_totale = visite_totale;
	}
}
