package classes;

public class AssociationNageurGroupe {
	public int id_association;
	public String nom_groupe;
	public int id_nageur;
	public int annee;
	
	public AssociationNageurGroupe() {
		
	}

	public AssociationNageurGroupe(int id_association, String nom_groupe, int id_nageur, int annee) {
		super();
		this.id_association = id_association;
		this.nom_groupe = nom_groupe;
		this.id_nageur = id_nageur;
		this.annee = annee;
	}
}
