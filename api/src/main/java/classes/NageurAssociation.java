package classes;

public class NageurAssociation {
	public int id_association;
	public String nom;
	public String prenom;
	public String nom_groupe;
	public int annee;
	
	public NageurAssociation() {
	}
	
	public NageurAssociation(int id_association, String nom, String prenom, String nom_groupe, int annee) {
		super();
		this.id_association = id_association;
		this.nom = nom;
		this.prenom = prenom;
		this.nom_groupe = nom_groupe;
		this.annee = annee;
	}
}
