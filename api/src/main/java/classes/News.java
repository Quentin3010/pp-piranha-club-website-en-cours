package classes;

public class News {
	public int id_news;
	public String date_news;
	public String titre;
	public String message;
	public String miniature;
	public String photos;
	
	public News() {
	}
	
	public News(int id_news, String date_news, String titre, String message, String miniature, String photos) {
		this.id_news = id_news;
		this.date_news = date_news;
		this.titre = titre;
		this.message = message;
		this.miniature = miniature;
		this.photos = photos;
	}
}
