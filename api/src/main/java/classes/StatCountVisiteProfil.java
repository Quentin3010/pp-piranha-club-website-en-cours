package classes;

public class StatCountVisiteProfil {
	public int id_nageur;
	public String nom;
	public String prenom;
	public int unique;
	public int totale;
	
	public StatCountVisiteProfil() {
	}
	
	public StatCountVisiteProfil(int id_nageur, String nom, String prenom, int unique, int totale) {
		this.id_nageur = id_nageur;
		this.nom = nom;
		this.prenom = prenom;
		this.unique = unique;
		this.totale = totale;
	}
}
