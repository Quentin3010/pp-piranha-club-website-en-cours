package classes;

public class NageurCarte {
	public int id_nageur;
	public String date_naiss;
	public String nom;
	public String prenom;
	public String photo;
	
	public NageurCarte() {
	}
	
	public NageurCarte(int id_nageur, String date_naiss, String nom, String prenom, String photo) {
		this.id_nageur = id_nageur;
		this.date_naiss = date_naiss;
		this.nom = nom;
		this.prenom = prenom;
		this.photo = photo;
	}
}
