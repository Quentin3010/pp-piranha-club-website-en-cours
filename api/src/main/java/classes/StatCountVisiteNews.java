package classes;

public class StatCountVisiteNews {
	public int id_news;
	public String miniature;
	public String titre;
	public int unique;
	public int totale;
	
	public StatCountVisiteNews() {
	}
	
	public StatCountVisiteNews(int id_news, String miniature, String titre, int unique, int totale) {
		this.id_news = id_news;
		this.miniature = miniature;
		this.titre = titre;
		this.unique = unique;
		this.totale = totale;
	}
}
