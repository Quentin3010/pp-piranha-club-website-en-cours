package classes;

public class Resultat {
	public int id_resultat;
	public String type_competition;
	public String date_competition;
	public String lien_telechargement;
	public String lien_telechargement_npdc;
	
	public Resultat() {
	}
	
	public Resultat(int id_resultat, String type_competition, String date_competition, String lien_telechargement, String lien_telechargement_npdc) {
		this.id_resultat = id_resultat;
		this.type_competition = type_competition;
		this.date_competition = date_competition;
		this.lien_telechargement = lien_telechargement;
		this.lien_telechargement_npdc = lien_telechargement_npdc;
	}
}
