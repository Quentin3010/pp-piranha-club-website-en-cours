package classes;

public class NbPerfByAnnee {
	public int id_nageur;
	public int annee_naiss;
	public String prenom;
	public String nom;
	public String nom_groupe;
	public int nbPerf;
	
	public NbPerfByAnnee() {}

	public NbPerfByAnnee(int id_nageur, int annee_naiss, String prenom, String nom, String nom_groupe, int nbPerf) {
		super();
		this.id_nageur = id_nageur;
		this.annee_naiss = annee_naiss;
		this.prenom = prenom;
		this.nom = nom;
		this.nom_groupe = nom_groupe;
		this.nbPerf = nbPerf;
	}
}
