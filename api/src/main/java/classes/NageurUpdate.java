package classes;

public class NageurUpdate {
	public int id_nageur;
	public String nom;
	public String prenom;
	public String date_naiss;
	public String sexe;
	
	public NageurUpdate() {
    }
	
	public NageurUpdate(int id_nageur, String nom, String prenom, String date_naiss, String sexe) {
		this.id_nageur = id_nageur;
		this.nom = nom;
		this.prenom = prenom;
		this.date_naiss = date_naiss;
		this.sexe = sexe;
	}

	@Override
	public String toString() {
		return "Nageur [id_nageur=" + id_nageur + ", nom=" + nom + ", prenom=" + prenom + ", date_naiss=" + date_naiss
				+ ", sexe=" + sexe + "]";
	}
	
	
}
