package me.jesuismister.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import classes.News;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.OPTIONS;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import me.jesuismister.main.Main;
import me.jesuismister.sql.DS;

@Produces("application/json")
@Path("news")
public class NewsRessource {
	private static final int MAX_PREVIEX = 200;

	@GET
	@Path("getPreviews/{nb}/{offset}")
	public Response getNewsPreviews(@PathParam("nb") int nb, @PathParam("offset") int offset) {
		List<News> res = new ArrayList<News>();

		String requete = "SELECT * FROM News ORDER BY id_news DESC LIMIT ? OFFSET ?";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, nb);
			ps.setInt(2, offset);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new News(rs.getInt("id_news"), rs.getString("date_news"), rs.getString("titre"), limiterChaine(rs.getString("message")), rs.getString("miniature"), rs.getString("photos")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("getPreviews")
	public Response corsForGetNewsPreviews() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}

	public String limiterChaine(String chaine) {
		if(chaine.length()<MAX_PREVIEX) {
			return chaine;
		}else {
			return chaine.substring(0, MAX_PREVIEX) + "...";
		}
	}
	
	@GET
	@Path("getPreviewsExcept/{nb}/{exception}")
	public Response getNewsPreviewsExcept(@PathParam("nb") int nb, @PathParam("exception") int exception) {
		List<News> res = new ArrayList<News>();

		String requete = "SELECT * FROM News WHERE id_news != ? ORDER BY id_news DESC LIMIT ?";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, exception);
			ps.setInt(2, nb);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new News(rs.getInt("id_news"), rs.getString("date_news"), rs.getString("titre"), limiterChaine(rs.getString("message")), rs.getString("miniature"), rs.getString("photos")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("getPreviewsExcept")
	public Response corsForGetNewsPreviewsExcept() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@GET
	@Path("get/{id}")
	public Response getNews(@PathParam("id") int id) {
		List<News> res = new ArrayList<News>();

		String requete = "SELECT * FROM News WHERE id_news = ?";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			rs.next();
			res.add(new News(rs.getInt("id_news"), rs.getString("date_news"), rs.getString("titre"), rs.getString("message"), rs.getString("miniature"), rs.getString("photos")));
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("get")
	public Response corsForGetNews() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@GET
	@Path("getAll")
	public Response getAll() {
		List<News> res = new ArrayList<News>();

		String requete = "SELECT * FROM News ORDER BY date_news DESC, id_news DESC;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new News(rs.getInt("id_news"), rs.getString("date_news"), rs.getString("titre"), rs.getString("message"), rs.getString("miniature"), rs.getString("photos")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("getAll")
	public Response corsForGetAll() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@POST
	@Path("add")
	@Consumes("application/json")
	public Response addNews(News news) {
		String requete = "INSERT INTO News VALUES (DEFAULT, ?, ?, ?, ?, ?);";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = con.prepareStatement(requete);
			ps.setString(1, news.date_news);
			ps.setString(2, news.titre);
			ps.setString(3, news.message);
			ps.setString(4, news.miniature);
			ps.setString(5, news.photos);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info(e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("add")
	public Response corsForAddNews() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@POST
	@Path("delete")
	public Response delete(int id) {
		String requete = "DELETE FROM News WHERE id_news = ?";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, id);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return Response.status(Response.Status.ACCEPTED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("delete")
	public Response corsForDeleteNews() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@POST
	@Path("update")
	@Consumes("application/json")
	public Response updateNageur(News news) {
		String requete = "UPDATE News SET date_news = ?, titre = ?, message = ?, miniature = ?, photos = ? WHERE id_news = ?;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = con.prepareStatement(requete);
			ps.setString(1, news.date_news);
			ps.setString(2, news.titre);
			ps.setString(3, news.message);
			ps.setString(4, news.miniature);
			ps.setString(5, news.photos);
			ps.setInt(6, news.id_news);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("update")
	public Response corsForUpdateNews() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
}
