package me.jesuismister.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import classes.Resultat;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.OPTIONS;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import me.jesuismister.main.Main;
import me.jesuismister.sql.DS;

@Produces("application/json")
@Path("resultat")
public class ResultatRessource {

	@GET
	@Path("getAll")
	public Response getAllYear() {
		List<Resultat> res = new ArrayList<Resultat>();

		String requete = "SELECT id_resultat, type_competition, date_competition, lien_telechargement, lien_telechargement_npdc FROM Resultat ORDER BY date_competition DESC; ";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new Resultat(rs.getInt("id_resultat"), rs.getString("type_competition"), rs.getString("date_competition"), rs.getString("lien_telechargement"), rs.getString("lien_telechargement_npdc")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@POST
	@Path("add")
	@Consumes("application/json")
	public Response addResultat(Resultat resultat) {
		String requete = "INSERT INTO Resultat VALUES (DEFAULT, ?, ?, ?, ?);";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = con.prepareStatement(requete);
			ps.setString(1, resultat.type_competition);
			ps.setString(2, resultat.date_competition);
			ps.setString(3, resultat.lien_telechargement);
			ps.setString(4, resultat.lien_telechargement_npdc);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info(e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Path("add")
	public Response corsForAddResultat() {
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@POST
	@Path("delete")
	public Response delete(int id) {
		String requete = "DELETE FROM Resultat WHERE id_resultat = ?";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, id);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return Response.status(Response.Status.ACCEPTED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("delete")
	public Response corsForDeleteNageur() {
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@POST
	@Path("update")
	@Consumes("application/json")
	public Response updateNageur(Resultat resultat) {
		String requete = "UPDATE Resultat SET type_competition = ?, date_competition = ?, lien_telechargement = ?, lien_telechargement_npdc = ? WHERE id_resultat = ?;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = con.prepareStatement(requete);
			ps.setString(1, resultat.type_competition);
			ps.setString(2, resultat.date_competition);
			ps.setString(3, resultat.lien_telechargement);
			ps.setString(4, resultat.lien_telechargement_npdc);
			ps.setInt(5, resultat.id_resultat);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("update")
	public Response corsForUpdateNageur() {
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
}
