package me.jesuismister.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import me.jesuismister.main.Main;
import me.jesuismister.sql.DS;
import classes.Record;

@Produces("application/json")
@Path("records")
public class RecordRessource {
	private static final List<String> sexes = List.of("Homme", "Femme");
	private static final List<String> categories = List.of("Mini-Poussin", "Poussin", "Benjamin", "Minime", "Cadet", "Junior", "Senior", "Master");
	
	@GET
	@Path("getRecords/{nage}")
	public Response getRecordsNage(@PathParam("nage") String nage) {
		Map<String, Map<String, Map<Integer, Record>>> res = new HashMap<String, Map<String, Map<Integer, Record>>>();
		for(String sexe : sexes) {
			res.put(sexe, new HashMap<String, Map<Integer, Record>>());
			for(String cat : categories) {
				res.get(sexe).put(cat, new HashMap<Integer, Record>());
			}
		}
		
		String requete = "SELECT t.sexe, t.id_nageur, t.nom, t.prenom, t.categorie_nageur, t.annee, t.distance, t.nage, MIN(t.temps) AS meilleur_temps FROM (SELECT n.sexe, n.id_nageur, n.nom, n.prenom, p.id_performance, year(p.date_competition) as annee, p.categorie_nageur, p.distance, p.nage, p.temps, ROW_NUMBER() OVER (PARTITION BY n.sexe, p.categorie_nageur, p.distance, p.nage ORDER BY p.temps) AS row_num FROM Performance AS p LEFT JOIN Nageur AS n ON p.id_nageur = n.id_nageur WHERE p.nage = ?) AS t WHERE t.row_num = 1 GROUP BY t.sexe, t.categorie_nageur, t.distance, t.nage ORDER BY t.sexe, t.categorie_nageur, t.distance, t.nage;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setString(1, nage.replace("_", " "));
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Record r = new Record(rs.getInt("id_nageur"), rs.getString("prenom"), rs.getString("nom"),rs.getInt("annee"), rs.getString("meilleur_temps"));
				res.get(rs.getString("sexe")).get(rs.getString("categorie_nageur")).put(rs.getInt("distance"), r);
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			Main.LOGGER.info(e.getMessage());
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
}
