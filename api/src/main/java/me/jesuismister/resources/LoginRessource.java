package me.jesuismister.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import me.jesuismister.main.Main;
import me.jesuismister.sql.DS;

@Produces("application/json")
@Path("login")
public class LoginRessource {

	@GET
	@Path("{login}/{mdp}")
	public Response getNewsPreviews(@PathParam("login") String login, @PathParam("mdp") String mdp) {
		List<Boolean> res = new ArrayList<Boolean>();

		String requete = "SELECT mdp FROM Login WHERE login = ?";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setString(1, login.hashCode()+"");
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				res.add(mdp.hashCode() == rs.getInt("mdp"));
			}else {
				res.add(false);
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
}
