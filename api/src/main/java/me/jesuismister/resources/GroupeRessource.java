package me.jesuismister.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import classes.AssociationNageurGroupe;
import classes.NageurAssociation;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.OPTIONS;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import me.jesuismister.main.Main;
import me.jesuismister.sql.DS;

@Produces("application/json")
@Path("groupe")
public class GroupeRessource {

	@GET
	@Path("getAllYear")
	public Response getAllYear() {
		List<String> res = new ArrayList<String>();

		String requete = "SELECT DISTINCT(annee) FROM `AssociationNageurGroupe` ORDER BY annee DESC;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(rs.getInt("annee") + "-" + (rs.getInt("annee")+1));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@POST
	@Path("associate")
	public Response associateNageurGroupe(AssociationNageurGroupe association) {
		String requete = "INSERT INTO AssociationNageurGroupe VALUES(DEFAULT, ?, ?, ?);";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setString(1, association.nom_groupe);
			ps.setInt(2, association.id_nageur);
			ps.setInt(3, association.annee);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("associate")
	public Response corsForAddNageur() {
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@GET
	@Path("getAllAssociation")
	public Response getAllNageurAssociation() {
		List<NageurAssociation> res = new ArrayList<NageurAssociation>();

		String requete = "SELECT id_assossiation, nom, prenom, nom_groupe, annee FROM `AssociationNageurGroupe` as A JOIN Nageur as N ON A.id_nageur = N.id_nageur ORDER BY annee DESC, nom_groupe, nom, prenom;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new NageurAssociation(rs.getInt("id_assossiation"), rs.getString("nom").toUpperCase(), rs.getString("prenom").substring(0,1).toUpperCase() + rs.getString("prenom").substring(1), rs.getString("nom_groupe"), rs.getInt("annee")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@POST
	@Path("deleteAssociation")
	@Consumes("application/json")
	public Response deleteAssociation(int i) {
		String requete = "DELETE FROM AssociationNageurGroupe WHERE id_assossiation = ?";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = con.prepareStatement(requete);
			ps.setInt(1, i);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("deleteAssociation")
	public Response corsForDeleteAssociation() {
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
}
