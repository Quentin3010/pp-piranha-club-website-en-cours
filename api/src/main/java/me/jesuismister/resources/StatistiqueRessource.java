package me.jesuismister.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import classes.StatCountVisite;
import classes.StatCountVisiteNews;
import classes.StatCountVisiteProfil;
import classes.StatSaveVisite;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.OPTIONS;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import me.jesuismister.main.Main;
import me.jesuismister.sql.DS;

@Produces("application/json")
@Path("stats")
public class StatistiqueRessource {

	//NOMBRE DE VISITE (UNIQUE/TOTALE) SUR UN MOIS
	@GET
	@Path("visite/{date}/monthly")
	public Response nbVisiteMensuel(@PathParam("date") String date) {
		List<StatCountVisite> res = new ArrayList<StatCountVisite>();

		String requete = "WITH RECURSIVE dates AS (\n"
				+ "  SELECT DATE(?) AS date_visite\n"
				+ "  UNION ALL\n"
				+ "  SELECT DATE_ADD(date_visite, INTERVAL 1 DAY)\n"
				+ "  FROM dates\n"
				+ "  WHERE date_visite < DATE(?)\n"
				+ ")\n"
				+ "SELECT\n"
				+ "    dates.date_visite,\n"
				+ "    COUNT(DISTINCT cv.ip) AS visite_unique,\n"
				+ "    COUNT(cv.ip) AS visite_totale\n"
				+ "FROM\n"
				+ "    dates\n"
				+ "LEFT JOIN\n"
				+ "    CompteurVisite cv ON dates.date_visite = cv.date_visite\n"
				+ "GROUP BY\n"
				+ "    dates.date_visite\n"
				+ "ORDER BY\n"
				+ "    dates.date_visite DESC;\n"
				+ "";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);

			ps.setString(2, date);

			String d = LocalDate.parse(date).minusMonths(1).format(DateTimeFormatter.ISO_LOCAL_DATE);
			ps.setString(1, d);

			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new StatCountVisite(rs.getString("date_visite"), "*", rs.getInt("visite_unique"), rs.getInt("visite_totale")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info(e.getMessage());
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	//NOMBRE DE VISITE (UNIQUE/TOTALE) SUR UN AN
	@GET
	@Path("visite/{date}/yearly")
	public Response nbVisiteAnuelle(@PathParam("date") String date) {
		List<StatCountVisite> res = new ArrayList<StatCountVisite>();

		String requete = "WITH RECURSIVE dates AS (\n"
				+ "  SELECT DATE(?) AS date_visite\n"
				+ "  UNION ALL\n"
				+ "  SELECT DATE_ADD(date_visite, INTERVAL 1 DAY)\n"
				+ "  FROM dates\n"
				+ "  WHERE date_visite < DATE(?)\n"
				+ ")\n"
				+ "SELECT\n"
				+ "    dates.date_visite,\n"
				+ "    COUNT(DISTINCT cv.ip) AS visite_unique,\n"
				+ "    COUNT(cv.ip) AS visite_totale\n"
				+ "FROM\n"
				+ "    dates\n"
				+ "LEFT JOIN\n"
				+ "    CompteurVisite cv ON dates.date_visite = cv.date_visite\n"
				+ "GROUP BY\n"
				+ "    dates.date_visite\n"
				+ "ORDER BY\n"
				+ "    dates.date_visite DESC;\n"
				+ "";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);

			String d = LocalDate.parse(date).minusYears(1).plusDays(1).format(DateTimeFormatter.ISO_LOCAL_DATE);
			ps.setString(1, d);
			ps.setString(2, date);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new StatCountVisite(rs.getString("date_visite"), "*", rs.getInt("visite_unique"), rs.getInt("visite_totale")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info(e.getMessage());
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	//NOMBRE DE VISITE D'UNE PAGE(UNIQUE/TOTALE) SUR UN MOIS
	@GET
	@Path("visite/{date}/page/{page}")
	public Response nbVisitePageMensuel(@PathParam("date") String date, @PathParam("page") String page) {
		List<StatCountVisite> res = new ArrayList<StatCountVisite>();

		String requete = "WITH RECURSIVE dates AS (\n"
				+ "  SELECT DATE(?) AS date_visite\n"
				+ "  UNION ALL\n"
				+ "  SELECT DATE_ADD(date_visite, INTERVAL 1 DAY)\n"
				+ "  FROM dates\n"
				+ "  WHERE date_visite < DATE(?)\n"
				+ ")\n"
				+ "SELECT\n"
				+ "    dates.date_visite,\n"
				+ "    COUNT(DISTINCT cv.ip) AS visite_unique,\n"
				+ "    COUNT(cv.ip) AS visite_totale\n"
				+ "FROM\n"
				+ "    dates\n"
				+ "LEFT JOIN\n"
				+ "    CompteurVisite cv ON dates.date_visite = cv.date_visite AND cv.page LIKE ?\n"
				+ "GROUP BY\n"
				+ "    dates.date_visite\n"
				+ "ORDER BY\n"
				+ "    dates.date_visite DESC;\n"
				+ "";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);

			String d = LocalDate.parse(date).minusMonths(1).format(DateTimeFormatter.ISO_LOCAL_DATE);
			ps.setString(1, d);
			ps.setString(2, date);
			ps.setString(3, "%"+page+"%");

			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new StatCountVisite(rs.getString("date_visite"), page, rs.getInt("visite_unique"), rs.getInt("visite_totale")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info(e.getMessage());
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	//NOMBRE DE VISITE SUR LES PROFILS NAGEURS (UNIQUE ET TOTALE)
	@GET
	@Path("visite/profils")
	public Response nbVisiteProfilsNageurs() {
		List<StatCountVisiteProfil> res = new ArrayList<StatCountVisiteProfil>();

		String requete = "SELECT n.id_nageur, n.nom, n.prenom, COUNT(DISTINCT cv.ip) AS 'visite_unique', COUNT(cv.ip) AS 'visite_totale' FROM Nageur n LEFT JOIN CompteurVisite cv ON SUBSTRING_INDEX(cv.page, '=', -1) = n.id_nageur AND cv.page LIKE 'profil_nageur%' GROUP BY n.id_nageur  \r\n"
				+ "ORDER BY `visite_totale` DESC";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new StatCountVisiteProfil(rs.getInt("id_nageur"), rs.getString("nom"), rs.getString("prenom"), rs.getInt("visite_unique"), rs.getInt("visite_totale")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info(e.getMessage());
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	//NOMBRE DE VISITE SUR LES NEWS (UNIQUE ET TOTALE)
	@GET
	@Path("visite/news")
	public Response nbVisiteNews() {
		List<StatCountVisiteNews> res = new ArrayList<StatCountVisiteNews>();

		String requete = "SELECT n.id_news, n.miniature, n.titre, COUNT(DISTINCT cv.ip) AS 'visite_unique', COUNT(cv.ip) AS 'visite_totale' FROM News n LEFT JOIN CompteurVisite cv ON SUBSTRING_INDEX(cv.page, '=', -1) = n.id_news AND (cv.page LIKE 'news.html?id=%' OR cv.page LIKE 'news?id=%') GROUP BY n.id_news ORDER BY n.id_news DESC;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new StatCountVisiteNews(rs.getInt("id_news"), rs.getString("miniature"), rs.getString("titre"), rs.getInt("visite_unique"), rs.getInt("visite_totale")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info(e.getMessage());
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	//NOMBRE DE VISITE DEPUIS FACEBOOK (UNIQUE ET TOTALE)
	@GET
	@Path("visite/facebook")
	public Response nbVisiteFacebook() {
		List<StatCountVisite> res = new ArrayList<StatCountVisite>();

		String requete = "SELECT COUNT(DISTINCT(ip)) as visite_unique, COUNT(ip) as visite_totale FROM CompteurVisite WHERE page LIKE '%fbclid%'";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new StatCountVisite("*", "from_facebook", rs.getInt("visite_unique"), rs.getInt("visite_totale")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info(e.getMessage());
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	//NOMBRE DE VISITE DEPUIS UN QRCODE (UNIQUE ET TOTALE)
	@GET
	@Path("visite/qrcode")
	public Response nbVisiteQRCode() {
		List<StatCountVisite> res = new ArrayList<StatCountVisite>();

		String requete = "SELECT COUNT(DISTINCT(ip)) as visite_unique, COUNT(ip) as visite_totale FROM CompteurVisite WHERE page LIKE '%qr_code=true%'";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new StatCountVisite("*", "from_qrcode", rs.getInt("visite_unique"), rs.getInt("visite_totale")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info(e.getMessage());
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	//NOMBRE DE VISITE DEPUIS UN QRCODE (UNIQUE ET TOTALE)
	@GET
	@Path("visite")
	public Response nbVisiteUnique() {
		List<Integer> res = new ArrayList<Integer>();

		String requete = "SELECT COUNT(DISTINCT(ip)) as visite_unique FROM CompteurVisite";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(rs.getInt("visite_unique"));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info(e.getMessage());
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	//AJOUT D'UNE VISITE SUR LE SITE
	@POST
	@Path("visite/add")
	public Response addVisite(StatSaveVisite visite) {
		String requete = "INSERT INTO CompteurVisite VALUES(DEFAULT, ?, ?);";
		if(visite.ip.hashCode()!=-254418555 && visite.ip.hashCode()!=-943502078) {
			try (Connection con = DS.getConnection()) {
				PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
				ps.setString(1, visite.page);
				ps.setString(2, visite.ip.hashCode()+"");
				ps.executeUpdate();
				con.close();
			} catch (Exception e) {
				Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}
		}

		return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Path("visite/add")
	public Response corsForAddNageur() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
}
