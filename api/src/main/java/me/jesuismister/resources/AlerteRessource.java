package me.jesuismister.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import classes.Alerte;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import me.jesuismister.sql.DS;
import me.jesuismister.main.Main;

@Produces("application/json")
@Path("loisir")
public class AlerteRessource {
	
	@GET
	@Path("getPhotos/{annee}")
	public Response getAlerte(@PathParam("annee") int annee) {
		List<Alerte> res = new ArrayList<Alerte>();

		String requete = "SELECT photo FROM PhotosLoisirs WHERE annee = ?;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, annee);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new Alerte(rs.getString("photo")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
}
