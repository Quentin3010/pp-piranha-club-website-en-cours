package me.jesuismister.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import classes.Nageur;
import classes.NageurCarte;
import classes.NageurPhoto;
import classes.NageurUpdate;
import classes.RecordNageur;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.OPTIONS;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import me.jesuismister.main.Main;
import me.jesuismister.sql.DS;

@Produces("application/json")
@Path("nageur")
public class NageurRessource {

	@GET
	@Path("get/{idNageur}")
	public Response getNageur(@PathParam("idNageur") int idNageur) {
		List<Nageur> res = new ArrayList<Nageur>();

		String requete = "SELECT * FROM Nageur WHERE id_nageur = ?;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, idNageur);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new Nageur(rs.getInt("id_nageur"), rs.getString("nom").toUpperCase(), rs.getString("prenom").substring(0,1).toUpperCase() + rs.getString("prenom").substring(1), 
						rs.getString("date_naiss"), rs.getString("sexe"), rs.getString("photo")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	@GET
	@Path("getAll/{orderId}")
	public Response getAll(@PathParam("orderId") String orderId) {
		List<Nageur> res = new ArrayList<Nageur>();

		String requete;
		if(orderId.equalsIgnoreCase("true")) {
			requete = "SELECT * FROM Nageur ORDER BY id_nageur;";
		}else {
			requete = "SELECT * FROM Nageur ORDER BY nom, prenom;";
		}

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new Nageur(rs.getInt("id_nageur"), rs.getString("nom").toUpperCase(), rs.getString("prenom").substring(0,1).toUpperCase() + rs.getString("prenom").substring(1), 
						rs.getString("date_naiss"), rs.getString("sexe"), rs.getString("photo")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	@GET
	@Path("getGroupe/{idNageur}")
	public Response getNageurGroupe(@PathParam("idNageur") int idNageur) {
		List<String> res = new ArrayList<String>();

		LocalDate dateDuJour = LocalDate.now();
		int mois = dateDuJour.getMonthValue();
		int annee = dateDuJour.getYear();

		if (mois >=1 && mois < 9) {
			annee -= 1;
		} 

		String requete = "SELECT * FROM AssociationNageurGroupe WHERE id_nageur = ? AND annee = ?; ";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, idNageur);
			ps.setInt(2, annee);

			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				res.add(rs.getString("nom_groupe") + " (" + annee + "-" + (annee+1) + ")");
			}else {
				res.add("Aucun");
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	@GET
	@Path("getRecords/{id_nageur}")
	public Response getRecords(@PathParam("id_nageur") int id_nageur) {
		List<RecordNageur> res = new ArrayList<RecordNageur>();

		String requete = "SELECT P.id_nageur, P.id_performance, P.distance, P.nage, P.temps, P.date_competition FROM Performance P JOIN (SELECT id_nageur, distance, nage, MIN(temps) AS min_temps FROM Performance WHERE id_nageur = ? GROUP BY distance, nage) AS MinPerf ON P.id_nageur = MinPerf.id_nageur AND P.distance = MinPerf.distance AND P.nage = MinPerf.nage AND P.temps = MinPerf.min_temps ORDER BY P.distance ASC, P.nage ASC;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, id_nageur);	
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new RecordNageur(rs.getInt("P.id_nageur"), rs.getInt("P.id_performance"), rs.getInt("P.distance"), rs.getString("P.nage"), rs.getString("P.temps"), rs.getString("P.date_competition")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@GET
	@Path("getAll/{groupe}/{annee}")
	public Response getAllFromGroupeAnnee(@PathParam("groupe") String Groupe, @PathParam("annee") int annee) {
		List<NageurCarte> res = new ArrayList<NageurCarte>();

		String requete = "SELECT DISTINCT(N.id_nageur), N.nom, N.prenom, N.photo, N.date_naiss FROM `AssociationNageurGroupe` as A JOIN Nageur as N ON A.id_nageur=N.id_nageur "
				+ "WHERE A.annee = ? AND A.nom_groupe = ? ORDER BY N.nom, N.prenom;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, annee);
			ps.setString(2, Groupe);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new NageurCarte(rs.getInt("N.id_nageur"), rs.getString("N.date_naiss"), rs.getString("N.nom").toUpperCase(), rs.getString("N.prenom").substring(0,1).toUpperCase() + rs.getString("N.prenom").substring(1), rs.getString("N.photo")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	@POST
	@Path("add")
	@Consumes("application/json")
	public Response addNageur(Nageur nageur) {
		String requete = "INSERT INTO Nageur (id_nageur, nom, prenom, date_naiss, sexe, photo) VALUES (DEFAULT, ?, ?, ?, ?, ?);";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = con.prepareStatement(requete);
			ps.setString(1, nageur.nom.toLowerCase());
			ps.setString(2, nageur.prenom.toLowerCase());
			if(nageur.date_naiss.equals("")) {
				ps.setString(3, null);
			}else {
				ps.setString(3, nageur.date_naiss);
			}
			ps.setString(4, nageur.sexe);
			ps.setString(5, nageur.photo);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Path("add")
	public Response corsForAddNageur() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@GET
	@Path("getId/{nom}/{prenom}")
	public Response getId(@PathParam("nom") String nom, @PathParam("prenom") String prenom) {
		List<Integer> res = new ArrayList<Integer>();

		String requete = "SELECT id_nageur FROM Nageur WHERE nom = ? AND prenom = ?;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setString(1, nom.toLowerCase());
			ps.setString(2, prenom.toLowerCase());
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				res.add(rs.getInt("id_nageur"));
			}else {
				res.add(-1);
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@POST
	@Path("update")
	@Consumes("application/json")
	public Response updateNageur(NageurUpdate nageur) {
		String requete = "UPDATE Nageur SET nom = ?, prenom = ?, date_naiss = ?, sexe = ? WHERE id_nageur = ?;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = con.prepareStatement(requete);
			ps.setString(1, nageur.nom.toLowerCase());
			ps.setString(2, nageur.prenom.toLowerCase());
			if(nageur.date_naiss.equals("")) {
				ps.setString(3, null);
			}else {
				ps.setString(3, nageur.date_naiss);
			}
			ps.setString(4, nageur.sexe);
			ps.setInt(5, nageur.id_nageur);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("update")
	public Response corsForUpdateNageur() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@POST
	@Path("changePhoto")
	@Consumes("application/json")
	public Response changePhoto(NageurPhoto nageur) {
		String requete = "UPDATE Nageur SET photo = ? WHERE id_nageur = ?;";
		
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = con.prepareStatement(requete);
			if(nageur.photo.equals("")) 
				ps.setString(1, null);
			else
				ps.setString(1, nageur.photo);
					
			ps.setInt(2, nageur.id_nageur);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Response.Status.OK).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("changePhoto")
	public Response corsForChangePhoto() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@POST
	@Path("delete")
	public Response delete(int id) {
		String requete = "DELETE FROM Nageur WHERE id_nageur = ?";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, id);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return Response.status(Response.Status.ACCEPTED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("delete")
	public Response corsForDeleteNageur() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
}
