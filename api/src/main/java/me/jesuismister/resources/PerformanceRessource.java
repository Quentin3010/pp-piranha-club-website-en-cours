package me.jesuismister.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import classes.NbPerfByAnnee;
import classes.Performance;
import classes.Performance2;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.OPTIONS;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import me.jesuismister.main.Main;
import me.jesuismister.sql.DS;

@Produces("application/json")
@Path("performance")
public class PerformanceRessource {

	@GET
	@Path("get/{idNageur}/{nage}/{distance}")
	public Response getPerformance(@PathParam("idNageur") int idNageur, @PathParam("nage") String nage, @PathParam("distance") int distance) {
		List<Performance> res = new ArrayList<Performance>();

		String requete = "SELECT * FROM Performance WHERE id_nageur = ? AND nage = ? AND distance = ? AND type_competition != 'record' ORDER BY date_competition DESC;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, idNageur);
			ps.setString(2, nage.replace("_", " "));
			ps.setInt(3, distance);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new Performance(rs.getInt("id_performance"), rs.getInt("id_nageur"), rs.getString("categorie_nageur"), 
						rs.getString("nage"), rs.getInt("distance"), rs.getString("temps"), 
						rs.getString("temps_50m"), rs.getString("temps_100m"), rs.getDate("date_competition").toString(), 
						rs.getString("type_competition"), rs.getString("lien_video")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@GET
	@Path("getBestByYear/{idNageur}/{year}/{nage}/{distance}")
	public Response getBestPerformanceByYear(@PathParam("idNageur") int idNageur, @PathParam("year") int year, @PathParam("nage") String nage, @PathParam("distance") int distance) {
		List<String> res = new ArrayList<String>();

		String requete = "SELECT temps FROM Performance WHERE id_nageur = ? AND nage = ? AND distance = ? AND date_competition>? AND date_competition<? AND type_competition != 'record' ORDER BY temps;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, idNageur);
			ps.setString(2, nage.replace("_", " "));
			ps.setInt(3, distance);
			ps.setString(4, year+"-09-01");
			ps.setString(5, (year+1)+"-09-01");
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				res.add(rs.getString("temps"));
			}else {
				res.add("");
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@GET
	@Path("getAllNbPerformanceByAnnee/{year}")
	public Response getAllNbPerformanceByAnnee(@PathParam("year") int year) {
		List<NbPerfByAnnee> res = new ArrayList<NbPerfByAnnee>();

		String requete = "select p.id_nageur,year(n.date_naiss) as annee_naiss ,CONCAT(UPPER(SUBSTRING(prenom, 1, 1)), LOWER(SUBSTRING(prenom, 2))) as prenom,UPPER(nom) as nom,a.nom_groupe,count(DISTINCT(p.type_competition)) AS nbPerf FROM Performance as p JOIN (Nageur AS n JOIN AssociationNageurGroupe AS a ON (n.id_nageur = a.id_nageur AND a.annee=?)) ON p.id_nageur=n.id_nageur WHERE date_competition>? AND date_competition<? AND p.type_competition != 'record' AND p.type_competition != 'Finale Nationale' AND p.type_competition != 'Inter-regionale' AND p.type_competition != 'Finale Régionale' AND p.type_competition != 'Finale Départementale' GROUP BY prenom, nom  ORDER BY a.nom_groupe, nom, prenom;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, year);
			ps.setString(2, year+"-09-01");
			ps.setString(3, (year+1)+"-09-01");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new NbPerfByAnnee(rs.getInt("id_nageur"),rs.getInt("annee_naiss"), rs.getString("prenom"),rs.getString("nom"),rs.getString("nom_groupe"),rs.getInt("nbPerf")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@POST
	@Path("add")
	public Response addPerformance(Performance performance) {
		String requete = "INSERT INTO Performance VALUES(DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, performance.id_nageur);
			ps.setString(2, performance.categorie_nageur);
			ps.setString(3, performance.nage);
			ps.setInt(4, performance.distance);
			ps.setString(5, performance.temps.replace(" ", ":"));
			if(performance.temps_50m.equals("")) ps.setString(6, "");
			else ps.setString(6, performance.temps_50m.replace(" ", ":"));
			
			if(performance.temps_100m.equals("")) ps.setString(7, "");
			else ps.setString(7, performance.temps_100m.replace(" ", ":"));
			ps.setString(8, performance.date_competition);
			ps.setString(9, performance.type_competition);
			ps.setString(10, performance.lien_video);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			Main.LOGGER.info(e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("add")
	public Response corsForAddNageur() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@GET
	@Path("getAll")
	public Response getAll() {
		List<Performance2> res = new ArrayList<Performance2>();

		String requete = "SELECT P.id_performance, CONCAT(UPPER(SUBSTRING(N.prenom, 1, 1)), LOWER(SUBSTRING(N.prenom, 2)), ' ', UPPER(SUBSTRING(N.nom, 1, 1)), '.') AS nom_complet, sexe, categorie_nageur, nage, distance, temps, temps_50m, temps_100m, date_competition, type_competition, lien_video FROM `Performance` as P JOIN Nageur as N ON P.id_nageur = N.id_nageur ORDER BY date_competition DESC, distance, nage, sexe, N.date_naiss;";

		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new Performance2(rs.getInt("id_performance"), rs.getString("nom_complet"), rs.getString("sexe"), rs.getString("categorie_nageur"), 
						rs.getString("nage"), rs.getInt("distance"), rs.getString("temps"), 
						rs.getString("temps_50m"), rs.getString("temps_100m"), rs.getDate("date_competition").toString(), 
						rs.getString("type_competition"), rs.getString("lien_video")));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}

		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@POST
	@Path("update")
	@Consumes("application/json")
	public Response deleteAssociation(Performance performance) {
		String requete = "UPDATE Performance SET categorie_nageur = ?, nage = ?, distance = ?, temps = ?, temps_50m = ?, temps_100m = ?, date_competition = ?, type_competition = ?, lien_video = ? WHERE id_performance = ?;";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = con.prepareStatement(requete);
			int i = 1;
			ps.setString(i++, performance.categorie_nageur);
			ps.setString(i++, performance.nage);
			ps.setInt(i++, performance.distance);
			ps.setString(i++, performance.temps);
			ps.setString(i++, performance.temps_50m);
			ps.setString(i++, performance.temps_100m);
			ps.setString(i++, performance.date_competition);
			ps.setString(i++, performance.type_competition);
			ps.setString(i++, performance.lien_video);
			ps.setInt(i++, performance.id_performance);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			Main.LOGGER.info(e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("update")
	public Response corsForUpdatePerformance() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
	
	@POST
	@Path("delete")
	public Response delete(int id) {
		String requete = "DELETE FROM Performance WHERE id_performance = ?";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.setInt(1, id);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return Response.status(Response.Status.ACCEPTED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@OPTIONS
	@Path("delete")
	public Response corsForDeleteNageur() {
		// Configuration des en-t�tes CORS pour autoriser les requ�tes cross-origin depuis toutes les origines
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST")
				.header("Access-Control-Allow-Headers", "Content-Type")
				.build();
	}
}
