package me.jesuismister.main;

import java.util.List;
import java.util.Map;

import org.glassfish.jersey.server.ResourceConfig;

import jakarta.ws.rs.ApplicationPath;
import me.jesuismister.resources.AlerteRessource;
import me.jesuismister.resources.StatistiqueRessource;
import me.jesuismister.resources.GroupeRessource;
import me.jesuismister.resources.LoginRessource;
import me.jesuismister.resources.LoisirRessource;
import me.jesuismister.resources.NageurRessource;
import me.jesuismister.resources.NewsRessource;
import me.jesuismister.resources.PerformanceRessource;
import me.jesuismister.resources.RecordRessource;
import me.jesuismister.resources.ResultatRessource;
import me.jesuismister.resources.TypeCompetitionRessource;


@ApplicationPath("api/v1/")
public class ApiV1 extends ResourceConfig {
	public static Map<String, List<String>> mapSeasonCol;

	public ApiV1() {
		packages("me.jesuismister.main");

		register(PerformanceRessource.class);
		register(NageurRessource.class);
		register(GroupeRessource.class);
		register(ResultatRessource.class);
		register(NewsRessource.class);
		register(LoginRessource.class);
		register(TypeCompetitionRessource.class);
		register(StatistiqueRessource.class);
		register(AlerteRessource.class);
		register(LoisirRessource.class);
		register(RecordRessource.class);
/*
		register(InfoResourceResource.class);
		register(CraftBlocksResource.class);
		register(CraftItemsResource.class);
		register(KilledByMobsResource.class);
		register(MobsKillResource.class);
		register(OtherResource.class);
		register(PlaceBlocksResource.class);
		register(PlayerListResource.class);
		*/	
	}
}
