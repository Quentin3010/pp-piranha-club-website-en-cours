const jwt = require('jsonwebtoken');
const jwtConfig = require('../config/jwt.config');

const validToken = (req, res , next) => {
  try {
    if (checkAdmin() == false) {
      res.status(301).redirect('/login');
    } else {
      const token = req.cookies.token;
      const decoded = jwt.verify(token, jwtConfig.SECRET_TOKEN);
      req.userId = decoded.id;
      next();
    }
  }
  catch (err) {
    console.log(`erreur JWT : ${err.message}`);
    if (req.headers['sec-fetch-dest'] === 'empty') { 
      res.status(401).json({ redirectTo : '/login'});
    } else {
      res.status(301).redirect('/login');
    }
  }
}
module.exports.validToken = validToken;

const ip_api = require('../config/ip').ip_api;

async function checkAdmin() {
    try {
        const login = sessionStorage.getItem("login");
        const mdp = sessionStorage.getItem("mdp");

        if (await isAdmin(login, mdp) == "true") return true;
        else return false;
    } catch (erreur) {
        return false
    }
}

async function isAdmin(login, mdp) {
    try {
        const response = await fetch(`${ip_api}/login/${login}/${mdp}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data;
}