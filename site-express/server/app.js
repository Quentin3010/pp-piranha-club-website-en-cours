const express = require('express');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const cookieParser = require('cookie-parser');
const compression = require('compression');

// routers
const index_router = require('./routes/index.route.js');
const inscription_router = require('./routes/inscription.route.js');
const sponsors_router = require('./routes/sponsors.route.js');
const contact_router = require('./routes/contact.route.js');
const news_router = require('./routes/news.route.js');
const espace_club_router = require('./routes/espace_club.route.js');
const nos_nageurs_router = require('./routes/nos_nageurs.route.js');
const profil_nageur_router = require('./routes/profil_nageur.route.js');
const resultats_router = require('./routes/resultats.route.js');
const login_router = require('./routes/login.route.js');
const admin_router = require('./routes/admin.route.js');
const galerie_router = require('./routes/galerie.route.js');

const api_router = require('./routes/api.route.js');

// define middlewares
const error = require('./middlewares/error.middleware.js');

const app = express();
app.use(express.static('build'));
app.use(compression());
app.use(express.static(path.join(__dirname, 'public'), { maxAge: '1y' }));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// route middlewares
app.use('/', index_router);
app.use('/index', index_router);

app.use('/inscription', inscription_router);

app.use('/sponsors', sponsors_router);

app.use('/contact', contact_router);

app.use('/news', news_router);

app.use('/espace_club', espace_club_router);

app.use('/nos_nageurs', nos_nageurs_router);

app.use('/profil_nageur', profil_nageur_router);

app.use('/resultats', resultats_router);

app.use('/galerie', galerie_router);

app.use('/login', login_router);

app.use('/admin', admin_router);

app.use('/api', api_router)

/****************************
UPLOAD ET DOWNLOAD DES PHOTOS
****************************/
const uploadRouter = require('./routes/photos/upload.route');
const photosRouter = require('./routes/photos/photos.route');

app.use('/upload', uploadRouter);
app.use('/photos', photosRouter);
app.use('/uploads', express.static(path.join(__dirname, '../website_photos')));

//////////////////////////

app.use(error);
module.exports = app;
