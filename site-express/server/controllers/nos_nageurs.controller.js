const sendFile = require('./util').sendFile;

const home = (req, res) => sendFile(req, res, 'html/nos_nageurs.html');

module.exports.home = home;
