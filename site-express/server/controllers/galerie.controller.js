const sendFile = require('./util').sendFile;

const home = (req, res) => sendFile(req, res, 'html/galerie.html');

module.exports.home = home;
