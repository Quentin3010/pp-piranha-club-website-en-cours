const sendFile = require('./util').sendFile;

const home = (req, res) => sendFile(req, res, 'html/login.html');
module.exports.home = home;
