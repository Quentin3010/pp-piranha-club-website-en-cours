const sendFile = require('./util').sendFile;

const home = (req, res) => sendFile(req, res, 'html/contact.html');

module.exports.home = home;
