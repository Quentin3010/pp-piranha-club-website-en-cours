const sendFile = require('./util').sendFile;

const home = (req, res) => sendFile(req, res, 'html/espace_club.html');

module.exports.home = home;
