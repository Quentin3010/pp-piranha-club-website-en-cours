const sendFile = require('./util').sendFile;

const home = (req, res) => sendFile(req, res, 'html/profil_nageur.html');

module.exports.home = home;
