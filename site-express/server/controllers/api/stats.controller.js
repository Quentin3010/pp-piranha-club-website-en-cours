const ip_api = require('../../config/ip').ip_api;
const fetch = require('cross-fetch');

const visite_add = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req.body)
    };

    fetch(`${ip_api}/stats/visite/add`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.visite_add = visite_add;

const visite_monthly = (req, res) => {
    const date = req.params.date;

    fetch(`${ip_api}/stats/visite/${date}/monthly`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.visite_monthly = visite_monthly;

const visite_yearly = (req, res) => {
    const date = req.params.date;

    fetch(`${ip_api}/stats/visite/${date}/yearly`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.visite_yearly = visite_yearly;

const visite_page = (req, res) => {
    const date = req.params.date;
    const page = req.params.page;

    fetch(`${ip_api}/stats/visite/${date}/page/${page}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.visite_page = visite_page;

const visite_profils = (req, res) => {
    fetch(`${ip_api}/stats/visite/profils`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.visite_profils = visite_profils;

const visite_news = (req, res) => {
    fetch(`${ip_api}/stats/visite/news`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.visite_news = visite_news;

const visite_qrcode = (req, res) => {
    fetch(`${ip_api}/stats/visite/qrcode`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.visite_qrcode = visite_qrcode;


const visite_facebook = (req, res) => {
    fetch(`${ip_api}/stats/visite/facebook`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.visite_facebook = visite_facebook;

const visite_unique = (req, res) => {
    fetch(`${ip_api}/stats/visite`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.visite_unique = visite_unique;