const ip_api = require('../../config/ip').ip_api;
const fetch = require('cross-fetch');

const add = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req.body)
    };

    fetch(`${ip_api}/performance/add`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.add = add;

const deletePerformance = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(parseInt(req.body.id))
    };
    
    fetch(`${ip_api}/performance/delete`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.delete = deletePerformance;

const update = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req.body)
    };

    fetch(`${ip_api}/performance/update`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.update = update;

const get = (req, res) => {
    const id_nageur = req.params.id_nageur;
    const nage = req.params.nage;
    const distance = req.params.distance;

    fetch(`${ip_api}/performance/get/${id_nageur}/${nage}/${distance}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.get = get;

const getAllNbPerformanceByAnnee = (req, res) => {
    const year = req.params.year;

    fetch(`${ip_api}/performance/getAllNbPerformanceByAnnee/${year}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getAllNbPerformanceByAnnee = getAllNbPerformanceByAnnee;

const getBestByYear = (req, res) => {
    const id_nageur = req.params.id_nageur;
    const year = req.params.year;
    const nage = req.params.nage;
    const distance = req.params.distance;

    fetch(`${ip_api}/performance/getBestByYear/${id_nageur}/${year}/${nage}/${distance}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getBestByYear = getBestByYear;

const getAll = (req, res) => {
    fetch(`${ip_api}/performance/getAll`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getAll = getAll;