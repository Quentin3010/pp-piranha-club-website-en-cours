const ip_api = require('../../config/ip').ip_api;
const fetch = require('cross-fetch');

const jwt = require('jsonwebtoken');
const jwtConfig = require('../../config/jwt.config');

const connect = async (req, res) => {
    const login = req.params.login;
    const mdp = req.params.mdp;

    try {
        const data = await isAdmin(login, mdp)
        if (data == "true") {
            const token = jwt.sign({ login: login }, jwtConfig.SECRET_TOKEN, { expiresIn: '1h' });
            res.cookie('token', token, { maxAge: 3600000, httpOnly: false, sameSite: 'strict' })
            res.status(200).json({ login: login });
        } else {
            res.status(404).send("Login ou mot de passe incorrect !");
        }
    } catch (e) {
        console.log("Erreur dans login.controller.js : connect")
        res.status(500).send("Internal Server Error");
    }
}
module.exports.connect = connect;

async function isAdmin(login, mdp) {
    try {
        const response = await fetch(`${ip_api}/login/${login}/${mdp}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data;
}