const ip_api = require('../../config/ip').ip_api;
const fetch = require('cross-fetch');

const add = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req.body)
    };

    fetch(`${ip_api}/nageur/add`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.add = add;

const deleteNageur = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(parseInt(req.body.id))
    };
    
    fetch(`${ip_api}/nageur/delete`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.delete = deleteNageur;

const update = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req.body)
    };

    fetch(`${ip_api}/nageur/update`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.update = update;

const changePhoto = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req.body)
    };

    fetch(`${ip_api}/nageur/changePhoto`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.changePhoto = changePhoto;

const getAll = (req, res) => {
    const orderById = req.params.orderById;

    fetch(`${ip_api}/nageur/getAll/${orderById}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getAll = getAll;

const getAllByYearGroupe = (req, res) => {
    const groupe = req.params.groupe;
    const annee = req.params.annee;

    fetch(`${ip_api}/nageur/getAll/${groupe}/${annee}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getAllByYearGroupe = getAllByYearGroupe;

const get = (req, res) => {
    const id = req.params.id;

    fetch(`${ip_api}/nageur/get/${id}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.get = get;

const getGroupe = (req, res) => {
    const id = req.params.id;

    fetch(`${ip_api}/nageur/getGroupe/${id}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getGroupe = getGroupe;

const getId = (req, res) => {
    const nom = req.params.nom;
    const prenom = req.params.prenom;

    fetch(`${ip_api}/nageur/getId/${nom}/${prenom}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getId = getId;

const getRecords = (req, res) => {
    const id_nageur = req.params.id_nageur;

    fetch(`${ip_api}/nageur/getRecords/${id_nageur}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getRecords = getRecords;