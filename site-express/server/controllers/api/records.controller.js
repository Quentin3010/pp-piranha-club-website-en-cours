const ip_api = require('../../config/ip').ip_api;
const fetch = require('cross-fetch');

const getRecords = (req, res) => {
    const nage = req.params.nage;

    fetch(`${ip_api}/records/getRecords/${nage}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getRecords = getRecords;