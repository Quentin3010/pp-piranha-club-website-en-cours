const ip_api = require('../../config/ip').ip_api;
const fetch = require('cross-fetch');

const add = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req.body)
    };

    fetch(`${ip_api}/news/add`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.add = add;

const deleteNews = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(parseInt(req.body.id))
    };
    
    fetch(`${ip_api}/news/delete`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.delete = deleteNews;

const update = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req.body)
    };

    fetch(`${ip_api}/news/update`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.update = update;

const getPreviews = (req, res) => {
    const nb = req.params.nb;
    const nbAdded = req.params.nbAdded;

    fetch(`${ip_api}/news/getPreviews/${nb}/${nbAdded}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getPreviews = getPreviews;

const getPreviewsExcept = (req, res) => {
    const nb = req.params.nb;
    const exception = req.params.exception;

    fetch(`${ip_api}/news/getPreviewsExcept/${nb}/${exception}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getPreviewsExcept = getPreviewsExcept;

const get = (req, res) => {
    const id = req.params.id;

    fetch(`${ip_api}/news/get/${id}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.get = get;

const getAll = (req, res) => {
    fetch(`${ip_api}/news/getAll`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getAll = getAll;
