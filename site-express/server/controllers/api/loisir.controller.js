const ip_api = require('../../config/ip').ip_api;
const fetch = require('cross-fetch');

const getPhotos = (req, res) => {
    const annee = req.params.annee;

    fetch(`${ip_api}/loisir/getPhotos/${annee}`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getPhotos = getPhotos;