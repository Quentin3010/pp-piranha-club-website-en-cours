const ip_api = require('../../config/ip').ip_api;
const fetch = require('cross-fetch');

const associate = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req.body)
    };

    fetch(`${ip_api}/groupe/associate`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.associate = associate;

const deleteAssociation = (req, res) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(parseInt(req.body.id))
    };

    fetch(`${ip_api}/groupe/deleteAssociation`, requestOptions)
        .then(_ => {
            res.status(200).send("Ajouté !");
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.deleteAssociation = deleteAssociation;

const getAllYear = (req, res) => {
    fetch(`${ip_api}/groupe/getAllYear`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getAllYear = getAllYear;

const getAllAssociation = (req, res) => {
    fetch(`${ip_api}/groupe/getAllAssociation`)
        .then(response => {
            if (!response.ok) {
                res.status(404).send("Not found");
                return Promise.reject("Not found");
            }
            return response.json();
        })
        .then(data => {
            res.send(data);
        })
        .catch(error => {
            console.error('Erreur lors de la requête fetch :', error);
            res.status(500).send("Internal Server Error");
        });
}
module.exports.getAllAssociation = getAllAssociation;