const sendFile = require('./util').sendFile;

const admin = (req, res) => sendFile(req, res, 'html/admin.html');
module.exports.admin = admin;

const edit_groupes = (req, res) => sendFile(req, res, 'html/edit_groupes.html');
module.exports.edit_groupes = edit_groupes;
const edit_nageurs = (req, res) => sendFile(req, res, 'html/edit_nageurs.html');
module.exports.edit_nageurs = edit_nageurs;
const edit_news = (req, res) => sendFile(req, res, 'html/edit_news.html');
module.exports.edit_news = edit_news;
const edit_performances = (req, res) => sendFile(req, res, 'html/edit_performances.html');
module.exports.edit_performances = edit_performances;
const edit_resultats = (req, res) => sendFile(req, res, 'html/edit_resultats.html');
module.exports.edit_resultats = edit_resultats;
const edit_galerie = (req, res) => sendFile(req, res, 'html/edit_galerie.html');
module.exports.edit_galerie = edit_galerie;
const edit_album = (req, res) => sendFile(req, res, 'html/edit_album.html');
module.exports.edit_album = edit_album;

const outil_prevision = (req, res) => sendFile(req, res, 'html/outil_prevision.html');
module.exports.outil_prevision = outil_prevision;
const outil_resume = (req, res) => sendFile(req, res, 'html/outil_resume.html');
module.exports.outil_resume = outil_resume;

const statistiques = (req, res) => sendFile(req, res, 'html/statistiques.html');
module.exports.statistiques = statistiques;

