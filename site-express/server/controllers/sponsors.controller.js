const sendFile = require('./util').sendFile;

const home = (req, res) => sendFile(req, res, 'html/sponsors.html');

module.exports.home = home;
