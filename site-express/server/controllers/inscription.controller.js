const sendFile = require('./util').sendFile;

const home = (req, res) => sendFile(req, res, 'html/inscription.html');

module.exports.home = home;
