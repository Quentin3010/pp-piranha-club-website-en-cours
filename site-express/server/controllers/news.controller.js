const sendFile = require('./util').sendFile;

const home = (req, res) => sendFile(req, res, 'html/news.html');

module.exports.home = home;
