const sendFile = require('./util').sendFile;

const home = (req, res) => sendFile(req, res, 'html/resultats.html');

module.exports.home = home;
