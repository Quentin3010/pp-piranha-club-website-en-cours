const createError = require('http-errors');

// catch 404 and forward to error handler
const notFound =
(req, res, next) => {
  next(createError(404));
};


// error handler
const handleError = (err, req, res, next) => {
  res.redirect('/');
};

module.exports = handleError;

module.exports.notFound = notFound;
module.exports.handleError = handleError;
