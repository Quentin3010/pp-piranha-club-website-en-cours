const sendFile = require('./util').sendFile;

const home = (req, res) => sendFile(req, res, 'html/index.html');

module.exports.home = home;
