const path = require('path');
const fs = require('fs');
const util = require('util');
const readdir = util.promisify(fs.readdir);
const stat = util.promisify(fs.stat);

const getPhotos = (req, res) => {
    const { nbFile, idx } = req.body;
    const folderPath = path.join(__dirname, '../../../website_photos/albums', req.params.folderName);

    fs.readdir(folderPath, (err, files) => {
        if (err) {
            return res.status(500).json({ error: 'Erreur lors de la lecture des fichiers' });
        }

        // Filtrer les fichiers pour obtenir uniquement les images
        const imageFiles = files.filter(file => {
            const ext = path.extname(file).toLowerCase();
            return ext === '.jpg' || ext === '.jpeg' || ext === '.png' || ext === '.gif';
        });

        // Créer un tableau pour stocker les fichiers avec leurs dates de création
        const fileDetails = [];

        imageFiles.forEach(file => {
            const filePath = path.join(folderPath, file);
            const stats = fs.statSync(filePath);
            fileDetails.push({ fileName: file, createdTime: stats.birthtime });
        });

        // Trier les fichiers par date de création du plus ancien au plus récent
        fileDetails.sort((a, b) => a.createdTime - b.createdTime);

        // Extraire les fichiers demandés en commençant à l'indice donné
        const selectedFiles = fileDetails.slice(idx, idx + nbFile);

        // Retourner le nombre total de photos et les fichiers sélectionnés
        res.json({ nbPhoto: selectedFiles.length, photos: selectedFiles.map(f => f.fileName) });
    });
};
module.exports.getPhotos = getPhotos;

const getPreviews = async (req, res) => {
    const photosDir = path.join(__dirname, '../../../website_photos/albums');

    try {
        const folders = await readdir(photosDir);

        if (folders.length === 0) {
            return res.status(204).json({ error: 'Aucune photo sur le serveur' });
        }

        const folderDetailsPromises = folders.map(async (folder) => {
            const folderPath = path.join(photosDir, folder);
            const stats = await stat(folderPath);

            return {
                name: folder,
                createdTime: stats.birthtime
            };
        });

        const folderDetails = await Promise.all(folderDetailsPromises);
        folderDetails.sort((a, b) => b.createdTime - a.createdTime);

        const previews = await Promise.all(
            folderDetails.map(async (detail) => {
                const folderPath = path.join(photosDir, detail.name);
                const files = await readdir(folderPath);

                const imageFiles = files.filter(file => {
                    const ext = path.extname(file).toLowerCase();
                    return ext === '.jpg' || ext === '.jpeg' || ext === '.png' || ext === '.gif';
                });

                if (imageFiles.length === 0) {
                    return null;
                }

                const imageDetailsPromises = imageFiles.map(async (image) => {
                    const imagePath = path.join(folderPath, image);
                    const stats = await stat(imagePath);

                    return {
                        name: image,
                        createdTime: stats.birthtime
                    };
                });

                const imageDetails = await Promise.all(imageDetailsPromises);
                imageDetails.sort((a, b) => a.createdTime - b.createdTime);

                return {
                    folderName: detail.name,
                    firstImage: imageDetails[0].name
                };
            })
        );

        res.json(previews.filter(preview => preview !== null));
    } catch (err) {
        res.status(500).json({ error: 'Erreur lors du traitement des dossiers ou des fichiers' });
    }
};

module.exports.getPreviews = getPreviews;

module.exports.getPreviews = getPreviews;


const deletePhoto = (req, res) => {
    const file_path = req.body.file_path;

    if (!file_path) {
        return res.status(400).json({ error: 'Le chemin du fichier est requis' });
    }

    const filePath = path.join(__dirname, '../../../website_photos/albums', file_path);

    fs.unlink(filePath, err => {
        if (err) {
            return res.status(500).json({ error: 'Erreur lors de la suppression du fichier' });
        }

        const folderPath = path.dirname(filePath);

        // Vérifier si le dossier est maintenant vide
        fs.readdir(folderPath, (err, files) => {
            if (err) {
                return res.status(500).json({ error: 'Erreur lors de la lecture du dossier' });
            }

            // Si le dossier est vide, le supprimer
            if (files.length === 0) {
                fs.rmdir(folderPath, err => {
                    if (err) {
                        return res.status(500).json({ error: 'Erreur lors de la suppression du dossier vide' });
                    }
                    res.status(204).json({ message: 'Fichier et dossier supprimés avec succès' });
                });
            } else {
                res.status(200).json({ message: 'Fichier supprimé avec succès' });
            }
        });
    });
};
module.exports.deletePhoto = deletePhoto;

const renameFolder = (req, res) => {
    const { oldName, newName } = req.body;

    if (!oldName || !newName) {
        return res.status(400).json({ error: 'Les noms des dossiers sont requis' });
    }

    const oldFolderPath = path.join(__dirname, '../../../website_photos/albums', oldName);
    const newFolderPath = path.join(__dirname, '../../../website_photos/albums', newName);

    fs.rename(oldFolderPath, newFolderPath, (err) => {
        if (err) {
            return res.status(500).json({ error: 'Erreur lors du renommage du dossier' });
        }
        res.status(200).json({ message: 'Dossier renommé avec succès' });
    });
};
module.exports.renameFolder = renameFolder;

const deleteFolder = (req, res) => {
    const { folderName } = req.body;

    if (!folderName) {
        return res.status(400).json({ error: 'Le nom du dossier est requis' });
    }

    const folderPath = path.join(__dirname, '../../../website_photos/albums', folderName);

    // Supprimer tous les fichiers dans le dossier
    fs.readdir(folderPath, (err, files) => {
        if (err) {
            return res.status(500).json({ error: 'Erreur lors de la lecture des fichiers du dossier' });
        }

        let filesDeleted = 0;

        if (files.length === 0) {
            // Si le dossier est vide, le supprimer directement
            fs.rmdir(folderPath, err => {
                if (err) {
                    return res.status(500).json({ error: 'Erreur lors de la suppression du dossier' });
                }
                return res.status(204).send(); // No Content
            });
        } else {
            // Supprimer tous les fichiers dans le dossier
            files.forEach(file => {
                const filePath = path.join(folderPath, file);
                fs.unlink(filePath, err => {
                    if (err) {
                        return res.status(500).json({ error: `Erreur lors de la suppression du fichier ${file}` });
                    }
                    filesDeleted++;

                    // Si tous les fichiers ont été supprimés, supprimer le dossier
                    if (filesDeleted === files.length) {
                        fs.rmdir(folderPath, err => {
                            if (err) {
                                return res.status(500).json({ error: 'Erreur lors de la suppression du dossier' });
                            }
                            res.status(204).send(); // No Content
                        });
                    }
                });
            });
        }
    });
};
module.exports.deleteFolder = deleteFolder;