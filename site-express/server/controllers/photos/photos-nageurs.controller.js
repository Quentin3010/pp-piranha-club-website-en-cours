const path = require('path');
const fs = require('fs');

const deleteNageurPhoto = (req, res) => {
    const id = req.body.id_nageur;

    if (!id) {
        return res.status(400).json({ error: "L'id du nageur fournis n'est pas valide" });
    }

    const filePath = path.join(__dirname, '../../../website_photos/nageurs', id + ".jpeg");

    fs.unlink(filePath, err => {
        return res.status(204).send();
    });
};
module.exports.deleteNageurPhoto = deleteNageurPhoto;