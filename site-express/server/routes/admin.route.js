const express = require('express');
const router = express.Router();

const authMiddleware = require('../middlewares/authentication-admin.middleware');

const controller = require('../controllers/admin.controller');

router.get('/', authMiddleware.validToken , controller.admin);

router.get('/edit_groupes', authMiddleware.validToken , controller.edit_groupes);
router.get('/edit_nageurs', authMiddleware.validToken , controller.edit_nageurs);
router.get('/edit_news', authMiddleware.validToken , controller.edit_news);
router.get('/edit_performances', authMiddleware.validToken , controller.edit_performances);
router.get('/edit_resultats', authMiddleware.validToken , controller.edit_resultats);
router.get('/edit_galerie', authMiddleware.validToken , controller.edit_galerie);
router.get('/edit_album', authMiddleware.validToken , controller.edit_album);

router.get('/outil_prevision', authMiddleware.validToken , controller.outil_prevision);
router.get('/outil_resume', authMiddleware.validToken , controller.outil_resume);

router.get('/statistiques', authMiddleware.validToken , controller.statistiques);

module.exports = router;

