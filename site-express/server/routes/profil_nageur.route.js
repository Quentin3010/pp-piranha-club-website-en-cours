const express = require('express');
const router = express.Router();

const controller = require('../controllers/profil_nageur.controller');

router.get('/', controller.home);

module.exports = router;
