const express = require('express');
const router = express.Router();

const authMiddleware = require('../middlewares/authentication-admin.middleware');

const competition_controller = require('../controllers/api/competition.controller');
router.get('/competition/getAll', competition_controller.getAll);

const groupe_controller = require('../controllers/api/groupe.controller');
router.post('/groupe/associate', authMiddleware.validToken, groupe_controller.associate);
router.post('/groupe/deleteAssociation', authMiddleware.validToken, groupe_controller.deleteAssociation);
router.get('/groupe/getAllYear', groupe_controller.getAllYear);
router.get('/groupe/getAllAssociation', groupe_controller.getAllAssociation);

const login_controller = require('../controllers/api/login.controller');
router.get('/login/connect/:login/:mdp', login_controller.connect);

const loisir_controller = require('../controllers/api/loisir.controller');
router.get('/loisir/getPhotos/:annee', loisir_controller.getPhotos);

const nageur_controller = require('../controllers/api/nageur.controller');
router.post('/nageur/add', authMiddleware.validToken, nageur_controller.add);
router.post('/nageur/delete', authMiddleware.validToken, nageur_controller.delete);
router.post('/nageur/update', authMiddleware.validToken, nageur_controller.update);
router.post('/nageur/changePhoto', authMiddleware.validToken, nageur_controller.changePhoto);
router.get('/nageur/getAll/:orderById', nageur_controller.getAll);
router.get('/nageur/getAll/:groupe/:annee', nageur_controller.getAllByYearGroupe);
router.get('/nageur/get/:id', nageur_controller.get);
router.get('/nageur/getGroupe/:id', nageur_controller.getGroupe);
router.get('/nageur/getId/:nom/:prenom', nageur_controller.getId);
router.get('/nageur/getRecords/:id_nageur', nageur_controller.getRecords);

const news_controller = require('../controllers/api/news.controller');
router.post('/news/add', authMiddleware.validToken, news_controller.add);
router.post('/news/delete', authMiddleware.validToken, news_controller.delete);
router.post('/news/update', authMiddleware.validToken, news_controller.update);
router.get('/news/getPreviews/:nb/:nbAdded', news_controller.getPreviews);
router.get('/news/getPreviewsExcept/:nb/:exception', news_controller.getPreviewsExcept);
router.get('/news/get/:id', news_controller.get);
router.get('/news/getAll', news_controller.getAll);

const performance_controller = require('../controllers/api/performance.controller');
router.post('/performance/add', authMiddleware.validToken, performance_controller.add);
router.post('/performance/delete', authMiddleware.validToken, performance_controller.delete);
router.post('/performance/update', authMiddleware.validToken, performance_controller.update);
router.get('/performance/get/:id_nageur/:nage/:distance', performance_controller.get);
router.get('/performance/getAllNbPerformanceByAnnee/:year', performance_controller.getAllNbPerformanceByAnnee);
router.get('/performance/getBestByYear/:id_nageur/:year/:nage/:distance', performance_controller.getBestByYear);
router.get('/performance/getAll', performance_controller.getAll);

const records_controller = require('../controllers/api/records.controller');
router.get('/records/getRecords/:nage', records_controller.getRecords);

const resultat_controller = require('../controllers/api/resultat.controller');
router.post('/resultat/add', authMiddleware.validToken, resultat_controller.add);
router.post('/resultat/delete', authMiddleware.validToken, resultat_controller.delete);
router.post('/resultat/update', authMiddleware.validToken, resultat_controller.update);
router.get('/resultat/getAll', resultat_controller.getAll);

const stats_controller = require('../controllers/api/stats.controller');
router.post('/stats/visite/add', stats_controller.visite_add);
router.get('/stats/visite/:date/monthly', authMiddleware.validToken, stats_controller.visite_monthly);
router.get('/stats/visite/:date/yearly', authMiddleware.validToken, stats_controller.visite_yearly);
router.get('/stats/visite/:date/page/:page', authMiddleware.validToken, stats_controller.visite_page);
router.get('/stats/visite/profils', authMiddleware.validToken, stats_controller.visite_profils);
router.get('/stats/visite/news', authMiddleware.validToken, stats_controller.visite_news);
router.get('/stats/visite/qrcode', authMiddleware.validToken, stats_controller.visite_qrcode);
router.get('/stats/visite/facebook', authMiddleware.validToken, stats_controller.visite_facebook);
router.get('/stats/visite', authMiddleware.validToken, stats_controller.visite_unique);

module.exports = router;

