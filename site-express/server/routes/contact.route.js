const express = require('express');
const router = express.Router();

const controller = require('../controllers/contact.controller');

router.get('/', controller.home);

module.exports = router;
