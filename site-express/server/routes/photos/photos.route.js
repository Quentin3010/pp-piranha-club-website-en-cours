const express = require('express');
const router = express.Router();
const photosController = require('../../controllers/photos/photos.controller');
const photosNageursController = require('../../controllers/photos/photos-nageurs.controller');

const authMiddleware = require('../../middlewares/authentication-admin.middleware');

router.get('/album/getPreviews', photosController.getPreviews);
router.post('/album/getPhotos/:folderName', photosController.getPhotos);
router.post('/album/deletePhoto', authMiddleware.validToken, photosController.deletePhoto);
router.post('/album/renameFolder', authMiddleware.validToken, photosController.renameFolder);
router.post('/album/deleteFolder', authMiddleware.validToken, photosController.deleteFolder);

router.post('/nageur/deleteNageurPhoto', authMiddleware.validToken, photosNageursController.deleteNageurPhoto);

module.exports = router;
