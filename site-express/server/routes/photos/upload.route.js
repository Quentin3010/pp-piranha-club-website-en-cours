const express = require('express');
const router = express.Router();
const multer = require('multer');
const sharp = require('sharp');
const path = require('path');
const fs = require('fs');

const authMiddleware = require('../../middlewares/authentication-admin.middleware');

// Chemin vers le fichier du watermark
const watermarkPath = path.join(__dirname, 'watermark.png');

const newFilenameFunction = (req, file, callback) => {
    const timestamp = Date.now();
    callback(null, `${timestamp}`);
};

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, req.uploadDir);
    },
    filename: newFilenameFunction,
});

const upload = multer({ storage: storage });

////////////////////////////////////////////////////////////////:

// Middleware pour créer le répertoire s'il n'existe pas
const createDirIfNotExists = (req, res, next) => {
    const dirName = req.params.folderName;
    if (!dirName) {
        return res.status(400).json({ error: 'Le nom du dossier est requis' });
    }

    const uploadDir = path.join(__dirname, '../../../website_photos/albums', dirName);
    if (!fs.existsSync(uploadDir)) {
        fs.mkdirSync(uploadDir, { recursive: true });
    }

    // Créer un sous-dossier 'resized' pour les fichiers redimensionnés
    const resizedDir = path.join(uploadDir);
    if (!fs.existsSync(resizedDir)) {
        fs.mkdirSync(resizedDir, { recursive: true });
    }

    req.uploadDir = uploadDir;
    req.resizedDir = resizedDir;
    next();
};

//upload.single(formField) Returns middleware that processes a single file associated with the given form field.
router.post('/album/:folderName/:quality/:watermark', authMiddleware.validToken, createDirIfNotExists, upload.single('photo'), async (req, res) => {
    try {
        const resizedImagePath = path.join(req.resizedDir, (new Date()).getTime() + ".jpeg");

        let quality
        try {
            quality = parseInt(req.params.quality)
        } catch (error) {
            return res.status(500).json({ error: 'Paramètre "quality" invalide !' });
        }

        let mainImage = sharp(req.file.path);
        const metadata = await mainImage.metadata();
        mainImage = await mainImage.rotate().toBuffer();

        let size;
        let coeff;
        if (metadata.orientation==undefined || metadata.orientation < 5) {
            if (metadata.width > 1280) size = 1280
            else size = metadata.width
            coeff = 0.1
        } else {
            if (metadata.width > 720) size = 720
            else size = metadata.width
            coeff = 0.2
        }

        if (req.params.watermark == "true") {
            let watermark = await sharp(watermarkPath)
                .resize({ width: Math.round(size * coeff) })
                .toBuffer();

            await sharp(mainImage)
                .composite([{ input: watermark, gravity: 'southeast' }])
                .resize({ width : size })
                .jpeg({ quality: quality })
                .withMetadata()
                .toFile(resizedImagePath);

            fs.unlinkSync(req.file.path);
        }
        else if (req.params.watermark == "false") {
            await sharp(mainImage)
                .resize(size)
                .jpeg({ quality: quality })
                .withMetadata()
                .toFile(resizedImagePath);

            fs.unlinkSync(req.file.path);
        }
        else return res.status(500).json({ error: 'Paramètre "watermark" invalide !' });



    } catch (e) {
        console.log(e);
        return res.status(500).json({ error: 'Erreur lors du traitement de l\'image' });
    }
    res.status(200).json({ message: 'Image uploadée avec succès !' });
});

////////////////////////////////////////////////////////////////:

// Middleware pour créer le répertoire s'il n'existe pas
const createDirIfNotExists2 = (req, res, next) => {
    req.id_nageur = req.params.id_nageur;

    const uploadDir = path.join(__dirname, '../../../website_photos/nageurs');
    if (!fs.existsSync(uploadDir)) {
        fs.mkdirSync(uploadDir, { recursive: true });
    }

    // Créer un sous-dossier 'resized' pour les fichiers redimensionnés
    const resizedDir = path.join(uploadDir);
    if (!fs.existsSync(resizedDir)) {
        fs.mkdirSync(resizedDir, { recursive: true });
    }

    req.uploadDir = uploadDir;
    req.resizedDir = resizedDir;
    next();
};

router.post('/nageur/:id_nageur', authMiddleware.validToken, createDirIfNotExists2, upload.single('photo'), async (req, res) => {
    try {
        const resizedImagePath = path.join(req.resizedDir, req.id_nageur + ".jpeg");

        // Charger l'image principale
        let mainImage = sharp(req.file.path);

        // Correction de l'orientation basée sur les métadonnées EXIF
        mainImage = await mainImage.rotate().toBuffer();

        // Superposer le watermark sur l'image principale
        await sharp(mainImage)
            .resize(256) // Redimensionner l'image principale
            .jpeg({ quality: 100 })
            .withMetadata() // Conserver les métadonnées d'orientation
            .toFile(resizedImagePath);

        fs.unlinkSync(req.file.path);
    } catch (e) {
        console.log(e);
        return res.status(500).json({ error: 'Erreur lors du traitement de l\'image' });
    }
    res.status(200).json({ message: 'Image uploadée avec succès !' });
});

module.exports = router;

