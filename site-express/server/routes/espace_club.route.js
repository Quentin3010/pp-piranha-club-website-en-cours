const express = require('express');
const router = express.Router();

const controller = require('../controllers/espace_club.controller');

router.get('/', controller.home);

module.exports = router;
