const express = require('express');
const router = express.Router();

const controller = require('../controllers/inscription.controller');

router.get('/', controller.home);

module.exports = router;
