const express = require('express');
const router = express.Router();

const controller = require('../controllers/sponsors.controller');

router.get('/', controller.home);

module.exports = router;
