const express = require('express');
const router = express.Router();

const controller = require('../controllers/galerie.controller');

router.get('/', controller.home);

module.exports = router;
