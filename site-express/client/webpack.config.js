const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (env, argv) => {
  const isProduction = argv.mode === 'production';

  return {
    entry: {
      'index': path.resolve(__dirname, 'src', './scripts/index.js'),
      'inscription': path.resolve(__dirname, 'src', './scripts/inscription.js'),
      'sponsors': path.resolve(__dirname, 'src', './scripts/sponsors.js'),
      'contact': path.resolve(__dirname, 'src', './scripts/contact.js'),
      'news': path.resolve(__dirname, 'src', './scripts/news.js'),
      'espace_club': path.resolve(__dirname, 'src', './scripts/espace_club.js'),
      'nos_nageurs': path.resolve(__dirname, 'src', './scripts/nos_nageurs.js'),
      'profil_nageur': path.resolve(__dirname, 'src', './scripts/profil_nageur.js'),
      'resultats': path.resolve(__dirname, 'src', './scripts/resultats.js'),
      'login': path.resolve(__dirname, 'src', './scripts/login.js'),
      'galerie': path.resolve(__dirname, 'src', './scripts/galerie.js'),
      'admin': path.resolve(__dirname, 'src', './scripts/admin/admin.js'),
      'edit_groupes': path.resolve(__dirname, 'src', './scripts/admin/edit/edit_groupes.js'),
      'edit_nageurs': path.resolve(__dirname, 'src', './scripts/admin/edit/edit_nageurs.js'),
      'edit_news': path.resolve(__dirname, 'src', './scripts/admin/edit/edit_news.js'),
      'edit_performances': path.resolve(__dirname, 'src', './scripts/admin/edit/edit_performances.js'),
      'edit_resultats': path.resolve(__dirname, 'src', './scripts/admin/edit/edit_resultats.js'),
      'edit_galerie': path.resolve(__dirname, 'src', './scripts/admin/edit/edit_galerie.js'),
      'edit_album': path.resolve(__dirname, 'src', './scripts/admin/edit/edit_album.js'),
      'statistiques': path.resolve(__dirname, 'src', './scripts/admin/statistiques.js'),
      'outil_prevision': path.resolve(__dirname, 'src', './scripts/admin/outil/outil_prevision.js'),
      'outil_resume': path.resolve(__dirname, 'src', './scripts/admin/outil/outil_resume.js'),
    },
    output: {
      path: path.resolve(__dirname, '../server/public'),
      filename: 'scripts/[name]-bundle.[contenthash].js', // Ajout d'un hash basé sur le contenu
      clean: true, // Nettoie l'ancienne sortie avant de générer
    },
    mode: isProduction ? 'production' : 'development',
    devtool: isProduction ? false : 'source-map', // Source maps uniquement en mode développement
    optimization: {
      minimizer: isProduction ? [`...`, new CssMinimizerPlugin()] : [],
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'style/[name].[contenthash].css', // Génère les CSS avec hash
      }),
      ...[
        'index', 'inscription', 'sponsors', 'contact', 'news', 'espace_club', 'nos_nageurs', 
        'profil_nageur', 'resultats', 'login', 'galerie', 'admin', 
        'edit_groupes', 'edit_nageurs', 'edit_news', 'edit_performances', 
        'edit_resultats', 'edit_galerie', 'edit_album', 'statistiques', 
        'outil_prevision', 'outil_resume'
      ].map(name => 
        new HtmlWebpackPlugin({
          template: path.resolve(__dirname, 'src', 'html', `${name}.html`), // Assure-toi que les fichiers existent
          filename: `html/${name}.html`, // Génère le fichier HTML à l'endroit voulu
          chunks: [name]
        })
      ),
      new CopyPlugin({
        patterns: [
          {
            context: path.resolve(__dirname, 'src', 'images'),
            from: '**/*',
            to: 'images/[name][ext]',
            noErrorOnMissing: true,
          },
          {
            context: path.resolve(__dirname, 'src', 'style'),
            from: '**/*',
            to: 'style/[name][ext]',
            noErrorOnMissing: true,
          },
        ],
      }),
    ],    
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            MiniCssExtractPlugin.loader, // Extrait les CSS dans des fichiers séparés
            'css-loader',
          ],
        },
        {
          test: /\.(png|jpg|gif)/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[contenthash].[ext]', // Ajout du hash pour les fichiers image
                outputPath: 'images',
              },
            },
          ],
        },
      ],
    },
    devServer: {
      static: {
        publicPath: path.resolve(__dirname, '../server/public/'),
        watch: true,
      },
      host: 'localhost',
      port: 8888,
      open: true,
    },
  };
};