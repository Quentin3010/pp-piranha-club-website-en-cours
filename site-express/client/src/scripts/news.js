console.log("NEWS.JS A ETE LOAD")

import '../scripts/other/header-section.js';
import '../scripts/other/footer-section.js';
import '../style/other/footer-section.css';
import '../style/other/header-section.css';

document.body.style = 'background-image: url("../../images/fond_top.png"), url("../../images/fond_repeat.png");'

import '../style/other/general.css';
import '../style/other/font-size.css';

import '../style/news.css';

const generateNewsPreview2 = require('./other/news-function').generateNewsPreview2;
const getPreviewsExcept = require('./other/news-function').getPreviewsExcept;
const addPreviews = require('./other/news-function').addPreviews;
const getNews = require('./other/news-function').getNews;

// ADD VISITE
import '../scripts/other/add_visite.js';

////////////////////
// SCRIPT SECTION //
////////////////////

function getParameterByName(name) {
    const url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

/*
function getUrl(fullUrl) {
    const tab = fullUrl.split("/")
    return tab[tab.length-1]
}
*/

const nbPageToAddWhenRequest = 10;
let nbAdded = 0;
function checkAddEnoughNews(nb) {
    return nbPageToAddWhenRequest == nb;
}

function hideButtonMoreNews() {
    const more_news = document.getElementById("more-news")
    more_news.setAttribute("style", "display: none;")
}

function addButtonEventHandler() {
    const more_news = document.getElementById("more-news")
    more_news.addEventListener("click", async function () {
        const nbPreviewAdded = await addPreviews(nbPageToAddWhenRequest, nbAdded);
        nbAdded += nbPreviewAdded
        if (!checkAddEnoughNews(nbPreviewAdded)) {
            hideButtonMoreNews()
        }
        const previews = document.querySelectorAll(".text-preview>div")
        previews.forEach(function (preview) {
            preview.innerHTML = preview.textContent
        })
    });
}

function convertDateToText(date) {
    var nomsJours = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    var nomsMois = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];

    return `${nomsJours[date.getDay()]} ${date.getDate()} ${nomsMois[date.getMonth()]} ${date.getFullYear()}`
}

createPage(nbPageToAddWhenRequest);
async function createPage(nbPageToAdd) {
    const id = getParameterByName("id");
    const main_section = document.getElementById("main-section")
    if (id == "" || id == null || id == undefined || !(id % 1 == 0)) {
        /**
        * PAGE AVEC TOUTES LES NEWS
        */
        main_section.innerHTML = `
        <section>
        <div class="div3">
        <h1>News du club</h1>
        </div>
        <div id="news-section"></div>
        <button style="display: display;" id="more-news">Plus de news</button>
        </section>
        `
        const nbPreviewAdded = await addPreviews(nbPageToAdd, nbAdded);
        nbAdded += nbPreviewAdded
        if (!checkAddEnoughNews(nbPreviewAdded)) {
            hideButtonMoreNews()
        } else {
            addButtonEventHandler()
        }
        const previews = document.querySelectorAll(".text-preview>div")
        previews.forEach(function (preview) {
            preview.innerHTML = preview.textContent
        })
    } else {
        /**
        * PAGE CENTREE SUR UNE NEWS
        */
        const news = await getNews(id)
        if (news.length == 0) {
            window.location.href = "news.html";
        }
        
        const date_news = convertDateToText(new Date(news[0]["date_news"]));
        let titre = news[0]["titre"];
        const message = news[0]["message"];
        let miniature = news[0]["miniature"];
        if (miniature === "") {
            miniature = "./img/no_image.png";
        }

        let res = `
        <div id="news-section2">
        <div class="div3 news-section4">
        <div class="top-section">
        <img src="${miniature}">
        <div>
        <h2 class="h2-news">${titre}</h2>
        <p>Posté le ${date_news}</p>
        </div>
        </div>
        <div id="text-section">
        <p>${message}</p>`

        const photos = news[0]["photos"];
        if (photos != "" && photos != null) {  
            res += `</br>
            <div id="carouselExampleIndicators" class="carousel slide">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>`
                    const tabPhotos = news[0]["photos"].split("\n")
                    for (let i = 1; i < tabPhotos.length; i++){
                        res += `<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="${i}" aria-label="Slide ${i+1}"></button>`
                    }
                res += `
                </div>
                <div class="carousel-inner">`
                    for (let i = 0; i < tabPhotos.length; i++){
                        if (i == 0) {
                            res += `<div class="carousel-item active"><img src="${tabPhotos[i]}" class="d-block carousel-image" alt="${tabPhotos[i]}"></div>`
                        } else {
                            res += `<div class="carousel-item"><img src="${tabPhotos[i]}" class="d-block carousel-image" alt="${tabPhotos[i]}"></div>`
                        }
                        
                    }
                res += `
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>`
        }

        res += `</div>
        </div>
        </div>
        <div class="div3">
        <div id="news-section">
        <div id="news-section-title">
        News récentes
        </div>
        <div id="news-section3">
        
        </div>
        <div id="other-news">
        <button id="other-news-button">Les autres news</button>
        </div>
        </div>
        </div>
        </div>
        `

        main_section.innerHTML = res;
        const button = document.getElementById("other-news-button")
        button.addEventListener("click", function () {
            window.location.href = "news";
        });
        
        const previews = await getPreviewsExcept(2, id);
        const news_section3 = document.getElementById("news-section3")
        news_section3.innerHTML += generateNewsPreview2(previews[0]["id_news"], previews[0]["titre"], previews[0]["miniature"], previews[0]["date_news"], "left");
        news_section3.innerHTML += generateNewsPreview2(previews[1]["id_news"], previews[1]["titre"], previews[1]["miniature"], previews[1]["date_news"], "right");
    }  
}