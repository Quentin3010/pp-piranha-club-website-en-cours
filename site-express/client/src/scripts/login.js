console.log("LOGIN.JS A ETE LOAD")

import '../scripts/other/header-section.js';
import '../scripts/other/footer-section.js';
import '../style/other/footer-section.css';
import '../style/other/header-section.css';

document.body.style = 'background-image: url("../../images/fond_top.png"), url("../../images/fond_repeat.png");'

import '../style/other/general.css';
import '../style/other/font-size.css';

import '../style/login.css';

////////////////////
// SCRIPT SECTION //
////////////////////

const login_button = document.getElementById("login-button")
login_button.addEventListener("click", function () {
    event.preventDefault(); // Empêche la soumission du formulaire par défaut
    connexion();
})

async function connexion() {
    const login = document.getElementById("login").value
    const mdp = document.getElementById("mdp").value
    if (login == "" || mdp == "") {
        if (login == "") {
            document.getElementById("login").setAttribute("style", "border-color: red")
        }
        if (mdp == "") {
            document.getElementById("mdp").setAttribute("style", "border-color: red")
        }
    } else {
        fetch(`/api/login/connect/${login}/${mdp}`)
        .then(response => {
            if (response.status != "200") showToast()
            else window.location.href = "/admin"
        })
    }
}


function showToast() {
    var toast = document.getElementById("toast-connexion");
    toast.setAttribute("style", "display: block;");
    setTimeout(function () {
        toast.setAttribute("style", "display: none;");
    }, 5000);
}
