function showToast(text, state) {
    const div = document.createElement("div");
    div.innerHTML = text
    div.setAttribute("class", `toast-${state}`)
    document.getElementById("toast-section").appendChild(div);

    setTimeout(() => {
        document.getElementById("toast-section").removeChild(div);
    }, 5000);
}
module.exports.showToast = showToast