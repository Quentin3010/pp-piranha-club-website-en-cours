const headerSection = document.getElementById("header-section")
headerSection.innerHTML = `
<div id="bande">
    <div> 
    <a href="/admin"><button id="admin-button">Admin</button></a>
    </div>
</div>
<div id="bande2" class="bande2">
    <div class="bande2"> 
    <button class="bande2">Admin</button>
    </div>
</div>
<div id="header">
    <div id="img-logo-div">
        <img id="img-logo" alt="logo-club" src="../../images/logo2.png">
    </div>
    <div id="header-right-div">
        <div id="header-buttons"> 
            <a href="/"><button id="accueil-button">Accueil</button></a>
            <a href="/inscription"><button id="inscription-button">Inscription</button></a>
            <a href="/espace_club"><button id="club-button">Le Club</button></a>
            <a href="/galerie"><button id="galerie-button">Galerie</button></a>
            <a href="/sponsors"><button id="sponsors-button">Sponsors</button></a>
            <a href="/contact"><button id="contact-button">Contact</button></a>
        </div>
    </div>
</div>
`

const accueil_button = document.getElementById("accueil-button")
accueil_button.addEventListener("click", function () {
    window.location.href = "/";
})

const inscription_button = document.getElementById("inscription-button")
inscription_button.addEventListener("click", function () {
    window.location.href = "/inscription";
})

const club_button = document.getElementById("club-button")
club_button.addEventListener("click", function () {
    window.location.href = "/espace_club";
})

const galerie_button = document.getElementById("galerie-button")
galerie_button.addEventListener("click", function () {
    window.location.href = "/galerie";
})

const sponsors_button = document.getElementById("sponsors-button")
sponsors_button.addEventListener("click", function () {
    window.location.href = "/sponsors";
})

const contact_button = document.getElementById("contact-button")
contact_button.addEventListener("click", function () {
    window.location.href = "/contact";
})