fetch("https://api.ipify.org?format=json")
  .then(response => response.json())
  .then(data => {
    const ipAddress = data.ip;

    let url = window.location.href.split("/")
    url = url[url.length - 1]
    if (url == "") {
      url = "index.html"
    }
    const page = url.replace(/\.html/g, "");

    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ page: page, ip: ipAddress })
    };

    fetch(`/api/stats/visite/add`, requestOptions);
  })
  .catch(error => {
    console.error("Error fetching IP address:", error);
  });

