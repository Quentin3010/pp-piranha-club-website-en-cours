function stripTags(html) {
    // Créer une div temporaire
    var tempDiv = document.createElement("div");
    // Définir le contenu HTML
    tempDiv.innerHTML = html;
    // Récupérer et retourner le texte
    return tempDiv.textContent || tempDiv.innerText || "";
}

/**
 * Fonction qui génère les previews des news sur la page d'accueil
 */
function generateNewsPreview(id, titre, image, texte, date) {
    let img = image;
    if (img === "") {
        img = "./img/no_image.png";
    }
    const nbJour = getNbJourDepuisLaDate(date)

    return `
    <div class="news-preview">
        <div value="${id}">
            <div class="image-preview">
               <img src="${img}">
            </div>
            <div class="preview">
                <!--Title section-->
                <div class="title-section">
                    <div class="title">
                        <a href="news?id=${id}" class="link"> ${titre} </a>
                    </div>
                    <div class="time-elapsed">
                        ${convertJour(nbJour)}
                    </div>
                </div>
                <!--Message section-->
                <div class="text-preview">
                    <div class="italic">
                        ${stripTags(texte)}
                    </div>
                </div>
            </div>
        </div>
    </div>
    `;
}
module.exports.generateNewsPreview = generateNewsPreview

function generateNewsPreview2(id, titre, image, date, pos) {
    let img = image;
    if (img === "") {
        img = "./img/no_image.png";
    }
    const nbJour = getNbJourDepuisLaDate(date)

    return `
    <div class="news-preview ${pos}">
        <div value="${id}">
            <div class="image-preview">
               <img src="${img}">
            </div>
            <div class="preview">
                <div class="title2">
                    <a href="news?id=${id}" class="link"> ${titre} </a>
                </div>
                <div class="time-elapsed2">
                    ${convertJour(nbJour)}
                </div>
            </div>
        </div>
    </div>
    `;
}
module.exports.generateNewsPreview2 = generateNewsPreview2

function getNbJourDepuisLaDate(date) {
    const dateDonnee = new Date(date); // YYYY-MM-DD format
    const dateDuJour = new Date();

    const timestampDateDonnee = dateDonnee.getTime();
    const timestampDateDuJour = dateDuJour.getTime();

    const differenceEnMillisecondes = timestampDateDuJour - timestampDateDonnee;
    return differenceEnMillisecondes / (1000 * 3600 * 24);
}
module.exports.getNbJourDepuisLaDate = getNbJourDepuisLaDate

function convertJour(nbJour) {
    if (Math.floor(nbJour) == 0) {
        return `Aujourd'hui`
    }else if (Math.floor(nbJour) == 1) {
        return `Hier`
    }else if (nbJour <= 31) {
        return `Il y a ${Math.floor(nbJour)} jours`
    } else if (nbJour < 365) {
        return `Il y a ${Math.floor(nbJour/30)} mois`
    } else {
        return `Il y a ${Math.floor(nbJour/365)} an`
    }
}
module.exports.convertJour = convertJour

async function getPreviews(nb, nbAdded) {
    try {
        const response = await fetch(`/api/news/getPreviews/${nb}/${nbAdded}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data
}
module.exports.getPreviews = getPreviews

async function getPreviewsExcept(nb, exception) {
    try {
        const response = await fetch(`/api/news/getPreviewsExcept/${nb}/${exception}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data
}
module.exports.getPreviewsExcept = getPreviewsExcept

async function addPreviews(nb, nbAdded){
    const data = await getPreviews(nb, nbAdded);

    const news_section = document.getElementById("news-section")
    for (let i = 0; i < data.length; i++){
        news_section.innerHTML += generateNewsPreview(data[i]["id_news"], data[i]["titre"], data[i]["miniature"], data[i]["message"], data[i]["date_news"])
    }
    return data.length;
}
module.exports.addPreviews = addPreviews


async function getNews(id) {
    try {
        const response = await fetch(`/api/news/get/${id}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data
}
module.exports.getNews = getNews