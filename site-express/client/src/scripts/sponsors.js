console.log("SPONSORS.JS A ETE LOAD")

import '../scripts/other/header-section.js';
import '../scripts/other/footer-section.js';
import '../style/other/footer-section.css';
import '../style/other/header-section.css';

document.body.style = 'background-image: url("../../images/fond_top.png"), url("../../images/fond_repeat.png");'

import '../style/other/general.css';
import '../style/other/font-size.css';

import '../style/sponsors.css';

// ADD VISITE
import '../scripts/other/add_visite.js';

function create_sponsor_icon(link, image_src, name) {
    return `<li class="carousel-slide">
        <a href="${link}" target="_blank">
            <img src="${image_src}" alt="${name}_logo" class="image-hover">
        </a>
    </li>`

}

const carousel_track = document.querySelector(".carousel-track")
let div = create_sponsor_icon("https://drive.google.com/file/d/19exbLOQ4f0p5CdKPRSDTBNTsihiAU85i/view?usp=sharing", "../images/nataquashop.png", "nataquashop")
div += create_sponsor_icon("https://www.moneaucristaline.fr/", "../images/cristaline.png", "cristaline")
div += create_sponsor_icon("https://www.eau-rozana.com/", "../images/rozana.png", "rozana")
div += create_sponsor_icon("https://www.launchdiagnostics.com/", "../images/launch_diagnostic.png", "launch_diagnostics")
div += create_sponsor_icon("https://www.facebook.com/p/Space-Food-100075963321361/", "../images/space_food.png", "space_food")
div += create_sponsor_icon("https://www.ambulancesdelamorinie.fr/", "../images/ambulance_de_la_morinie.png", "ambulance_de_la_morinie")
div += create_sponsor_icon("https://www.garage-carrosserie-tatinghem.fr/", "../images/garage_nicolas_martin.png", "garage_nicolas_martin")
carousel_track.innerHTML = div

let currentIndex = 0;
const slides = document.querySelectorAll('.carousel-slide');
const totalSlides = slides.length;
const visibleSlides = 5;

function updateCarousel() {
    const track = document.querySelector('.carousel-track');
    const slideWidth = slides[0].offsetWidth;
    const newTransformValue = -slideWidth * currentIndex;
    track.style.transform = `translateX(${newTransformValue}px)`;
}

const prev = document.querySelector(".prev")
prev.addEventListener("click", () => {
    if (currentIndex > 0) {
        currentIndex--;
    } else {
        currentIndex = totalSlides - visibleSlides;
    }
    updateCarousel();
})

const next = document.querySelector(".next")
next.addEventListener("click", () => {
    if (currentIndex < totalSlides - visibleSlides) {
        currentIndex++;
    } else {
        currentIndex = 0;
    }
    updateCarousel();
})

function nextSlide() {
    if (currentIndex < totalSlides - visibleSlides) {
        currentIndex++;
    } else {
        currentIndex = 0;
    }
    updateCarousel();
}

// Initial call to set the correct position
updateCarousel();
