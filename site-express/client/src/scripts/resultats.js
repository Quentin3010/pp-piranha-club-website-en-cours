console.log("RESULTATS.JS A ETE LOAD")

import '../scripts/other/header-section.js';
import '../scripts/other/footer-section.js';
import '../style/other/footer-section.css';
import '../style/other/header-section.css';

document.body.style = 'background-image: url("../../images/fond_top.png"), url("../../images/fond_repeat.png");'

import '../style/other/general.css';
import '../style/other/font-size.css';

import '../style/resultats.css';

// ADD VISITE
import '../scripts/other/add_visite.js';

////////////////////
// SCRIPT SECTION //
////////////////////

function convertDate(date) {
    const data = date.split("-")
    return `${data[2]}/${data[1]}/${data[0]}`
}

async function generateListe() {
    let data = null
    try {
        const response = await fetch(`/api/resultat/getAll`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }

    let annee = quelAnneeSportive(data[0]["date_competition"])
    let res = `<table class="tab-res"><tr><th colspan=4>Résultats ${parseInt(annee)}-${parseInt(annee)+1}</th></tr></table>`
    for (let i = 0; i < data.length; i++){
        if(annee!=quelAnneeSportive(data[i]["date_competition"])){
            annee = quelAnneeSportive(data[i]["date_competition"])
            res += `</br><table class="tab-res"><tr><th colspan=4>Résultats ${parseInt(annee)}-${parseInt(annee)+1}</th></tr></table>`
        }
        if(data[i]["type_competition"].includes("Qualificative")) res += createRow1(data[i])
        else res += createRow2(data[i])
        console.log(data[i])
    }
    const div_resultats = document.getElementById("div-resultats")
    div_resultats.innerHTML = res + `</table>`
}

function quelAnneeSportive(date) {
    if (new Date(date) < new Date(`${date.split("-")[0]}-09-01`)) {
        return date.split("-")[0] - 1;
    } else {
        return date.split("-")[0];
    }
}

function createRow1(data){
    let dl1 = `<span class="no-res">Résultats Compétition</span>`
    if(data["lien_telechargement"]!="") dl1 = `<a class="dl" href="${data["lien_telechargement"]}">Résultats Compétition</a>`

    
    let dl2 = `<span class="no-res">Résultats NPDC</span>`
    if(data["lien_telechargement_npdc"]!="") dl2 = `<a class="dl" href="${data["lien_telechargement_npdc"]}">Résultats NPDC</a>`

    return `<table class="res-td"><tr>
    <td style="width:15% !important">${convertDate(data["date_competition"])}</td>
    <td style="width:35% !important">${data["type_competition"]}</td>
    <td style="width:25% !important">${dl1}</td>
    <td style="width:25% !important">${dl2}</td>
    </tr></table>`
}

function createRow2(data){
    let dl1 = `<span class="no-res">Résultats Compétition</span>`
    if(data["lien_telechargement"]!="") dl1 = `<a class="dl" href="${data["lien_telechargement"]}">Résultats Compétition</a>`

    return `<table class="res-td"><tr>
    <td style="width:15% !important">${convertDate(data["date_competition"])}</td>
    <td style="width:35% !important">${data["type_competition"]}</td>
    <td style="width:50% !important" colspan=2>${dl1}</td>
    </tr></table>`
}

generateListe();
