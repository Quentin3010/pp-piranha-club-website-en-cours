console.log("PROFIL_NAGEUR.JS A ETE LOAD")

import '../scripts/other/header-section.js';
import '../scripts/other/footer-section.js';
import '../style/other/footer-section.css';
import '../style/other/header-section.css';

document.body.style = 'background-image: url("../../images/fond_top.png"), url("../../images/fond_repeat.png");'

import '../style/other/general.css';
import '../style/other/font-size.css';

import '../style/profil_nageur.css';

// ADD VISITE
import '../scripts/other/add_visite.js';

////////////////////
// SCRIPT SECTION //
////////////////////

const video_div = document.getElementById("video-div")

/**
 * CARTE D'IDENTITE DU NAGEUR
 */
function getParameterByName(name) {
    const url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

const id = getParameterByName("id");
if (id == "" || id == null || id == undefined || !(id % 1 == 0)) {
    window.location.href = "espace_nageurs";
}

async function getInfoNageur() {
    let data = null
    try {
        const response = await fetch(`/api/nageur/get/${id}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data
}

async function getGroupe(){
    let data = "Aucun"
    try {
        const response = await fetch(`/api/nageur/getGroupe/${id}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data
}

async function generateProfilCard() {
    const info = await getInfoNageur();
    const groupe = await getGroupe();

    //RECUPERATION DES DONNEES
    if (info.length == 0) {
        //SI PAS DE DONNE POUR LE NAGEUR, ALORS ON RETOURNE A L'ESPACE NAGEUR
        window.location.href = "espace_nageurs";
    }
    const nom = info[0]["nom"];
    const prenom = info[0]["prenom"];
    let date_naiss = info[0]["date_naiss"];
    const sexe = info[0]["sexe"];
    let photo = info[0]["photo"];

    //CREATION DE LA CARTE DU NAGEUR
    const carte_nageur = document.getElementById("carte_nageur");
    carte_nageur.innerHTML = getCarte(nom, prenom, date_naiss, sexe, groupe, photo);
}

function getAge(birthDate) {
    const birth = new Date(birthDate);
    const today = new Date();
    
    let age = today.getFullYear() - birth.getFullYear();
    const monthDifference = today.getMonth() - birth.getMonth();

    if (monthDifference < 0 || (monthDifference === 0 && today.getDate() < birth.getDate())) {
        age--;
    }

    return age;
}

function getCarte(nom, prenom, date_naiss, sexe, groupe_actuelle, photo) {
    let img;
    if (photo == null || photo==="") img = "../images/default_profil.jpg";
    else img = `${photo}/${id}.jpeg`;

    let text_date = "";
    if (date_naiss == undefined || date_naiss == "") {
        text_date = "<h4>??/??/????</h4>"
    } else {
        text_date = `<h4>${convertToDate(date_naiss)} (${getAge(date_naiss)} ans)</h4>`
    }

    return `
    <div id="photo-section">
        <img src="${img}">
    </div>
    <div id="info-section">
        <h2 id="h2-prenom-nom">${prenom} ${nom}</h2>
        <div id="description-profil">
            ${text_date}
            <h4>${sexe}</h4>
            <h4>Groupe : ${groupe_actuelle}</h4>
        </div>
    </div>
`;
}

function convertToDate(date) {
    const data = date.split("-")
    if(data[0]=="") return ""
    if(data[2]=="01" && data[1]=="01") return `??/??/${data[0]}`
    return `${data[2]}/${data[1]}/${data[0]}`
}

/**
 * PERF DU NAGEUR
 */

function createRow(distance, dateCompet, competition, temps, temps50m, temps100m, categorie, lien_video) {
    let res = `<table class="perf"><tbody><tr>
        <td class="date-compet">${convertToDate(dateCompet)}</td>
        <td class="type-compet">${competition}</td>
        <td class="temps-compet">
            <span class="temps">${temps.replace(/:/g, "'")} &nbsp;ⓘ</span>
            ${create_tooltip(distance, temps, temps50m, temps100m)}
        </td>
    `;
    if (lien_video === undefined || lien_video === "") {
        res += `<td class="video-compet"><img class="logo-ytb" src="../images/ytb_gris.png"></td>`;
    } else {
        res += `<td class="video-compet"><a target="_blank" href="${lien_video}"><img class="logo-ytb logo_ok" src="../images/ytb_rouge.png"></a></td>`;
    }
    return res + "</tr></tbody></table>";
}

function create_tooltip(distance, temps, temps50m, temps100m){
    let res = "Temps passages :</br>"
    if(distance>50) res += `- 50m : ${temps50m !== "" ? temps50m.replace(/:/g, "'") : "NO DATA"}</br>`
    if(distance>100) res += `- 100m : ${temps100m !== "" ? temps100m.replace(/:/g, "'") : "NO DATA"}</br>`
    res += `- ${distance}m : ${temps.replace(/:/g, "'")}`
    return `<div class="info-box" style="display: none;">${res}</div>`
}

async function generetaTable(distance, nage) {
    const perf_section = document.getElementById("perf-section")
    const res = await createAllRow(nage, distance)
    perf_section.innerHTML = res
    createEventHandlerRegaderVideos()
    test()
}

function test(){
    document.addEventListener('click', function(event) {
        // Vérifiez si l'utilisateur a cliqué sur un temps
        const tempsElement = event.target.closest('.temps-compet');
        
        // Si on clique en dehors, cacher toutes les info-box
        document.querySelectorAll('.info-box').forEach(box => {
            box.style.display = 'none';
        });

        // Si un temps a été cliqué
        if (tempsElement) {
            const infoBox = tempsElement.querySelector(".temps").nextElementSibling; // Récupérer la boîte d'info
    
            // Basculer l'affichage de la boîte d'info
            if (infoBox.style.display === 'none' || infoBox.style.display === '') {
                infoBox.style.display = 'block'; // Afficher la boîte d'info
            } else {
                infoBox.style.display = 'none'; // Cacher la boîte d'info si déjà affichée
            }
    
            // Empêcher la fermeture de la boîte lors du clic sur elle
            event.stopPropagation(); 
        }
    });
}

async function getPerformance(nage, distance) {
    let data = null
    try {
        const response = await fetch(`/api/performance/get/${id}/${nage}/${distance}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data
}

async function createAllRow(nage, distance) {
    const perfs = await getPerformance(nage, distance)
    let res = ""
    if (perfs.length > 0) {
        for (let i = 0; i < perfs.length; i++) {
            res += createRow(distance, perfs[i]["date_competition"], perfs[i]["type_competition"], perfs[i]["temps"], perfs[i]["temps_50m"],
                perfs[i]["temps_100m"], perfs[i]["categorie_nageur"], perfs[i]["lien_video"])
        }
    } else {
        res += `<p class="center">Aucun résultat trouvé.</p>`
    }
    return res;
}

function generateButtonEventHandler() {
    document.getElementById('validate-button').addEventListener('click', function() {
        const distance = document.getElementById('distance-selector').value;
        const nage = document.getElementById('style-selector').value;
        
        generetaTable(distance, nage);
        test()
    });
}

function disableScroll() {
    // Récupère la largeur de la barre de défilement
    var scrollbarWidth = window.innerWidth - document.documentElement.clientWidth;
    // Récupère la position actuelle de la barre de défilement
    var scrollPosition = window.scrollY || window.pageYOffset;

    // Ajoute un style pour fixer la position de la barre de défilement et compenser la largeur
    document.body.style.position = 'fixed';
    document.body.style.width = 'calc(100% - ' + scrollbarWidth + 'px)';
    document.body.style.top = `-${scrollPosition}px`;
}

function enableScroll() {
    // Récupère la position fixée de la barre de défilement
    var scrollPosition = document.body.style.top;
    // Rétablit le défilement normal
    document.body.style.position = '';
    document.body.style.top = '';
    window.scrollTo(0, parseInt(scrollPosition || '0') * -1);
}

function createEventHandlerRegaderVideos(){
    const video_buttons = document.querySelectorAll(".video-button");
    video_buttons.forEach(video_button => {
        const value = video_button.getAttribute("value");
        video_button.addEventListener("click", function(event){
            event.preventDefault();
            generateVideoSection(value)
            disableScroll();
        });
    });
}

function extraireIdVideo(url) {
    var regex = /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
    var correspondance = url.match(regex);
    
    if (correspondance && correspondance[1]) {
        return correspondance[1];
    } else {
        return null;
    }
}

function generateVideoSection(lien){
    video_div.innerHTML = `<iframe id="video-player" src=https://www.youtube.com/embed/${extraireIdVideo(lien)}?vq=hd1080 frameborder="0" allowfullscreen></iframe>`
    video_div.setAttribute("style", "display:block");
}

async function fillTableRecords(){
    const data = await getRecords()
    for(let i in data){
        console.log(data[i].temps, data[i].date_competition.split("-")[0], data[i].distance, data[i].nage.replace(" ", "_"))
        document.getElementById(`${data[i].distance}-${data[i].nage.replace(" ", "_").toLowerCase()}`).innerHTML = `<div class="temps-record">${data[i].temps.replace(/:/g, "'")}</div><div class="annee-record">${data[i].date_competition.split("-")[0]}</div>`
    }
}

async function getRecords() {
    let data = null
    try {
        const response = await fetch(`/api/nageur/getRecords/${id}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data
}

//INITIALISATION DE LA PAGE
generateButtonEventHandler();
generateProfilCard();
fillTableRecords();
generetaTable(50, "Nage_Libre");
video_div.addEventListener("click", function () {
    video_div.setAttribute("style", "display:none")
    video_div.innerHTML = ""
    enableScroll();
})
