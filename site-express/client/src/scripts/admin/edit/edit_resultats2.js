console.log("EDIT_RESULTATS2.JS A ETE LOAD")
const showToast = require('../../other/toast').showToast;

const data_tab = document.getElementById("data-tab")
data_tab.innerHTML = `<tr>
<th class="th-id">Id</th>
<th>Type compétition</th>
<th>Date compétition</th>
<th>Lien téléchargement</th>
<th>Lien téléchargement NPDC</th>
<th class="th-buttons"></th>
</tr>`

add_nageurs_rows()
async function add_nageurs_rows() {
    try {
        const response = await fetch(`/api/resultat/getAll`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    const tab_id = await generate_nageur_row(data)
    generate_button_event(tab_id)
}

async function generate_nageur_row(data) {
    let res = ""
    let tab_id = []
    for (let i = 0; i < data.length; i++){
        res += `<tr><td>${data[i]["id_resultat"]}</td>`
        res += `<td><input type="text" id="td-type_competition-${data[i]["id_resultat"]}" value="${data[i]["type_competition"]}"></td>`
        res += `<td><input type="date" id="td-date_competition-${data[i]["id_resultat"]}" value="${data[i]["date_competition"]}"></td>`
        res += `<td><input type="text" id="td-lien_telechargement-${data[i]["id_resultat"]}" value="${data[i]["lien_telechargement"]}"></td>`
        res += `<td><input type="text" id="td-lien_telechargement-npdc-${data[i]["id_resultat"]}" value="${data[i]["lien_telechargement_npdc"]}"></td>`
        res += `<td><button id="edit-button-${data[i]["id_resultat"]}" class="buttons2 modif-res">Edit</button><button id="delete-button-${data[i]["id_resultat"]}" value="0" class="buttons2 suppr-ligne">X</button></td></tr>`
        tab_id.push(data[i]["id_resultat"])
    }
    data_tab.innerHTML += res
    return tab_id
}

function generate_button_event(tab_id) {
    for (const id of tab_id){
        const edit_button = document.getElementById(`edit-button-${id}`) 
        edit_button.addEventListener("click", function () {
            edit_resultat(id)
        })
        const delete_button = document.getElementById(`delete-button-${id}`) 
        delete_button.addEventListener("click", function () {
            if (delete_button.value == 0) {
                delete_button.value = 1
            } else {
                delete_resultat(id)
            }
        })
    }
}

async function delete_resultat(id_resultat) {
    const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({id: id_resultat})
    };

    const response = await fetch(`/api/resultat/delete`, requestOptions);
    console.log(response.status)
    if (response.status == 200) {
        window.location.href = "/admin/edit_resultats";
    }
}

async function edit_resultat(id) {
    const messageData = {
        id_resultat: id,
        type_competition: document.getElementById(`td-type_competition-${id}`).value,
        date_competition: document.getElementById(`td-date_competition-${id}`).value,
        lien_telechargement: document.getElementById(`td-lien_telechargement-${id}`).value,
        lien_telechargement_npdc: document.getElementById(`td-lien_telechargement-npdc-${id}`).value
    }
    
    const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(messageData)
    };

    const response = await fetch(`/api/resultat/update`, requestOptions);
    if (response.status == 200) {
        showToast("Edit réussi !", "success")
    }
}