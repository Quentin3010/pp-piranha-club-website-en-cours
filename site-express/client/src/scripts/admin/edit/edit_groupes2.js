console.log("EDIT_GROUPES2.JS A ETE LOAD")

const data_tab = document.getElementById("data-tab")
data_tab.innerHTML = `<tr>
<th class="th-small">Id</th>
<th>Nom</th>
<th>Prenom</th>
<th>Groupe</th>
<th>Année</th>
<th class="th-small"></th>
</tr>`

add_nageurs_rows()
async function add_nageurs_rows() {
    try {
        const response = await fetch(`/api/groupe/getAllAssociation`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    const tab_id = await generate_nageur_row(data)
    generate_button_event(tab_id)
}

async function generate_nageur_row(data) {
    let res = ""
    let tab_id = []
    for (let i = 0; i < data.length; i++){
        res += `<tr><td>${data[i]["id_association"]}</td>`
        res += `<td>${data[i]["nom"]}</td>`
        res += `<td>${data[i]["prenom"]}</td>`
        res += `<td>${data[i]["nom_groupe"]}</td>`
        res += `<td>${data[i]["annee"]}</td>`
        res += `<td><button id="delete-button-${data[i]["id_association"]}" value="0" class="buttons2 suppr-ligne">X</button></tr>`
        tab_id.push(data[i]["id_association"])
    }
    data_tab.innerHTML += res
    return tab_id
}

function generate_button_event(tab_id) {
    for (const id of tab_id){
        const delete_button = document.getElementById(`delete-button-${id}`) 
        delete_button.addEventListener("click", function () {
            if (delete_button.value == 0) {
                delete_button.value = 1
            } else {
                delete_association(id)
            }
        })
    }
}

async function delete_association(id) {
    const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({id: parseInt(id)})
    };

    const response = await fetch(`/api/groupe/deleteAssociation`, requestOptions);
    if (response.status == 200) {
        window.location.href = "/admin/edit_groupes";
    }
}