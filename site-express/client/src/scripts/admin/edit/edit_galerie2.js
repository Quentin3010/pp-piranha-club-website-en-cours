console.log("EDIT_GALERIE2.JS A ETE LOAD");

const liste_albums = document.getElementById("liste-albums");

// Fonction pour créer le HTML des prévisualisations d'albums
function createAlbumPreviewHTML(previews) {
    let html = "";
    previews.forEach(preview => {
        const folderName = preview.folderName;
        const displayName = folderName.replace(/_/g, " ");
        const firstImage = preview.firstImage;
        
        html += `
            <div class="div3 album-div" value="${folderName}">
                <img value="${folderName}" alt="${folderName}" src="/uploads/albums/${folderName}/${firstImage}">
                <div class="title-section">
                    <input placeholder="Nom de l'album" id="input-${folderName}" class="album_name" value="${displayName}">
                    <button class="valider" valider="${folderName}">💾</button>
                </div>
                <button class="supprimer" supprimer="${folderName}">Supprimer</button>
            </div>`;
    });
    return html;
}

// Fonction pour charger les prévisualisations d'albums depuis le serveur
async function loadPreviews() {
    try {
        const response = await fetch('/photos/album/getPreviews');
        if (!response.ok) {
            alert('Erreur lors de la récupération des prévisualisations');
        }
        if(response.status==204) return
        
        const previews = await response.json();
        const previewsHTML = createAlbumPreviewHTML(previews);
        liste_albums.innerHTML += previewsHTML;
        
        // Ajouter les gestionnaires d'événements une fois que les prévisualisations sont chargées
        addEventListeners();
    } catch (error) {
        console.error(error);
        liste_albums.innerHTML += `<p>Pas d'album sur le serveur...</p>`;
    }
}

// Fonction pour ajouter les gestionnaires d'événements aux éléments de l'interface utilisateur
function addEventListeners() {
    liste_albums.addEventListener('click', async function (event) {
        const albumImg = event.target.closest('div img[value]');
        if (albumImg) {
            const folderName = albumImg.getAttribute('value');
            navigateToEditAlbum(folderName);
        }

        const albumButtonValider = event.target.closest('div div button[valider]');
        if (albumButtonValider) {
            const folderName = albumButtonValider.getAttribute('valider');
            const newFolderName = document.getElementById(`input-${folderName}`).value.replace(/ /g, "_");
            await renameAlbum(folderName, newFolderName);
        }

        const albumButtonSupprimer = event.target.closest('div button[supprimer]');
        if (albumButtonSupprimer) {
            const folderName = albumButtonSupprimer.getAttribute('supprimer');
            await deleteAlbum(folderName);
        }
    });
}

// Fonction pour naviguer vers la page d'édition d'un album
function navigateToEditAlbum(folderName) {
    window.location.href = `/admin/edit_album?album=${folderName}`;
}

// Fonction pour renommer un album
async function renameAlbum(oldFolderName, newFolderName) {
    try {
        const response = await fetch('/photos/album/renameFolder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ oldName: oldFolderName, newName: newFolderName })
        });
    
        if (!response.ok) {
            alert('Erreur lors du renommage du dossier');
        }
    } catch (error) {
        console.error(error);
        alert('Erreur lors du renommage du dossier');
    }
}

// Fonction pour supprimer un album
async function deleteAlbum(folderName) {
    try {
        const response = await fetch('/photos/album/deleteFolder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ folderName })
        });
    
        if (!response.ok) {
            alert('Erreur lors de la suppression du dossier');
        }
        window.location.href = `/admin/edit_galerie`;
    } catch (error) {
        console.error(error);
        alert('Erreur lors de la suppression du dossier');
    }
}

// Appeler la fonction pour charger les prévisualisations d'albums au chargement de la page
loadPreviews();

