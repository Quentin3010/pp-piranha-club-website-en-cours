console.log("EDIT_RESULTATS1.JS A ETE LOAD")

const add_resultat_button = document.getElementById("add-res");
add_resultat_button.addEventListener("click", function () {
  const messageData = {
    type_competition: document.querySelector(".add_res>.liste_type_competition")
      .value,
    date_competition: document.querySelector(".add_res>.date-competition")
      .value,
    lien_telechargement: document.querySelector(".add_res>.lien_resultat")
      .value,
  };
  if (
    messageData.type_competition != "" &&
    messageData.date_competition != "" &&
    messageData.lien_telechargement != ""
  ) {
    addNewResultat(messageData);
  } else {
    showToast("Résultat invalide", "echec");
  }
});

async function addNewResultat(messageData) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(messageData),
  };

  const response = await fetch(`/api/resultat/add`, requestOptions);
  if (response.status == 200) {
    window.location.href = "/admin/edit_resultats";
  }
}

updateTypeCompetition();
async function updateTypeCompetition() {
    const allType = await getAllType();

    const liste_type_competition = document.querySelectorAll(".liste_type_competition")
    for (let i = 0; i < liste_type_competition.length; i++) {
        liste_type_competition[i].innerHTML = `<option value=""></option>`;
        for (let j = 0; j < allType.length; j++){
            liste_type_competition[i].innerHTML += `<option value="${allType[j]}">${allType[j]}</option>`;
        }
    }
}

async function getAllType() {
    try {
        const response = await fetch(`/api/competition/getAll`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data;
}