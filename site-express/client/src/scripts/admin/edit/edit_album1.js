console.log("EDIT_ALBUM1.JS A ETE LOAD");

const album = new URLSearchParams(window.location.search).get('album');
const nbFile = 32;
let idx = 0;
let nb_file_added = 0;

// Chargement initial de l'album
async function init() {
    if (album !== undefined) {
        await loadAlbum(album);
        await loadInitialPhotos(album);

        // Écouteur d'événement pour charger plus de photos au scroll
        window.addEventListener('scroll', async () => {
            if (nb_file_added < nbFile) return; // La galerie est totalement chargée

            if ((window.innerHeight + window.scrollY + 20) >= document.body.offsetHeight) {
                addPhotosToDOM(await loadMorePhotos(album, nbFile, idx), album);
            }
        });
    }
}

// Fonction pour charger l'album initial
async function loadAlbum(folderName) {
    const mainSection = document.getElementById('main-section');
    mainSection.innerHTML += `
        <div class="div3"><h1>Album : ${folderName.replace(/_/g, " ")}</h1></div>
        <div class="div3">
            <div id="photos-section"></div>
        </div>
    `;
}

// Fonction pour charger les premières photos de l'album
async function loadInitialPhotos(folderName) {
    addPhotosToDOM(await loadMorePhotos(folderName, nbFile, idx), folderName);
}

// Fonction pour charger plus de photos
async function loadMorePhotos(folderName, nbFiles, startIndex) {
    const response = await fetch(`/photos/album/getPhotos/${folderName}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ nbFile: nbFiles, idx: startIndex })
    });

    if (!response.ok) {
        window.location.href = `/edit_galerie`;
    }

    const data = await response.json();
    idx += data.nbPhoto;
    nb_file_added = data.nbPhoto;

    return data.photos;
}

// Fonction pour ajouter les photos au DOM
function addPhotosToDOM(photosList, folderName) {
    const photosSection = document.getElementById("photos-section");
    let photosHTML = '';

    photosList.forEach(photo => {
        const photoValue = `${folderName}/${photo}`;
        photosHTML += `
            <div class="div3 image_fullscreen">
                <div>
                    <img alt="${photoValue}" value="${photoValue}" src="/uploads/albums/${photoValue}">
                </div>
                <button class="photo-button" data-value="${photoValue}">Supprimer</button>
            </div>
        `;
    });

    photosSection.innerHTML += photosHTML;

    // Ajout des gestionnaires d'événements pour les boutons de suppression
    const photoButtons = document.querySelectorAll('.photo-button');
    photoButtons.forEach(button => {
        button.addEventListener('click', async (event) => {
            const response = await fetch('/photos/album/deletePhoto', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ file_path: event.target.getAttribute('data-value') })
            });

            if (response.status === 200) {
                const parentDiv = event.target.closest('.div3');
                if (parentDiv) {
                    parentDiv.remove();
                }
            } else if (response.status === 204) {
                window.location.href = `/admin/edit_galerie`;
            } else {
                alert("Erreur lors de la suppression");
            }
        });
    });
}

// Appel à la fonction d'initialisation
init();
