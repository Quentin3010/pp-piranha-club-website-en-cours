console.log("EDIT_NEWS3.JS A ETE LOAD")

function convertDateToText(date) {
  var nomsJours = [
    "Dimanche",
    "Lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi",
  ];
  var nomsMois = [
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Août",
    "Septembre",
    "Octobre",
    "Novembre",
    "Décembre",
  ];

  return `${nomsJours[date.getDay()]} ${date.getDate()} ${
    nomsMois[date.getMonth()]
  } ${date.getFullYear()}`;
}

function generatePreview(news) {
  const date_news = convertDateToText(new Date());

  let titre = news[0]["titre"];
  const message = news[0]["message"];
  let miniature = news[0]["miniature"];
  if (miniature === "") {
    miniature = "../../../images/no_image.png";
  }

  let res = `
        <div id="news-section2">
        <div class="div3">
          <h3>Preview de la News</h3>
        </div>
        <div class="div3 news-section4">
        <div class="top-section">
        <img src="${miniature}">
        <div>
        <h2 class="h2-news">${titre}</h2>
        <p>Posté le ${date_news}</p>
        </div>
        </div>
        <div id="text-section">
        <p>${message}</p>`;

  if (news[0]["photos"] != "") {
    res += `</br>
    <div id="carouselExampleIndicators" class="carousel slide">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>`;
    const tabPhotos = news[0]["photos"].split("\n");
    for (let i = 1; i < tabPhotos.length; i++) {
      res += `<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="${i}" aria-label="Slide ${
        i + 1
      }"></button>`;
    }
    res += `
        </div>
        <div class="carousel-inner">`;
    for (let i = 0; i < tabPhotos.length; i++) {
      if (i == 0) {
        res += `<div class="carousel-item active"><img src="${tabPhotos[i]}" class="d-block carousel-image" alt="${tabPhotos[i]}"></div>`;
      } else {
        res += `<div class="carousel-item"><img src="${tabPhotos[i]}" class="d-block carousel-image" alt="${tabPhotos[i]}"></div>`;
      }
    }
    res += `
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>`;
  }

  const preview_section = document.getElementById("right-box");
  preview_section.innerHTML = res;
}
module.exports.generatePreview = generatePreview

const default_news = [
  {
    titre: "Titre",
    message: "Message",
    miniature: "",
    photos: "",
    date_news: new Date().getTime(),
  },
];

generatePreview(default_news);
