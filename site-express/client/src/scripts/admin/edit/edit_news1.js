console.log("EDIT_NEWS1.JS A ETE LOAD")
const showToast = require('../../other/toast').showToast;
const news_preview = require("./edit_news4").news_preview

const button_post_news = document.getElementById("post_message");
button_post_news.addEventListener("click", function () {
  if (
    news_preview[0]["titre"] != "Titre" &&
    news_preview[0]["message"] != "Message" &&
    news_preview[0]["miniature"] != ""
  ) {
    createPost(news_preview[0]);
  } else {
    showToast("News invalide", "echec")
  }
});

async function createPost(messageData) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(messageData),
  };

  const response = await fetch(`/api/news/add`, requestOptions);
  if (response.status == 201 || response.status == 200) {
    window.location.href = "/admin/edit_news";
  }
}
