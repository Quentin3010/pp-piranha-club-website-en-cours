console.log("EDIT_PERFORMANCES4.JS A ETE LOAD")
const showToast = require("../../other/toast").showToast

const data_tab = document.getElementById("modif-perf-table")

let actual_type_competition = ""

add_nageurs_rows()
async function add_nageurs_rows() {
    try {
        const response = await fetch(`/api/performance/getAll`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    
    const tab_id = await generate_nageur_row(data)
    generate_button_event(tab_id)
}

async function generate_nageur_row(data) {
    let res = ""
    let tab_id = []
    for (let i = 0; i < data.length; i++){
        if (actual_type_competition != data[i]["type_competition"]) {
            res += `<tr><th class="separation-type-competition" colspan="12">${data[i]["type_competition"]}</th></tr>`
            actual_type_competition = data[i]["type_competition"]
        }

        res += `<tr id="tr-perf${data[i]["id_performance"]}" class="perf-ligne tr-${data[i]["sexe"]}"><td>${data[i]["id_performance"]}</td>`
        res += `<td class="td-nom">${data[i]["nom_complet"]}</td>`
        res += `<td><input type="text" id="td-categorie_nageur-${data[i]["id_performance"]}" value="${data[i]["categorie_nageur"]}"></td>`
        res += `<td><input type="text" id="td-nage-${data[i]["id_performance"]}" value="${data[i]["nage"]}"></td>`
        res += `<td><input type="number" id="td-distance-${data[i]["id_performance"]}" value="${data[i]["distance"]}"></td>`
        res += `<td><input type="text" id="td-temps-${data[i]["id_performance"]}" value="${data[i]["temps"]}"></td>`
        res += `<td><input type="text" id="td-temps_50m-${data[i]["id_performance"]}" value="${data[i]["temps_50m"]}"></td>`
        res += `<td><input type="text" id="td-temps_100m-${data[i]["id_performance"]}" value="${data[i]["temps_100m"]}"></td>`
        res += `<td><input type="date" id="td-date_competition-${data[i]["id_performance"]}" value="${data[i]["date_competition"]}"></td>`
        res += `<td><input type="text" id="td-type_competition-${data[i]["id_performance"]}" value="${data[i]["type_competition"]}"></td>`
        res += `<td><input type="text" id="td-lien_video-${data[i]["id_performance"]}" value="${data[i]["lien_video"]}"></td>`
        res += `<td class="buttons-perf"><button id="edit-button-${data[i]["id_performance"]}" class="buttons3 modif-perf">Edit</button><button id="delete-button-${data[i]["id_performance"]}" value="0" class="buttons3 suppr-ligne">X</button></td></tr>`
        tab_id.push(data[i]["id_performance"])
    }
    data_tab.innerHTML += res
    return tab_id
}

function generate_button_event(tab_id) {
    for (const id of tab_id){
        const edit_button = document.getElementById(`edit-button-${id}`) 
        edit_button.addEventListener("click", function () {
            edit_performance(id)
        })
        const delete_button = document.getElementById(`delete-button-${id}`) 
        delete_button.addEventListener("click", function () {
            if (delete_button.value == 0) {
                delete_button.value = 1
            } else {
                delete_performance(id)
            }
        })
    }
}

async function delete_performance(id_performance) {
    const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({id: parseInt(id_performance)})
    };

    const response = await fetch(`/api/performance/delete`, requestOptions);
    if (response.status == 200) {
        const tr = document.getElementById(`tr-perf${id_performance}`)
        tr.remove()
    }
}

async function edit_performance(id) {
    const messageData = {
        id_performance: id,
        categorie_nageur: document.getElementById(`td-categorie_nageur-${id}`).value,
        nage: document.getElementById(`td-nage-${id}`).value,
        distance: document.getElementById(`td-distance-${id}`).value,
        temps: document.getElementById(`td-temps-${id}`).value,
        temps_50m: document.getElementById(`td-temps_50m-${id}`).value,
        temps_100m: document.getElementById(`td-temps_100m-${id}`).value,
        date_competition: document.getElementById(`td-date_competition-${id}`).value,
        type_competition: document.getElementById(`td-type_competition-${id}`).value,
        lien_video : document.getElementById(`td-lien_video-${id}`).value
    }
    
    const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(messageData)
    };

    const response = await fetch(`/api/performance/update`, requestOptions);
    if (response.status == 200) {
        showToast("Edit réussi !", "success")
    }
}