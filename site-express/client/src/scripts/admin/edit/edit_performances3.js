console.log("EDIT_PERFORMANCES3.JS A ETE LOAD")

const add_perf_button = document.getElementById("add-perfs")

async function getId(nom, prenom) {
    try {
        const response = await fetch(`/api/nageur/getId/${nom}/${prenom}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data
}

add_perf_button.addEventListener("click", async function () {
    const type_competition = document.querySelector(".add_perf>.liste_type_competition").value
    const date_competition = document.querySelector(".add_perf>.date-competition").value

    if (type_competition == "" || date_competition == "") {
        return
    }

    const distance = document.querySelectorAll(".add_perf>.distance")
    const nage = document.querySelectorAll(".add_perf>.nage")
    const nom = document.querySelectorAll(".add_perf>.nom")
    const prenom = document.querySelectorAll(".add_perf>.prenom")
    const categorie = document.querySelectorAll(".add_perf>.categorie")
    const temps_1 = document.querySelectorAll(".temps_final_1")
    const temps_2 = document.querySelectorAll(".temps_final_2")
    const temps_3 = document.querySelectorAll(".temps_final_3")
    const temps_50m_1 = document.querySelectorAll(".temps_50m_1")
    const temps_50m_2 = document.querySelectorAll(".temps_50m_2")
    const temps_50m_3 = document.querySelectorAll(".temps_50m_3")
    const temps_100m_1 = document.querySelectorAll(".temps_100m_1")
    const temps_100m_2 = document.querySelectorAll(".temps_100m_2")
    const temps_100m_3 = document.querySelectorAll(".temps_100m_3")
    //const temps_200m_1 = document.querySelectorAll(".temps_200m_1")
    //const temps_200m_2 = document.querySelectorAll(".temps_200m_2")
    //const temps_200m_3 = document.querySelectorAll(".temps_200m_3")
    const l_video = document.querySelectorAll(".l_video")
    const uuids = document.querySelectorAll(".suppr-ligne")

    for (let i = 0; i < nom.length; i++) {
        suppError(nom[i], prenom[i], temps_1[i], temps_2[i], temps_3[i], distance[i], nage[i], categorie[i])

        if (nom[i].value=="" || prenom[i].value=="") {
            nom[i].classList.add("font-color-red");
            prenom[i].classList.add("font-color-red");
        }else {
            let idNageur = (await getId(nom[i].value, prenom[i].value))[0];
            if (nom[i].value == "" || prenom[i].value == "" || idNageur == -1) {
                //LE NAGEUR N'EXISTE PAS
                nom[i].classList.add("font-color-red");
                prenom[i].classList.add("font-color-red");
            } else {
                const temps50m = createTemps(temps_50m_1[i].value, temps_50m_2[i].value, temps_50m_3[i].value);
                const temps100m = createTemps(temps_100m_1[i].value, temps_100m_2[i].value, temps_100m_3[i].value);
                //const temps200m = createTemps(temps_200m_1[i].value, temps_200m_2[i].value, temps_200m_3[i].value);
    
                const messageData = {
                    id_nageur: idNageur,
                    categorie_nageur: categorie[i].value,
                    nage: nage[i].value,
                    distance: distance[i].value,
                    temps: temps_1[i].value+":"+temps_2[i].value+":"+temps_3[i].value,
                    temps_50m: temps50m,
                    temps_100m: temps100m,
                    //temps_200m: temps200m,
                    type_competition: type_competition,
                    date_competition: date_competition,
                    lien_video: l_video[i].value || ""
                };

                if (temps_1[i].value != "" && temps_2[i].value !="" && temps_3[i].value !="" && distance[i].value != "" &&
                    nage[i].value != "" && categorie[i].value != "") {
                    addPerformance(messageData)
                    const ligne = document.getElementById(`ligne-${uuids[i].value}`);
                    ligne.remove();
                } else {
                    showError(temps_1[i], temps_2[i], temps_3[i], distance[i], nage[i], categorie[i])
                }
            }
        }
        
    }
})

function showError(temps_1, temps_2, temps_3, distance, nage, categorie) {
    if(temps_1.value=="") temps_1.classList.add("font-color-red")
    if(temps_2.value=="") temps_2.classList.add("font-color-red")
    if(temps_3.value=="") temps_3.classList.add("font-color-red")
    if(distance.value=="") distance.classList.add("font-color-red")
    if(nage.value=="") nage.classList.add("font-color-red")
    if(categorie.value=="") categorie.classList.add("font-color-red")
}

function suppError(nom,prenom,temps_1, temps_2, temps_3, distance, nage, categorie) {
    nom.classList.remove("font-color-red");
    prenom.classList.remove("font-color-red");
    temps_1.classList.remove("font-color-red")
    temps_2.classList.remove("font-color-red")
    temps_3.classList.remove("font-color-red")
    distance.classList.remove("font-color-red")
    nage.classList.remove("font-color-red")
    categorie.classList.remove("font-color-red")
}

function createTemps(minutes, secondes, centisecondes) {
    if (minutes=="" && secondes=="" && centisecondes=="") return ""
    else return minutes+":"+secondes+":"+centisecondes
}

async function addPerformance(messageData) {
    const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(messageData)
    };
    
    await fetch(`/api/performance/add`, requestOptions);
} 

