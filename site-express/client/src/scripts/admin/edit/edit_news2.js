console.log("EDIT_NEWS2.JS A ETE LOAD")
const showToast = require('../../other/toast').showToast;

const data_tab = document.getElementById("modif-news-section")

add_nageurs_rows()
async function add_nageurs_rows() {
    try {
        const response = await fetch(`/api/news/getAll`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    const tab_id = await generate_news_row(data)
    generate_button_event(tab_id)
}

async function generate_news_row(data) {
    let res = ""
    let tab_id = []
    for (let i = 0; i < data.length; i++){
        res += `<div class="div3 modif-news-section">`
        res += `<div class="modif-news-section-first">`
        //res += `<p>ID : ${data[i]["id_news"]}</p>`
        res += `<p><input placeholder="Titre" class="modif-titre" type="texte" id="td-titre-${data[i]["id_news"]}" value="${data[i]["titre"]}">`
        res += `<input class="modif-date" type="date" id="td-date_news-${data[i]["id_news"]}" value="${data[i]["date_news"]}"></p>`
        res += `<p><input placeholder="URL de la miniature" type="texte" id="td-miniature-${data[i]["id_news"]}" value="${data[i]["miniature"]}"></p>`
        res += `<p><textarea placeholder="Message" rows="8" id="td-message-${data[i]["id_news"]}">${data[i]["message"]}</textarea></p>`
        res += `<textarea rows="2" id="td-photos-${data[i]["id_news"]}" class="texte" rows="1" cols="50" placeholder='URL vers les photos (optionnel). Une URL par ligne !!'>${data[i]["photos"]}</textarea>`
        res += `</div>`
        res += `<div class="modif-news-section-second">`
        res += `<button id="edit-button-${data[i]["id_news"]}" class="edit-button">Edit</button>`
        res += `<button id="delete-button-${data[i]["id_news"]}" class="suppr-button">X</button>`
        res += `</div>`
        res += `</div>`
        tab_id.push(data[i]["id_news"])
    }
    data_tab.innerHTML += res
    return tab_id
}

function generate_button_event(tab_id) {
    for (const id of tab_id){
        const edit_button = document.getElementById(`edit-button-${id}`) 
        edit_button.addEventListener("click", function () {
            edit_news(id)
        })
        const delete_button = document.getElementById(`delete-button-${id}`) 
        delete_button.addEventListener("click", function () {
            if (delete_button.value == 0) {
                delete_button.value = 1
            } else {
                delete_news(id)
            }
        })
    }
}

async function delete_news(id_resultat) {
    const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({id: id_resultat})
    };

    const response = await fetch(`/api/news/delete`, requestOptions);
    console.log(response.status)
    if (response.status == 200) {
        window.location.href = "/admin/edit_news";
    }
}

async function edit_news(id) {
    const messageData = {
        id_news: id,
        date_news: document.getElementById(`td-date_news-${id}`).value,
        titre: document.getElementById(`td-titre-${id}`).value,
        message: document.getElementById(`td-message-${id}`).value,
        miniature: document.getElementById(`td-miniature-${id}`).value,
        photos: document.getElementById(`td-photos-${id}`).value
    }
    
    const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(messageData)
    };

    const response = await fetch(`/api/news/update`, requestOptions);
    if (response.status == 200) {
        showToast("Edit de la news !", "success")
    }
}