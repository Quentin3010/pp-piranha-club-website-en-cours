console.log("EDIT_PERFORMANCES1.JS A ETE LOAD")

updateTypeCompetition();
async function updateTypeCompetition() {
    const allType = await getAllType();

    const liste_type_competition = document.querySelectorAll(".liste_type_competition")
    for (let i = 0; i < liste_type_competition.length; i++) {
        liste_type_competition[i].innerHTML = `<option value=""></option>`;
        for (let j = 0; j < allType.length; j++){
            liste_type_competition[i].innerHTML += `<option value="${allType[j]}">${allType[j]}</option>`;
        }
    }
}

async function getAllType() {
    try {
        const response = await fetch(`/api/competition/getAll`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data;
}

async function add_new_ligne(nom, prenom, categorie, nage, distance, temps_final, passage50m, passage100m, passage200m) {
    const add_perf_table = document.getElementById("add-perf-table")
    const uuid = uuidv4();

    add_perf_table.innerHTML += `<tr id="ligne-${uuid}" class="perf-ligne">
    <td class="add_perf td-nom-prenom">
        <input class="nom" type="text" placeholder="Nom" value="${nom}">
        <input class="prenom type="text" placeholder="Prenom" value=${prenom}>
    </td>
    <td class="add_perf">
        ${generateCategorieSelect(categorie)}
    </td>
    <td class="add_perf">
        ${generateNageSelect(nage)}
    </td>
    <td class="add_perf">
        ${generateDistanceSelect(distance)}
    </td>
    <td class="add_perf temps_final">
        <input class="temps temps_final_1" type="text" inputmode="numeric" pattern="[0-9]*" value="${temps_final[0]}"> :
        <input class="temps temps_final_2" type="text" inputmode="numeric" pattern="[0-9]*" value="${temps_final[1]}"> :
        <input class="temps temps_final_3" type="text" inputmode="numeric" pattern="[0-9]*" value="${temps_final[2]}">
    </td>
    <td class="add_perf temps_50m">
        <input class="temps temps_50m_1" type="text" inputmode="numeric" pattern="[0-9]*" value="${passage50m[0]}"> :
        <input class="temps temps_50m_2" type="text" inputmode="numeric" pattern="[0-9]*" value="${passage50m[1]}"> :
        <input class="temps temps_50m_3" type="text" inputmode="numeric" pattern="[0-9]*" value="${passage50m[2]}">
    </td>
    <td class="add_perf temps_100m">
        <input class="temps temps_100m_1" type="text" inputmode="numeric" pattern="[0-9]*" value="${passage100m[0]}"> :
        <input class="temps temps_100m_2" type="text" inputmode="numeric" pattern="[0-9]*" value="${passage100m[1]}"> :
        <input class="temps temps_100m_3" type="text" inputmode="numeric" pattern="[0-9]*" value="${passage100m[2]}">
    </td>
    <td class="add_perf">
        <input class="l_video" type="text" placeholder="Lien Youtube">
    </td>
    <td class="td-suppr">
        <button id="suppr-ligne" value="${uuid}" class="suppr-ligne">X</button>
    </td>
    </tr>`
}

function generateDistanceSelect(selectedDistance) {
    const distances = ["", "50", "100", "200", "400", "800"];

    const selectElement = document.createElement("select");
    selectElement.classList.add("distance");
    selectElement.setAttribute("required", "true");

    for (const distance of distances) {
        const optionElement = document.createElement("option");
        optionElement.value = distance;
        optionElement.textContent = distance === "" ? "" : `${distance}m`;

        if (distance === selectedDistance) {
            optionElement.setAttribute("selected", "true");
        }

        selectElement.appendChild(optionElement);
    }

    return selectElement.outerHTML;
}

function generateNageSelect(selectedNage) {
    const nages = ["", "Nage Libre", "Dos", "Brasse", "Papillon", "4 nages"];

    const selectElement = document.createElement("select");
    selectElement.classList.add("nage");
    selectElement.setAttribute("required", "true");

    for (const nage of nages) {
        const optionElement = document.createElement("option");
        optionElement.value = nage;
        optionElement.textContent = nage === "" ? "" : nage;

        if (nage === selectedNage) {
            optionElement.setAttribute("selected", "true");
        }

        selectElement.appendChild(optionElement);
    }

    return selectElement.outerHTML;
}

function generateCategorieSelect(selectedCategorie) {
    const categories = ["", "Mini-Poussin", "Poussin", "Benjamin", "Minime", "Cadet", "Junior", "Senior", "Master"];

    const selectElement = document.createElement("select");
    selectElement.classList.add("categorie");
    selectElement.setAttribute("required", "true");

    for (const categorie of categories) {
        const optionElement = document.createElement("option");
        optionElement.value = categorie;
        optionElement.textContent = categorie === "" ? "" : categorie;

        if (categorie === selectedCategorie) {
            optionElement.setAttribute("selected", "true");
        }

        selectElement.appendChild(optionElement);
    }

    return selectElement.outerHTML;
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0,
            v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

async function generate_event_handler() {
    const buttons = document.querySelectorAll(".suppr-ligne");
    buttons.forEach(button => {
        button.addEventListener("click", function () {
            const ligne = document.getElementById(`ligne-${button.value}`);
            ligne.remove();
        });
    })
}

async function generate_new_line(nom, prenom, categorie,nage,distance,temps_final,passage50m,passage100m,passage200m) {
    await add_new_ligne(nom, prenom, categorie,nage,distance,temps_final,passage50m,passage100m,passage200m);
    await generate_event_handler()
}
module.exports.generate_new_line = generate_new_line

const add_ligne = document.getElementById("add-ligne")
add_ligne.addEventListener("click", function () {
    generate_new_line("","","","","",["","",""],["","",""],["","",""],["","",""])
})

generate_new_line("","","","","",["","",""],["","",""],["","",""],["","",""])
