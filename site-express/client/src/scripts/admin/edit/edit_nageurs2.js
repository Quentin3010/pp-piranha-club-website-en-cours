console.log("EDIT_NAGEURS2.JS A ETE LOAD")
const showToast = require('../../other/toast').showToast;

const data_tab = document.getElementById("data-tab")

add_nageurs_rows()
async function add_nageurs_rows() {
    let data = null;
    try {
        const response = await fetch(`/api/nageur/getAll/true`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }

    const tab_id = await generate_nageur_row(data)
    generate_button_event(tab_id)
}

async function generate_nageur_row(data) {
    let res = ""
    let tab_id = []
    for (let i = data.length; i > 0; i--) {
        res += `<tr id="tr-nageur-${data[i - 1]["id_nageur"]}"><td class="td-id">${data[i - 1]["id_nageur"]}</td>`

        let img;
        if (data[i - 1]["photo"] == null || data[i - 1]["photo"] === "") img = "../images/default_profil.jpg";
        else img = `${data[i - 1]["photo"]}/${data[i - 1]["id_nageur"]}.jpeg`;
        res += `<td><img id="td-photo-${data[i - 1]["id_nageur"]}" value="${data[i - 1]["photo"]}" class="icon-nageur" src="${img}"><div class="upload-suppr-div"><input class="upload_icon" type="file" accept="image/*" id_nageur="${data[i - 1]["id_nageur"]}"><button class="supprimer-icon" id_nageur="${data[i - 1]["id_nageur"]}">X</button></div></td>`

        res += `<td><input type="text" id="td-nom-${data[i - 1]["id_nageur"]}" value="${data[i - 1]["nom"]}"></td>`
        res += `<td><input type="text" id="td-prenom-${data[i - 1]["id_nageur"]}" value="${data[i - 1]["prenom"]}"></td>`
        res += `<td><input type="date" id="td-date_naiss-${data[i - 1]["id_nageur"]}" value="${data[i - 1]["date_naiss"]}"></td>`
        if (data[i - 1]["sexe"] == "Homme") {
            res += `<td><select class="sexe" id="td-sexe-${data[i - 1]["id_nageur"]}" required>
                <option value="Homme">Homme</option>
                <option value="Femme">Femme</option>
            </select></td>`
        } else {
            res += `<td><select class="sexe" id="td-sexe-${data[i - 1]["id_nageur"]}" required>
                <option value="Femme">Femme</option>
                <option value="Homme">Homme</option>
            </select></td>`
        }
        res += `<td class="td-buttons"><button id="edit-button-${data[i - 1]["id_nageur"]}" class="buttons2 modif-nageur">Edit</button><button id="delete-button-${data[i - 1]["id_nageur"]}" value="0" class="buttons2 suppr-ligne">X</button></td></tr>`
        tab_id.push(data[i - 1]["id_nageur"])
    }
    data_tab.innerHTML += res
    return tab_id
}

function generate_button_event(tab_id) {
    for (const id of tab_id) {
        const edit_button = document.getElementById(`edit-button-${id}`)
        edit_button.addEventListener("click", function () {
            edit_nageur(id)
        })
        const delete_button = document.getElementById(`delete-button-${id}`)
        delete_button.addEventListener("click", function () {
            if (delete_button.value == 0) {
                delete_button.value = 1
            } else {
                delete_nageur(id)
            }
        })
    }
    const upload_icon = document.querySelectorAll(".upload_icon")
    upload_icon.forEach(function (button) {
        button.addEventListener("change", async function () {
            const id_nageur = button.getAttribute("id_nageur");
            const formData = new FormData();
            formData.append('photo', button.files[0] );

            const response = await uploadPhoto(formData, id_nageur)

            if (response.status === 200) {
                edit_icon(id_nageur, "/uploads/nageurs")
                document.getElementById(`td-photo-${id_nageur}`).setAttribute("src", `/uploads/nageurs/${id_nageur}.jpeg?timestamp=${new Date().getTime()}`);
            } else {
                alert("Erreur lors de l'upload");
            }
        })
    })
    const supprimer_icon = document.querySelectorAll(".supprimer-icon")
    supprimer_icon.forEach(function (button) {
        button.addEventListener("click", async function () {
            const id_nageur = button.getAttribute("id_nageur");
            const response = await fetch('/photos/nageur/deleteNageurPhoto', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ id_nageur: id_nageur })
            });

            if (response.status === 204) {
                edit_icon(button.getAttribute("id_nageur"),"")
                document.getElementById(`td-photo-${id_nageur}`).setAttribute("src", `../images/default_profil.jpg?timestamp=${new Date().getTime()}`);
            } else {
                alert("Erreur lors de la suppression");
            }
        })
    })
}

// Fonction pour effectuer l'envoi de la photo
async function uploadPhoto(formData, id_nageur) {
    const url = `/upload/nageur/${id_nageur}`;
    const options = {
        method: 'POST',
        body: formData
    };
    return await fetch(url, options);
}

async function delete_nageur(id_nageur) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ id: parseInt(id_nageur) })
    };
    const response = await fetch(`/api/nageur/delete`, requestOptions);
    if (response.status == 200) {
        const tr = document.getElementById(`tr-nageur-${id_nageur}`)
        tr.remove()
    }
}

async function edit_nageur(id) {
    const messageData = {
        id_nageur: id,
        nom: document.getElementById(`td-nom-${id}`).value,
        prenom: document.getElementById(`td-prenom-${id}`).value,
        date_naiss: document.getElementById(`td-date_naiss-${id}`).value,
        sexe: document.getElementById(`td-sexe-${id}`).value
    }

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(messageData)
    };

    const response = await fetch(`/api/nageur/update`, requestOptions);
    if (response.status == 200) {
        showToast("Edit réussi !", "success")
    }
}

async function edit_icon(id, v) {
    const messageData = {
        id_nageur: id,
        photo: v
    }

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(messageData)
    };

    const response = await fetch(`/api/nageur/changePhoto`, requestOptions);
    if (response.status == 200) {
        showToast("Photo modifié !", "success")
    }
}