console.log("EDIT_NEWS4.JS A ETE LOAD")
const generatePreview = require("./edit_news3").generatePreview

let today = new Date();
let year = today.getFullYear();
let month = String(today.getMonth() + 1).padStart(2, "0");
let day = String(today.getDate()).padStart(2, "0");

const news_preview = [
  {
    titre: "Titre",
    message: "Message",
    miniature: "",
    photos: "",
    date_news: `${year}-${month}-${day}`
  }
];
module.exports.news_preview = news_preview

const titre = document.getElementById("titre");
titre.addEventListener("input", function () {
  if (titre.value == "") news_preview[0]["titre"] = "Titre";
  else news_preview[0]["titre"] = titre.value;
  generatePreview(news_preview);
});

const miniature_link = document.getElementById("miniature_link");
miniature_link.addEventListener("input", function () {
  if (miniature_link.value == "") news_preview[0]["miniature"] = "";
  else news_preview[0]["miniature"] = miniature_link.value;
  generatePreview(news_preview);
});

const message_section = document.getElementById("message-section");
message_section.addEventListener("input", function () {
  if (message_section.value == "") news_preview[0]["message"] = "Message";
  else news_preview[0]["message"] = message_section.value;
  generatePreview(news_preview);
});

const photos = document.getElementById("photos");
photos.addEventListener("input", function () {
  if (photos.value == "") news_preview[0]["photos"] = "";
  else news_preview[0]["photos"] = photos.value;
  generatePreview(news_preview);
});
