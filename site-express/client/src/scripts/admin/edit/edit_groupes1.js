console.log("EDIT_GROUPES1.JS A ETE LOAD")

const annee = document.querySelector(".associate_groupe>input");

updateListeNageur();
async function updateListeNageur() {
    const allNageur = await getAllNageur();

    const liste_nageur = document.querySelectorAll(".liste_nageur")
    for (let i = 0; i < liste_nageur.length; i++) {
        liste_nageur[i].innerHTML = `<option value=""></option>`;
        for (let j = 0; j < allNageur.length; j++){
            liste_nageur[i].innerHTML += `<option value="${allNageur[j]["id_nageur"]}">${allNageur[j]["nom"]} ${allNageur[j]["prenom"]}</option>`;
        }
    }
}

async function getAllNageur() {
    try {
        const response = await fetch(`/api/nageur/getAll/false`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data;
}

const associate_groupe_button = document.getElementById("associer-button")
associate_groupe_button.addEventListener("click", function () {
    const messageData = {
        id_nageur: document.querySelector(".liste_nageur").value,
        nom_groupe: document.querySelector(".groupe").value,
        annee: document.querySelector(".annee").value,
      };
    if (messageData.id_nageur != "" && messageData.nom_groupe != "" && messageData.annee != "") {
        addAssociation(messageData)
    }
})

async function addAssociation(messageData) {
    const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(messageData)
    };
    
    const response = await fetch(`/api/groupe/associate`, requestOptions);
    if (response.status == 200) {
        window.location.href = "/admin/edit_groupes";
    }
} 

