console.log("EDIT_GALERIE1.JS A ETE LOAD");

// Fonction pour gérer l'envoi du formulaire d'upload
document.getElementById('uploadForm').addEventListener('submit', handleUploadForm);

async function handleUploadForm(event) {
    event.preventDefault();

    const files = document.getElementById('photo').files;
    const folderName = document.getElementById('folderName').value.replace(/ /g, "_");

    const transfert = document.getElementById('transfert');
    for (let i = 0; i < files.length; i++) {
        const formData = new FormData();
        formData.append('photo', files[i]);

        try {
            const response = await uploadPhoto(formData, folderName, document.getElementById('quality').value, document.getElementById('watermark').checked);
            if (response.status == 200) {
                await response.json();
                transfert.innerHTML = `Fichiers envoyés : ${i} / ${document.getElementById('photo').files.length}`
            } else {
                alert('Error uploading photo');
            }
        } catch (error) {
            console.error(error);
            alert('Error uploading photo');
        }
    }

    if (files.length > 0) {
        window.location.href = `/admin/edit_galerie`;
    }
}

// Fonction pour effectuer l'envoi de la photo
async function uploadPhoto(formData, folderName, quality, watermark) {
    const url = `/upload/album/${folderName}/${quality}/${watermark}`;
    const options = {
        method: 'POST',
        body: formData
    };
    return await fetch(url, options);
}

