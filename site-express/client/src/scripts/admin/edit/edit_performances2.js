console.log("EDIT_PERFORMANCES2.JS A ETE LOAD")
const generate_new_line = require("./edit_performances1").generate_new_line

let map;

const annee_input = document.getElementById("annee-input");
const date = new Date();
annee_input.value = (date.getMonth() >= 8) ? date.getFullYear() : date.getFullYear() - 1;

function uploadPdf() {
    var fileInput = document.getElementById('pdfInput');
    var outputDiv = document.getElementById('output'); // Ajout d'un élément div pour afficher le texte

    // Efface le contenu précédent
    outputDiv.innerHTML = '';

    var file = fileInput.files[0];

    if (file) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var pdfData = e.target.result;
            displayPdf(pdfData, outputDiv); // Passer l'élément div en paramètre
        };

        reader.readAsArrayBuffer(file);
    } else {
        alert('Veuillez sélectionner un fichier PDF.');
    }
}
const upload_pdf_button = document.getElementById("upload_pdf_button")
upload_pdf_button.addEventListener("click", () => {
    uploadPdf();
})

async function displayPdf(pdfData, outputDiv) {
    const pdfDoc = await pdfjsLib.getDocument({ data: pdfData }).promise;
    map = {
        "Nage Libre": {
            "50": [],
            "100": [],
            "200": [],
            "400": [],
            "800": [],
            "1500": [],
        },
        "Brasse": {
            "50": [],
            "100": [],
            "200": [],
        },
        "Dos": {
            "50": [],
            "100": [],
            "200": [],
        },
        "Papillon": {
            "50": [],
            "100": [],
            "200": [],
        },
        "4 nages": {
            "100": [],
            "200": [],
            "400": [],
        }
    }

    for (let pageNum = 1; pageNum <= pdfDoc.numPages; pageNum++) {
        let text = '';
        const page = await pdfDoc.getPage(pageNum);
        const textContent = await page.getTextContent();

        textContent.items.forEach(function (textItem) {
            text += textItem.str + " ";
        })

        await extractMatches(text);
    }

    outputDiv.innerHTML = ""
    for (const nage in map) {
        for (const distance in map[nage]) {
            if (map.hasOwnProperty(nage) && map[nage].hasOwnProperty(distance)) {
                // Ajoutez le contenu de chaque distance à outputDiv
                map[nage][distance].forEach(async e => {
                    await generate_new_line(e["Nom"], e["Prénom"], e["Catégorie"], nage, distance, e["TpsRéalisé"], e["TempsPassage50m"], e["TempsPassage100m"], e["TempsPassage200m"]);
                })
            }
        }
    }
}

function sexe(sexe) {
    if (sexe == "G") return "Homme"
    else if (sexe == "F") return "Femme"
    else return sexe
}

function categorie(annee) {
    const dateCompetition = document.querySelector(".add_perf>.date-competition").value;
    const competitionDate = new Date(dateCompetition);
    const resultYear = competitionDate.getMonth() < 8 ? competitionDate.getFullYear() - 1: competitionDate.getFullYear();
    const annee_cat = resultYear - annee
    if (isNaN(annee_cat)) {
        return
    }

    if (annee_cat <= 8) return "Mini-Poussin"
    else if (annee_cat <= 10) return "Poussin"
    else if (annee_cat <= 12) return "Benjamin"
    else if (annee_cat <= 14) return "Minime"
    else if (annee_cat <= 16) return "Cadet"
    else if (annee_cat <= 18) return "Junior"
    else if (annee_cat <= 24) return "Senior"
    else return "Master"
}

function temps(minutes, secondes, centiseconde) {
    if (minutes == undefined || secondes == undefined || centiseconde == undefined) return ["", "", ""]
    else {
        if (minutes.length == 1) return ["0" + minutes, secondes, centiseconde]
        else return [minutes, secondes, centiseconde]
    }
}

function createDataObject(match) {
    console.log("--> " + match)
    const place = document.getElementById("place-input").value;
    const club = document.getElementById("club-input").value;
    const nom = document.getElementById("nom-input").value;
    const prenom = document.getElementById("prenom-input").value;
    const numLicence = document.getElementById("num-licence-input").value;
    const sexee = document.getElementById("sexe-input").value;
    const categoriee = document.getElementById("categorie-input").value;
    const tempsEngagement = parseInt(document.getElementById("temps-engagement-input").value,10);
    const tempsRealise = parseInt(document.getElementById("temps-realise-input").value,10);
    const ptsFFN = document.getElementById("pts-ffn-input").value;
    const points = document.getElementById("points-input").value;
    const passage50m = parseInt(document.getElementById("passage-50m-input").value,10);
    const passage100m = parseInt(document.getElementById("passage-100m-input").value,10);


    const res = {
        Place: match[place],
        Club: match[club],
        Nom: match[nom],
        Prénom: match[prenom],
        NumdeLicence: match[numLicence] || "", // Rend le champ facultatif
        Sexe: sexe(match[sexee]),
        Catégorie: categorie(match[categoriee]),
        TpsEng: temps(match[tempsEngagement], match[tempsEngagement+1], match[tempsEngagement+2]),
        TpsRéalisé: temps(match[tempsRealise], match[tempsRealise+1], match[tempsRealise+2]),
        PtsFFN: match[ptsFFN],
        Points: match[points],
        TempsPassage50m: temps(match[passage50m], match[passage50m+1], match[passage50m+2]),
        TempsPassage100m: temps(match[passage100m], match[passage100m+1], match[passage100m+2]),
        //TempsPassage200m: temps(match[22],match[23],match[24]),
    };

    return res
}

let nage_actuel = ""
let distance_actuel = ""
async function extractMatches(inputText) {
    //Regex pour extraire la nage
    const regexNage = /(50m|100m|200m|400m)\s(Nage Libre|Dos|Brasse|Papillon|4 nages)\s(Messieurs|Dames)/g;
    //1 club NOM Prenom num_licence S annee 00 00 00 11 11 11 pts-ffn pts 22 22 22 33 33 33 44 44 44
    const regex400m = /(\d+)\s+([A-Za-zÀ-ÖØ-öø-ÿ\-]+(?:[A-Za-zÀ-ÖØ-öø-ÿ-\-]+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ]+(?:\s[A-ZÀ-ÖØ-öø-ÿ]+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ]+(?:[A-Za-zÀ-ÖØ-öø-ÿ]+)*)\s*([0-9_]+)\s+([A-Z])\s+(\d{4})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d+)\s+(\d{1,2})(\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2}))?/g;
    //1 club NOM Prenom num_licence S annee 00 00 00 11 11 11 pts-ffn pts 22 22 22 33 33 33
    const regex200m = /(\d+)\s+([A-Za-zÀ-ÖØ-öø-ÿ\-]+(?:[A-Za-zÀ-ÖØ-öø-ÿ-\-]+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ]+(?:\s[A-ZÀ-ÖØ-öø-ÿ]+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ]+(?:[A-Za-zÀ-ÖØ-öø-ÿ]+)*)\s*([0-9_]+)\s+([A-Z])\s+(\d{4})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d+)\s+(\d{1,2})(\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2}))?/g;
    //1 club NOM Prenom num_licence S annee 00 00 00 11 11 11 pts-ffn pts 22 22 22
    const regex100m = /(\d+)\s+([A-Za-zÀ-ÖØ-öø-ÿ\-]+(?:[A-Za-zÀ-ÖØ-öø-ÿ-\-]+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ]+(?:\s[A-ZÀ-ÖØ-öø-ÿ]+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ]+(?:[A-Za-zÀ-ÖØ-öø-ÿ]+)*)\s*([0-9_]+)?\s+([A-Z])\s+(\d{4})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d+)\s+(\d{1,2})(\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2}))?/g;
    //1 club NOM Prenom num_licence S annee 00 00 00 11 11 11
    const regex50m = /(\d+)\s+([A-Za-zÀ-ÖØ-öø-ÿ\-]+(?:[A-Za-zÀ-ÖØ-öø-ÿ-\-]+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ]+(?:\s[A-ZÀ-ÖØ-öø-ÿ]+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ]+(?:[A-Za-zÀ-ÖØ-öø-ÿ]+)*)\s*([0-9_]+)\s+([A-Z])\s+(\d{4})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})/g;

    let matches = []

    await execRegexNage(regexNage, inputText);
    if (matches.length === 0) {
        if (distance_actuel == "50m") matches = await execRegex(regex50m, inputText);
        else if (distance_actuel == "100m") matches = await execRegex(regex100m, inputText);
        else if (distance_actuel == "200m") matches = await execRegex(regex200m, inputText);
        else matches = await execRegex(regex400m, inputText);
    }

    if (matches.length != 0) {
        const lines = matches
        map[nage_actuel][`${distance_actuel}`] = [...new Set(map[nage_actuel][`${distance_actuel}`].concat(lines))];
    }

}

async function execRegex(regex, text) {
    let match;
    const matches = []
    while ((match = regex.exec(text)) !== null) {
        const dataObject = createDataObject(match);
        if (dataObject.Club === "Arques") {
            console.log("=> " + match)
            matches.push(dataObject);
        }
    }
    return matches
}

async function execRegexNage(regex, text) {
    let match;
    while ((match = regex.exec(text)) !== null) {
        distance_actuel = match[1].substring(0, match[1].length - 1)
        nage_actuel = match[2]
    }
}

/*
async function createPefLine(matches) {
    const lines = []
    matches.forEach(function (match) {
        const line = `${match.Nom},${match.Prénom},${match.Sexe},${match.Catégorie},${match.TpsRéalisé},${match.TempsPassage50m},${match.TempsPassage100m}`
        lines.push(line)
    })
    return lines
}
*/
