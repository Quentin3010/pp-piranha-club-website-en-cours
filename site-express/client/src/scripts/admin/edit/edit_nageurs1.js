console.log("EDIT_NAGEURS1.JS A ETE LOAD")

const add_nageur_button = document.getElementById("add-nageur")
add_nageur_button.addEventListener("click", function () {
    const messageData = {
        nom: document.querySelector(".add_nageur>.nom").value,
        prenom: document.querySelector(".add_nageur>.prenom").value,
        date_naiss: document.querySelector(".add_nageur>.date-naiss").value,
        sexe: document.querySelector(".add_nageur>.sexe").value,
        photo: document.querySelector(".add_nageur>.photo_link").value
      };
    if (messageData.nom != "" && messageData.prenom != "" && messageData.sexe != "") {
        addNewNageur(messageData)
    }
})

async function addNewNageur(messageData) {
    const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(messageData)
    };
    
    const response = await fetch(`/api/nageur/add`, requestOptions);
    if (response.status == 200) {
        window.location.href = "/admin/edit_nageurs";
    }
} 

