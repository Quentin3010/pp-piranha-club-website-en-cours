console.log("OUTIL_RESUME1.JS A ETE LOAD")

const distances = [200, 100, 50]
const nages = ["nage_libre","dos","brasse","papillon","4_nages"]

async function getAllNbPerformanceByAnnee(year) {
    try {
        const response = await fetch(`/api/performance/getAllNbPerformanceByAnnee/${year}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data;
}

async function getBestByYear(id_nageur, year, nage, distance) {
    try {
        const response = await fetch(`/api/performance/getBestByYear/${id_nageur}/${year}/${nage}/${distance}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data[0];
}

async function getTimes(id_nageur, year) {
    const data = []
    for(i in distances){
        for(u in nages){
            const temps = await getBestByYear(id_nageur, year, nages[u], distances[i])
            data.push(temps)
        }
    }
    return data
}

function convertTimeToTxt(time) {
    if (time == undefined) return ""
    else return time
}

function convertTimesToTd(times) {
    return `<td>${convertTimeToTxt(times[0])}</td><td>${convertTimeToTxt(times[1])}</td><td>${convertTimeToTxt(times[2])}</td><td>${convertTimeToTxt(times[3])}</td><td>${convertTimeToTxt(times[4])}</td><td>${convertTimeToTxt(times[5])}</td><td>${convertTimeToTxt(times[6])}</td><td>${convertTimeToTxt(times[7])}</td><td>${convertTimeToTxt(times[8])}</td><td>${convertTimeToTxt(times[9])}</td><td>${convertTimeToTxt(times[10])}</td><td>${convertTimeToTxt(times[11])}</td><td>${convertTimeToTxt(times[12])}</td><td>${convertTimeToTxt(times[13])}</td>`
}

async function generateTable(year){
    const listeNageur = await getAllNbPerformanceByAnnee(year)
    let res = `<table>
    <thead>
    <tr>
    <th rowspan="2">Club</th>
    <th rowspan="2">Année</th>
    <th class="td2" rowspan="2">Nageur</th>
    <th colspan="5">200m</th>
    <th colspan="5">100m</th>
    <th colspan="4">50m</th>
    <th rowspan="2">NB Journée</th>
    </tr>
    <tr>
    <th>Nage Libre</th>
    <th>Dos</th>
    <th>Brasse</th>
    <th>Papillon</th>
    <th>4 Nages</th>
    <th>Nage Libre</th>
    <th>Dos</th>
    <th>Brasse</th>
    <th>Papillon</th>
    <th>4 Nages</th>
    <th>Nage Libre</th>
    <th>Dos</th>
    <th>Brasse</th>
    <th>Papillon</th>
    </tr>
    </thead>
    <tbody>`
    for (let i = 0; i < listeNageur.length; i++) {
        const annee_naiss = listeNageur[i]["annee_naiss"]
        const times = await getTimes(listeNageur[i]["id_nageur"], year)
        const nageur = `${listeNageur[i]["nom"]} ${listeNageur[i]["prenom"]}`
        const nom_groupe = listeNageur[i]["nom_groupe"]
        const nbPerf = listeNageur[i]["nbPerf"]
        if ((year-annee_naiss >10 && nbPerf>=5) || (annee_naiss!=0 && year-annee_naiss <=10 && nbPerf>=3))  res += `<tr value="ok">`
        else res += `<tr>`
        res += `<td>${nom_groupe}</td><td>${annee_naiss!=0 ? annee_naiss : "?"}</td><td class="td2">${nageur}</td>${convertTimesToTd(times)}<td>${nbPerf}</td>`
        res += `</tr>`
    }
    res += `</tbody></table>`
    
    const tab_section = document.getElementById("tab-section")
    tab_section.innerHTML = res
}

const load_button = document.getElementById("load-button")
load_button.addEventListener("click", () => {
    const anneeSelector = document.getElementById("anneeSelector")
    const tab_section = document.getElementById("tab-section")
    tab_section.innerHTML = `<img id="loading" src="../../../images/loading.gif">`
    generateTable(anneeSelector.value);
})

async function generateSelector() {
    let data = null
    try {
        const response = await fetch(`/api/groupe/getAllYear`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    const anneeSelector = document.getElementById("anneeSelector");
    for (let i = 0; i < data.length; i++) {
        anneeSelector.innerHTML += `<option class="option" value="${data[i].split("-")[0]}">${data[i]}</option>`;
    }
}
generateSelector()