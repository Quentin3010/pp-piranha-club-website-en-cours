console.log("OUTIL_PREVISION3.JS A ETE LOAD")

class TempsData {
    constructor() {
        this.nage_libre = ""
        this.dos = ""
        this.brasse = ""
        this.papillon = ""
        this.quatre_nages = ""
    }
    
    toString(id, distance) {
        let res = `<td class="td2 td-to-select time_td">${this.nage_libre}</td><td class="td2 td-to-select time_td">${this.dos}</td><td class="td2 td-to-select time_td">${this.brasse}</td><td class="td2 td-to-select time_td">${this.papillon}</td>`;
        if (distance == 100) res += `<td class="td2 td-to-select time_td">${this.quatre_nages}</td>`;
        return res
    }
    
    setTemps(nage, temps) {
        const tempsFormat = `${temps[0]}:${temps[1]}:${temps[2]}`;
        const comparerTemps = (ancienTemps, nouveauTemps) => {
            if (!ancienTemps) return nouveauTemps; // Si pas de temps existant, accepter le nouveau
            return (convertTimeToSeconds(nouveauTemps) < convertTimeToSeconds(ancienTemps)) ? nouveauTemps : ancienTemps;
        };
    
        switch (nage) {
            case "Nage Libre":
                this.nage_libre = comparerTemps(this.nage_libre, tempsFormat);
                break;
            case "Dos":
                this.dos = comparerTemps(this.dos, tempsFormat);
                break;
            case "Brasse":
                this.brasse = comparerTemps(this.brasse, tempsFormat);
                break;
            case "Papillon":
                this.papillon = comparerTemps(this.papillon, tempsFormat);
                break;
            case "4 nages":
                this.quatre_nages = comparerTemps(this.quatre_nages, tempsFormat);
                break;
        }
    }    

    merge(other) {
        // Conserver le meilleur temps (le plus rapide) pour chaque nage
        const comparerTemps = (temps1, temps2) => {
            if (!temps1) return temps2;
            if (!temps2) return temps1;
            return (convertTimeToSeconds(temps1) < convertTimeToSeconds(temps2)) ? temps1 : temps2;
        };
    
        this.nage_libre = comparerTemps(this.nage_libre, other.nage_libre);
        this.dos = comparerTemps(this.dos, other.dos);
        this.brasse = comparerTemps(this.brasse, other.brasse);
        this.papillon = comparerTemps(this.papillon, other.papillon);
        this.quatre_nages = comparerTemps(this.quatre_nages, other.quatre_nages);
    }
    
}
module.exports.TempsData = TempsData

function convertTimeToSeconds(time) {
    if (time.split(':').length == 1) {
        return 10000;   
    }
    const [minutes, seconds, centiseconds] = time.split(':').map(Number);
    return minutes * 60 + seconds + centiseconds / 100;
}