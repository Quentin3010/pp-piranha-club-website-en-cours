console.log("OUTIL_PREVISION1.JS A ETE LOAD")
const NageurData = require("./outil_prevision2").NageurData

const date = new Date();
document.getElementById("annee-input").value = (date.getMonth() >= 8) ? date.getFullYear() : date.getFullYear() - 1;

let tabNageurs = {
    "Femme" : {
        "Mini-Poussin" : new Map(),
        "Poussin" : new Map(),
        "Benjamin" : new Map(),
        "Minime" : new Map(),
        "Cadet" : new Map(),
        "Junior" : new Map(),
        "Senior" : new Map(),
        "Master" : new Map(),
        "Vétéran" : new Map()
    },
    "Homme" : {
        "Mini-Poussin" : new Map(),
        "Poussin" : new Map(),
        "Benjamin" : new Map(),
        "Minime" : new Map(),
        "Cadet" : new Map(),
        "Junior" : new Map(),
        "Senior" : new Map(),
        "Master" : new Map(),
        "Vétéran" : new Map()
    }
}

const categorie_rechercher = "Cadet"
const sexe_rechercher = "Homme"

function ajouterOuMettreAJourMap(distance, nage, data) {
    let key = data["NumdeLicence"] != "" ? data["NumdeLicence"] : null;

    if (!key) {
        // On parcourt les clés pour vérifier si un nageur avec le même nom, prénom, et catégorie existe
        const nageursMap = tabNageurs[data["Sexe"]][data["Catégorie"]];
        for (let [existingKey, nageurData] of nageursMap) {
            if (
                nageurData.nom === data["Nom"].toUpperCase() + " " + data["Prénom"] &&
                nageurData.categorie === data["Catégorie"]
            ) {
                key = existingKey;
                break;
            }
        }

        // Si aucune clé existante ne correspond, on génère une nouvelle clé
        if (!key) {
            key = data["Nom"].toUpperCase() + "-" + data["Prénom"] + "-" + data["Catégorie"] + "-" + data["Club"];
        }
    }

    // Si le nageur n'existe pas dans la map, on l'ajoute
    if (!tabNageurs[data["Sexe"]][data["Catégorie"]].has(key)) {
        tabNageurs[data["Sexe"]][data["Catégorie"]].set(key, new NageurData(data["Nom"].toUpperCase() + " " + data["Prénom"], data["Club"]));
    }

    // Mise à jour des données du nageur
    let dataNageur = tabNageurs[data["Sexe"]][data["Catégorie"]].get(key);
    dataNageur.setTemps(distance, nage, data["TpsRéalisé"]);
}


async function setRanks() {
    for(const sexeTab in tabNageurs){
        for(const categorieTab in tabNageurs[sexeTab]){
            const nage_libre_100m_tab = []
            const dos_100m_tab = []
            const brasse_100m_tab = []
            const papillon_100m_tab = []
            const quatre_nages_100m_tab = []
            
            const nage_libre_50m_tab = []
            const dos_50m_tab = []
            const brasse_50m_tab = []
            const papillon_50m_tab = []
            
            tabNageurs[sexeTab][categorieTab].forEach((data, k) => {
                nage_libre_100m_tab.push({ [k]: [data][0].tps100m.nage_libre });
                dos_100m_tab.push({ [k]: [data][0].tps100m.dos });
                brasse_100m_tab.push({ [k]: [data][0].tps100m.brasse });
                papillon_100m_tab.push({ [k]: [data][0].tps100m.papillon });
                quatre_nages_100m_tab.push({ [k]: [data][0].tps100m.quatre_nages });
                
                nage_libre_50m_tab.push({ [k]: [data][0].tps50m.nage_libre });
                dos_50m_tab.push({ [k]: [data][0].tps50m.dos });
                brasse_50m_tab.push({ [k]: [data][0].tps50m.brasse });
                papillon_50m_tab.push({ [k]: [data][0].tps50m.papillon });
            });
            
            setRankInTab(sexeTab, categorieTab, sortMapByTime(nage_libre_100m_tab), "100m", "nage_libre")
            setRankInTab(sexeTab, categorieTab, sortMapByTime(dos_100m_tab), "100m", "dos")
            setRankInTab(sexeTab, categorieTab, sortMapByTime(brasse_100m_tab), "100m", "brasse")
            setRankInTab(sexeTab, categorieTab, sortMapByTime(papillon_100m_tab), "100m", "papillon")
            setRankInTab(sexeTab, categorieTab, sortMapByTime(quatre_nages_100m_tab), "100m", "quatre_nages")
            
            setRankInTab(sexeTab, categorieTab, sortMapByTime(nage_libre_50m_tab), "50m", "nage_libre")
            setRankInTab(sexeTab, categorieTab, sortMapByTime(dos_50m_tab), "50m", "dos")
            setRankInTab(sexeTab, categorieTab, sortMapByTime(brasse_50m_tab), "50m", "brasse")
            setRankInTab(sexeTab, categorieTab, sortMapByTime(papillon_50m_tab), "50m", "papillon")
        }
    }
}

// Fonction pour convertir le temps en format numérique
function convertTimeToSeconds(time) {
    if (time.split(':').length == 1) {
        return 10000;   
    }
    const [minutes, seconds, centiseconds] = time.split(':').map(Number);
    return minutes * 60 + seconds + centiseconds / 100;
}

// Fonction pour trier une map par le temps
function sortMapByTime(map) {
    return map.sort((a, b) => {
        const timeA = Object.values(a)[0];
        const timeB = Object.values(b)[0];
        
        const secondsA = convertTimeToSeconds(timeA);
        const secondsB = convertTimeToSeconds(timeB);
        
        return secondsA - secondsB;
    });
}

function setRankInTab(sexeTab, categorieTab, tab, distance, nage) {
    i = 1
    tab.forEach(element => {
        const [key, value] = Object.entries(element)[0];
        
        const td = document.getElementById(`${sexeTab}-${categorieTab}-${key}-${distance}-${nage}`)
        if (value != "") {
            try{
                td.innerHTML = `${i}`
            }catch(e){
                i--
            }
            i++
        }
    })
}

function tablesToExcel(tableIds, sheetNames) {
    const csvContent = [];
    
    tableIds.forEach((id, index) => {
        const table = document.getElementById(id);
        const sheetName = sheetNames[index];
        const rows = table.querySelectorAll('tr');
        
        // Ajouter le nom de la table avant la table
        csvContent.push(`"${sheetName}"`);
        csvContent.push(`"Club","Nageur","100m NL", "100m Dos", "100m Brasse", "100m Papillon", "100m 4 Nages", "50m NL", "50m Dos", "50m Brasse", "50m Papillon"`)

        // Extract data rows
        for (let i = 1; i < rows.length; i++) {
            const row = Array.from(rows[i].querySelectorAll('td')).map((td) => td.innerText);
            
            // Fill in missing values with empty strings at the beginning of the row
            while (row.length < 11) {
                row.unshift('');
            }
            //if(row[2]!="")
                csvContent.push(`"${row.join('","')}"`);
        }
        
        // Add a blank line between sheets
        csvContent.push('');
    });
    
    // Combine sheets into a single CSV content
    const csvData = csvContent.join('\n');
    
    // Create a Blob with UTF-8 encoding
    const blob = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]), csvData], { type: 'text/csv; charset=UTF-8' });
    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = 'data.csv';
    link.click();
}

// Appel de la fonction avec l'ID de votre table
const download = document.getElementById('download');
download.addEventListener('click', () => {
    const list = [
        'Femme-Mini-Poussin',
        'Femme-Poussin',
        'Femme-Benjamin',
        'Femme-Minime',
        'Femme-Cadet',
        'Femme-Junior',
        'Femme-Senior',
        'Femme-Master',
        'Femme-Vétéran',
        'Homme-Mini-Poussin',
        'Homme-Poussin',
        'Homme-Benjamin',
        'Homme-Minime',
        'Homme-Cadet',
        'Homme-Junior',
        'Homme-Senior',
        'Homme-Master',
        'Homme-Vétéran',
    ];
    tablesToExcel(list, list);
});

function uploadPdf() {
    var fileInput = document.getElementById('pdfInput');
    var outputDiv = document.getElementById('output'); // Ajout d'un élément div pour afficher le texte

    var file = fileInput.files[0];

    if (file) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var pdfData = e.target.result;
            displayPdf(pdfData, outputDiv); // Passer l'élément div en paramètre
        };

        reader.readAsArrayBuffer(file);
    } else {
        alert('Veuillez sélectionner un fichier PDF.');
    }
}
const button_upload_pdf = document.getElementById("button_upload_pdf")
button_upload_pdf.addEventListener("click", () => {
    uploadPdf()
})

async function readPages(pdfDoc) {
    for (let pageNum = 1; pageNum <= pdfDoc.numPages; pageNum++) {
        let text = '';
        const page = await pdfDoc.getPage(pageNum);
        const textContent = await page.getTextContent();

        textContent.items.forEach(function (textItem) {
            text += textItem.str + " ";
        })
        extractMatches(text);
    }
}

function mergeDuplicates() {
    // Parcours des sexes (Femme, Homme)
    for (const sexe in tabNageurs) {
        // Parcours des catégories
        for (const categorie in tabNageurs[sexe]) {
            const categoryMap = tabNageurs[sexe][categorie];

            // Dictionnaire temporaire pour regrouper les nageurs par nom, prénom, et club
            let nageursFusionnes = new Map();

            // Parcours de chaque nageur dans la catégorie
            categoryMap.forEach((nageurData, key) => {
                const { nom, club } = nageurData;

                // Créer une clé unique basée sur le nom, prénom et club pour identifier les doublons
                const keyUnique = `${nom}-${club}`;

                if (nageursFusionnes.has(keyUnique)) {
                    // Si un nageur avec le même nom, prénom et club existe déjà, on fusionne les temps
                    let nageurExistant = nageursFusionnes.get(keyUnique);
                    
                    // Fusionner les temps pour chaque nage (ici, on garde le meilleur temps)
                    nageurExistant.tps50m.merge(nageurData.tps50m);
                    nageurExistant.tps100m.merge(nageurData.tps100m);
                } else {
                    // Si le nageur est unique, on l'ajoute à la map temporaire
                    nageursFusionnes.set(keyUnique, nageurData);
                }
            });

            // Maintenant, on vide la catégorie et on y insère les nageurs fusionnés
            categoryMap.clear();
            nageursFusionnes.forEach((nageurData, keyUnique) => {
                // Générer une clé unique pour l'insertion
                const keyFusion = keyUnique;
                categoryMap.set(keyFusion, nageurData);
            });
        }
    }
}

async function displayPdf(pdfData, outputDiv) {
    const pdfDoc = await pdfjsLib.getDocument({ data: pdfData }).promise;

    for(let i = 0; i<2; i++)
        await readPages(pdfDoc);
    mergeDuplicates();
    
    let res = ""
    for (const sexeTab in tabNageurs) {
        res += '<div class="div3">'
        res += `<h2>${sexeTab}</h2>`
        res += "</div>"
        res += '<div class="div3">'
        for (const categorieTab in tabNageurs[sexeTab]) {
            res += `<h3>${categorieTab}</h3>`
            res += `<table class="table2" id="${sexeTab}-${categorieTab}">
            <thead>
              <tr class="tr2">
                <th class="th2" rowspan="2">Club</th>
                <th class="th2" rowspan="2">Nageur</th>
                <th class="th2" colspan="5">100m</th>
                <th class="th2" colspan="4">50m</th>
              </tr>
              <tr>
                <th class="th2">Nage Libre</th>
                <th class="th2">Dos</th>
                <th class="th2">Brasse</th>
                <th class="th2">Papillon</th>
                <th class="th2">4 Nages</th>
                <th class="th2">Nage Libre</th>
                <th class="th2">Dos</th>
                <th class="th2">Brasse</th>
                <th class="th2">Papillon</th>
              </tr>
            </thead>
            <tbody>`
            tabNageurs[sexeTab][categorieTab].forEach((element, k) => {
                res += `<tr value="${[element][0].club}">`
                res += `<td class="td2" rowspan="2">${[element][0].club}</td><td class="td2" rowspan="2">${[element][0].nom}</td>`
                res += [element][0].tps100m.toString(k, 100)
                res += [element][0].tps50m.toString(k, 50)
                res += `</tr>`
                res += `<tr class="tr2" value="${[element][0].club}">`
                res += `<td class="td2 rank_td" id="${sexeTab}-${categorieTab}-${k}-${100}m-nage_libre"></td><td class="td2 rank_td" id="${sexeTab}-${categorieTab}-${k}-${100}m-dos"></td><td class="td2 rank_td" id="${sexeTab}-${categorieTab}-${k}-${100}m-brasse"></td><td class="td2 rank_td" id="${sexeTab}-${categorieTab}-${k}-${100}m-papillon"></td><td class="td2 rank_td" id="${sexeTab}-${categorieTab}-${k}-${100}m-quatre_nages"></td><td class="td2 rank_td" id="${sexeTab}-${categorieTab}-${k}-${50}m-nage_libre"></td><td class="td2 rank_td" id="${sexeTab}-${categorieTab}-${k}-${50}m-dos"></td><td class="td2 rank_td" id="${sexeTab}-${categorieTab}-${k}-${50}m-brasse"></td><td class="td2 rank_td" id="${sexeTab}-${categorieTab}-${k}-${50}m-papillon"></td>`
                res += `</tr >`
            })
            res += `</tbody></table>`
        }
        res += "</div>"
    }
    outputDiv.innerHTML = res + "</div>"

    setRanks();

    addSelectEventHandler();
}

function sexe(sexe) {
    if (sexe == "G") return "Homme"
    else if (sexe == "F") return "Femme"
    else return sexe
}

function categorie(annee) {
    const annee_cat = parseInt(document.getElementById("annee-input").value) - parseInt(annee)
    if (annee_cat <= 8) return "Mini-Poussin"
    else if (annee_cat <= 10) return "Poussin"
    else if (annee_cat <= 12) return "Benjamin"
    else if (annee_cat <= 14) return "Minime"
    else if (annee_cat <= 16) return "Cadet"
    else if (annee_cat <= 19) return "Junior"
    else if (annee_cat <= 30) return "Senior"
    else if (annee_cat <= 40) return "Master"
    else return "Vétéran"
}

function temps(minutes, secondes, centiseconde) {
    if (minutes == undefined || secondes == undefined || centiseconde == undefined) return ["", "", ""]
    else {
        if (minutes.length == 1) return ["0" + minutes, secondes, centiseconde]
        else return [minutes, secondes, centiseconde]
    }
}

function createDataObject(match) {
    return {
        Place: match[1],
        Club: match[2],
        Nom: match[3],
        Prénom: match[4],
        NumdeLicence: match[5] || "", // Rend le champ facultatif
        Sexe: sexe(match[6]),
        Catégorie: categorie(match[7]),
        TpsEng: temps(match[8], match[9], match[10]),
        TpsRéalisé: temps(match[11], match[12], match[13]),
        PtsFFN: match[14],
        Points: match[15],
        TempsPassage50m: temps(match[16], match[17], match[18]),
        TempsPassage100m: temps(match[19], match[20], match[21]),
        //TempsPassage200m: temps(match[22],match[23],match[24]),
    };
}

let nage_actuel = ""
let distance_actuel = ""
async function extractMatches(inputText) {
    //Regex pour extraire la nage
    const regexNage = /(50m|100m|200m|400m)\s(Nage Libre|Dos|Brasse|Papillon|4 nages)\s(Messieurs|Dames)/g;
    //1 club NOM Prenom num_licence S annee 00 00 00 11 11 11 pts-ffn pts 22 22 22 33 33 33 44 44 44
    const regex400m = /(\d+)\s+([A-Za-zÀ-ÖØ-öø-ÿ'\-]+(?:[A-Za-zÀ-ÖØ-öø-ÿ-'\-]+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ']+(?:\s[A-ZÀ-ÖØ-öø-ÿ']+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ']+(?:[A-Za-zÀ-ÖØ-öø-ÿ']+)*)\s*([0-9_]+)\s+([A-Z])\s+(\d{4})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d+)\s+(\d{1,2})(\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2}))?/g;
    //1 club NOM Prenom num_licence S annee 00 00 00 11 11 11 pts-ffn pts 22 22 22 33 33 33
    const regex200m = /(\d+)\s+([A-Za-zÀ-ÖØ-öø-ÿ'\-]+(?:[A-Za-zÀ-ÖØ-öø-ÿ-'\-]+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ']+(?:\s[A-ZÀ-ÖØ-öø-ÿ']+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ']+(?:[A-Za-zÀ-ÖØ-öø-ÿ']+)*)\s*([0-9_]+)\s+([A-Z])\s+(\d{4})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d+)\s+(\d{1,2})(\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2}))?/g;
    //1 club NOM Prenom num_licence S annee 00 00 00 11 11 11 pts-ffn pts 22 22 22
    const regex100m = /(\d+)\s+([A-Za-zÀ-ÖØ-öø-ÿ'\-]+(?:\s[A-Za-zÀ-ÖØ-öø-ÿ'\-]+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ']+(?:\s[A-ZÀ-ÖØ-öø-ÿ']+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ']+(?:[A-Za-zÀ-ÖØ-öø-ÿ']+)*)\s*([0-9_]+)?\s+([A-Z])\s+(\d{4})\s+(?:(\d{1,2})\s+(\d{1,2})\s+(\d{1,2}))?\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d+)\s+(\d{1,2})(\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2}))?/g;
    //1 club NOM Prenom num_licence S annee 00 00 00 11 11 11
    const regex50m = /(\d+)\s+([A-Za-zÀ-ÖØ-öø-ÿ'\-]+(?:[A-Za-zÀ-ÖØ-öø-ÿ-'\-]+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ']+(?:\s[A-ZÀ-ÖØ-öø-ÿ']+)*)\s+([A-Za-zÀ-ÖØ-öø-ÿ']+(?:[A-Za-zÀ-ÖØ-öø-ÿ']+)*)\s*([0-9_]+)\s+([A-Z])\s+(\d{4})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})\s+(\d{1,2})/g;

    let matches = []

    await execRegexNage(regexNage, inputText);
    if (matches.length === 0) {
        if (distance_actuel.includes("50")) matches = await execRegex(regex50m, inputText);
        else if (distance_actuel.includes("100")) matches = await execRegex(regex100m, inputText);
        else if (distance_actuel.includes("200")) matches = await execRegex(regex200m, inputText);
        else matches = await execRegex(regex400m, inputText);
    }

    if (matches.length != 0) {
        matches.forEach(match => {
            ajouterOuMettreAJourMap(distance_actuel, nage_actuel, match)
        })
    }
}

async function execRegex(regex, text) {
    let match;
    const matches = []

    while ((match = regex.exec(text)) !== null) {
        const dataObject = createDataObject(match);
        matches.push(dataObject);
    }
    return matches
}

async function execRegexNage(regex, text) {
    let match;
    while ((match = regex.exec(text)) !== null) {
        distance_actuel = match[1].substring(0, match[1].length - 1)
        nage_actuel = match[2]
    }
}

function addSelectEventHandler() {
    const selectableTds = document.querySelectorAll(".td-to-select");

    selectableTds.forEach(td => {
        td.addEventListener("click", () => {
            if (!td.classList.contains("selected1") && !td.classList.contains("selected2")) {
                td.classList.add("selected1");
            } else if (td.classList.contains("selected1")) {
                td.classList.remove("selected1");
                td.classList.add("selected2");
            } else if (td.classList.contains("selected2")) {
                td.classList.remove("selected1", "selected2");
            }

            const twoRowsBelow = td.parentElement.nextElementSibling;
            if (twoRowsBelow) {
                const cellIndex = Array.from(td.parentElement.children).indexOf(td);
                const targetIndex = cellIndex - 2;
                if (targetIndex >= 0) {
                    const targetTd = twoRowsBelow.children[targetIndex];
                    if (targetTd) {
                        if (!targetTd.classList.contains("selected1") && !targetTd.classList.contains("selected2")) {
                            targetTd.classList.add("selected1");
                        } else if (targetTd.classList.contains("selected1")) {
                            targetTd.classList.remove("selected1");
                            targetTd.classList.add("selected2");
                        } else if (targetTd.classList.contains("selected2")) {
                            targetTd.classList.remove("selected1", "selected2");
                        }
                    }
                }
            }
        });
    });
}
