console.log("OUTIL_PREVISION2.JS A ETE LOAD")
const TempsData = require("./outil_prevision3").TempsData

class NageurData {
    constructor(nom,club) {
        this.nom = nom;
        this.club = club;
        this.tps50m = new TempsData();
        this.tps100m = new TempsData();
    }

    setTemps(distance, nage, temps){
        if(distance==50) this.tps50m.setTemps(nage, temps);
        else if(distance==100) this.tps100m.setTemps(nage, temps);
    }
}

module.exports.NageurData = NageurData