console.log("ADMIN.JS A ETE LOAD")

import '../../scripts/other/header-section.js';
import '../../scripts/other/footer-section.js';
import '../../style/other/footer-section.css';
import '../../style/other/header-section.css';

document.body.style = 'background-image: url("../../../images/fond_top.png"), url("../../../images/fond_repeat.png");'

import '../../style/other/general.css';
import '../../style/other/font-size.css';

import '../../style/admin/admin.css';

const buttons = document.querySelectorAll(".admin-sections")
buttons.forEach(b => {
    b.setAttribute("style", 'background-image: url("../../images/fond_top.png");');
})
////////////////////
// SCRIPT SECTION //
////////////////////

//////////////////////////
// REDIRECTION SECTIONS //
//////////////////////////
function create_redirection_square(e) {
    const rgb = hexToRgb(e.color);
    
    const intensity_boost = 18
    const lighterColor = `rgb(${Math.min(rgb.r + intensity_boost, 255)}, ${Math.min(rgb.g + intensity_boost, 255)}, ${Math.min(rgb.b + intensity_boost, 255)})`;
    
    return `
        <a class="div_redirection" style="background: radial-gradient(circle, ${e.color} 0%, ${lighterColor} 70%), repeating-linear-gradient(
                        45deg, 
                        rgba(255, 255, 255, 0.1), 
                        rgba(255, 255, 255, 0.1) 1px, 
                        rgba(0, 0, 0, 0.05) 1px, 
                        rgba(0, 0, 0, 0.05) 1px
                    );" href="/admin/${e.direction}">
            <div>
                <h2>${e.name.replace(" ", "</br>")}</h2>
            </div>
        </a>`;
}

// Fonction pour convertir hex en rgb
function hexToRgb(hex) {
    const bigint = parseInt(hex.replace('#', ''), 16);
    const r = (bigint >> 16) & 255;
    const g = (bigint >> 8) & 255;
    const b = bigint & 255;
    return { r, g, b };
}


const list = [
    {name:"Section Nageur",direction:"edit_nageurs",color:"#4559bf"},
    {name:"Section Performances",direction:"edit_performances",color:"#4559bf"},
    {name:"Section Résultats",direction:"edit_resultats",color:"#4559bf"},
    {name:"Section News",direction:"edit_news",color:"#4559bf"},
    {name:"Section Galerie",direction:"edit_galerie",color:"#4559bf"},
    
    {name:"Section Accueil",direction:"",color:"#51bd48"},
    {name:"Section Inscription",direction:"",color:"#51bd48"},
    {name:"Section Sponsors",direction:"",color:"#51bd48"},
    {name:"Section Contact",direction:"",color:"#51bd48"},
    
    {name:"Outil Prévision",direction:"outil_prevision",color:"#bd4884"},
    {name:"Outil Résumé",direction:"outil_resume",color:"#bd4884"},
]
const gestion_du_site = document.getElementById("gestion_du_site")
for(const i in list){
    gestion_du_site.innerHTML += create_redirection_square(list[i])
}


//////////////////////////
// STATISTIQUES SECTION //
//////////////////////////
var switchElement = document.getElementById("mySwitch");

const data_visite = document.getElementById("data-visite")
createGraph([], [], false)
getGeneraleVisite(new Date().getTime(), false)

async function getGeneraleVisite(givenDate, totale) {
    let x = []
    let y = []

    let formattedDate = new Date(givenDate).toISOString().slice(0, 10);
    const response = await fetch(`/api/stats/visite/${formattedDate}/monthly`);
    const data = await response.json();

    for (let i = 0; i < data.length; i++) {
        x.push(data[i]["date"])
        if (totale == false) y.push(data[i]["visite_unique"])
        else y.push(data[i]["visite_totale"])
    }
    createGraph(x, y, totale)

}

function createGraph(x, y, totale) {
    const data = [{
        x: x,
        y: y,
        type: 'bar',
        mode: 'lines+markers',
        line: {
            color: 'blue'
        }
    }];

    let title;
    if (totale == false) title = 'Nombre de visite unique des 30 derniers jours'
    else title = 'Nombre de visite totale des 30 derniers jours'

    const layout = {
        title: title,
        xaxis: {
            title: 'Jours',
        },
        yaxis: {
            title: 'Nombre de visiste',
            rangemode: 'tozero'
        }
    };
    Plotly.newPlot('lineChart', data, layout);
}

let nb_mois = 0;
const stats_precedente = document.getElementById("stats-precedente")
stats_precedente.addEventListener("click", function () {
    nb_mois--
    getGeneraleVisite(new Date().getTime() + nb_mois * (30 * 24 * 60 * 60 * 1000), switchElement.checked)
})

const stats_suivante = document.getElementById("stats-suivante")
stats_suivante.addEventListener("click", function () {
    nb_mois++
    getGeneraleVisite(new Date().getTime() + nb_mois * (30 * 24 * 60 * 60 * 1000), switchElement.checked)
})

switchElement.addEventListener("change", function () {
    getGeneraleVisite(new Date().getTime() + nb_mois * (30 * 24 * 60 * 60 * 1000), switchElement.checked)
});