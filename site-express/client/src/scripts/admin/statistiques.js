console.log("STATISTIQUES.JS A ETE LOAD")

import '../../scripts/other/header-section.js';
import '../../scripts/other/footer-section.js';
import '../../style/other/footer-section.css';
import '../../style/other/header-section.css';

document.body.style = 'background-image: url("../../../images/fond_top.png"), url("../../../images/fond_repeat.png");'

import '../../style/other/general.css';
import '../../style/other/font-size.css';

import '../../style/admin/statistiques.css';

////////////////////
// SCRIPT SECTION //
////////////////////

function createGraph(DivName, x, y, typeGraphique, titreGraphique, totale) {
    const data = [{
        x: x,
        y: y,
        type: typeGraphique,
        mode: 'lines',
        line: {
            color: 'blue'
        }
    }];

    const layout = {
        title: titreGraphique,
        xaxis: {
            title: 'Jours',
        },
        yaxis: {
            title: 'Nombre de visiste',
            rangemode: 'tozero'
        }
    };
    Plotly.newPlot(DivName, data, layout);
}

////////////////////
// STATS ANNUELLE //
////////////////////
var switchElement1 = document.getElementById("mySwitch1");
createGraph('stats_annuelle', [], [], 'bar', 'Nombre de visite unique des 365 derniers jours', false)
getGeneraleVisite(new Date().getTime(), false)

async function getGeneraleVisite(givenDate, totale) {
    let x = []
    let y = []

    let formattedDate = new Date(givenDate).toISOString().slice(0,10);
    const response = await fetch(`/api/stats/visite/${formattedDate}/yearly`);
    const data = await response.json();

    for(let i = 0; i<data.length; i++){
        x.push(data[i]["date"])
        if(totale==false) y.push(data[i]["visite_unique"])
        else y.push(data[i]["visite_totale"])
    }
    let title;
    if(totale==false) title = 'Nombre de visite unique des 365 derniers jours'
    else title = 'Nombre de visite totale des 365 derniers jours'

    createGraph('stats_annuelle', x, y, 'bar', title, totale)
    
}

let nb_mois1 = 0;
const stats_precedente1 = document.getElementById("stats-precedente1")
stats_precedente1.addEventListener("click", function () {
    nb_mois1--
    getGeneraleVisite(new Date().getTime() + nb_mois1*(365*24 * 60 * 60 * 1000), switchElement1.checked)
})

const stats_suivante1 = document.getElementById("stats-suivante1")
stats_suivante1.addEventListener("click", function () {
    nb_mois1++
    getGeneraleVisite(new Date().getTime() + nb_mois1*(365*24 * 60 * 60 * 1000), switchElement1.checked)
})

switchElement1.addEventListener("change", function() {
    getGeneraleVisite(new Date().getTime() + nb_mois1*(365*24 * 60 * 60 * 1000), switchElement1.checked)
});

//////////////////////////
// STATS PAGE MENSUELLE //
//////////////////////////
var selecteur_de_page = document.getElementById("selecteur_de_page");
var switchElement2 = document.getElementById("mySwitch2");
createGraph('stats_page_mensuelle', [], [], 'plot', 'Nombre de visite unique de la page "inscription.html" lors des 30 derniers jours', false)
getVisitePage('inscription', new Date().getTime(), false)

async function getVisitePage(page, givenDate, totale) {
    let x = []
    let y = []

    let formattedDate = new Date(givenDate).toISOString().slice(0,10);
    const response = await fetch(`/api/stats/visite/${formattedDate}/page/${page}`);
    const data = await response.json();

    for(let i = 0; i<data.length; i++){
        x.push(data[i]["date"])
        if(totale==false) y.push(data[i]["visite_unique"])
        else y.push(data[i]["visite_totale"])
    }
    let title;
    if(totale==false) title = 'Nombre de visite unique de la page "' + selecteur_de_page.value + '" lors des 30 derniers jours'
    else title = 'Nombre de visite totale de la page "' + selecteur_de_page.value + '" lors des 30 derniers jours'

    createGraph('stats_page_mensuelle', x, y, 'plot', title, totale)
    
}

let nb_mois2 = 0;
const stats_precedente2 = document.getElementById("stats-precedente2")
stats_precedente2.addEventListener("click", function () {
    nb_mois2--
    getVisitePage(selecteur_de_page.value, new Date().getTime() + nb_mois2*(30*24 * 60 * 60 * 1000), switchElement2.checked)
})

const stats_suivante2 = document.getElementById("stats-suivante2")
stats_suivante2.addEventListener("click", function () {
    nb_mois2++
    getVisitePage(selecteur_de_page.value, new Date().getTime() + nb_mois2*(30*24 * 60 * 60 * 1000), switchElement2.checked)
})

switchElement2.addEventListener("change", function() {
    getVisitePage(selecteur_de_page.value, new Date().getTime() + nb_mois2*(30*24 * 60 * 60 * 1000), switchElement2.checked)
});

selecteur_de_page.addEventListener("change", function() {
    nb_mois2 = 0
    getVisitePage(selecteur_de_page.value, new Date().getTime() + nb_mois2*(30*24 * 60 * 60 * 1000), switchElement2.checked)
});

////////////////////////////
// VISITE PROFILS NAGEURS //
////////////////////////////
generateTableVisteProfil()
async function generateTableVisteProfil(){
    const liste_profil_nageur = document.getElementById("liste_profil_nageur")

    let res = "<div class='table-overflow'><table id='table1'>"
    res += "<tr> <th>ID</th> <th>Nageur</th> <th>Unique</th> <th>Totale</th> </tr>"

    const response = await fetch(`/api/stats/visite/profils`);
    const data = await response.json();
    data.forEach(function(d){
        res += `<tr> <td>${d["id_nageur"]} </td> <td>${nomPrenom(d["nom"], d["prenom"])}</td> <td>${d["unique"]} </td> <td>${d["totale"]} </td> </tr>`
    })

    res += "</table></div>"

    liste_profil_nageur.innerHTML += res
}  

function nomPrenom(nom, prenom) {
    var nomMajuscules = nom.toUpperCase();
    var prenomCapitalise = prenom.charAt(0).toUpperCase() + prenom.slice(1);
    return nomMajuscules + " " + prenomCapitalise;
}

/////////////////
// VISITE NEWS //
/////////////////
generateTableNews()
async function generateTableNews(){
    const liste_news = document.getElementById("liste_news")

    let res = "<div class='table-overflow'> <table id='table2'>"
    res += "<tr> <th>ID</th> <th>Minia</th> <th>Titre</th> <th>Unique</th> <th>Totale</th> </tr>"

    const response = await fetch(`/api/stats/visite/news`);
    const data = await response.json();
    data.forEach(function(d){
        res += `<tr> <td>${d["id_news"]} </td> <td><img src="${d["miniature"]}" class="miniature"></td> <td>${d["titre"]} </td> <td>${d["unique"]} </td> <td>${d["totale"]} </td> </tr>`
    })

    res += "</table></div>"

    liste_news.innerHTML += res
}  

//////////////////////
// VISITE EXTERIEUR //
//////////////////////

setValueForVisiteExterieur()
async function setValueForVisiteExterieur(){
    let response = await fetch(`/api/stats/visite/qrcode`);
    let data = await response.json();
    document.getElementById("td-unique-qr").innerHTML = data[0]["visite_unique"]  
    document.getElementById("td-totale-qr").innerHTML = data[0]["visite_totale"]  


    response = await fetch(`/api/stats/visite/facebook`);
    data = await response.json();
    document.getElementById("td-unique-fb").innerHTML = data[0]["visite_unique"]  
    document.getElementById("td-totale-fb").innerHTML = data[0]["visite_totale"]  
} 

/////////////////////////////
// NOMBRE DE VISITE UNIQUE //
/////////////////////////////

getNbVisiteUnique()
async function getNbVisiteUnique(){
    const response = await fetch(`/api/stats/visite`);
    const data = await response.json();
    document.getElementById("nb_visite_unique").innerHTML = data[0] 
} 