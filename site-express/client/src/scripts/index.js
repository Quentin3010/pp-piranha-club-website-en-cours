console.log("INDEX.JS A ETE LOAD")

import '../scripts/other/header-section.js';
import '../scripts/other/footer-section.js';
import '../style/other/footer-section.css';
import '../style/other/header-section.css';

document.body.style = 'background-image: url("../../images/fond_top.png"), url("../../images/fond_repeat.png");'

import '../style/news.css';
import '../style/other/general.css';
import '../style/other/font-size.css';

import '../style/index.css';

const addPreviews = require('./other/news-function').addPreviews;

// ADD VISITE
import '../scripts/other/add_visite.js';

////////////////////
// SCRIPT SECTION //
////////////////////

generateNewsAccueil()
async function generateNewsAccueil() {
    await addPreviews(3, 0);
    const previews = document.querySelectorAll(".text-preview>div")
    previews.forEach(function (preview) {
        preview.innerHTML = preview.textContent
    })
}

/*
async function getAlerte() {
    try {
        const response = await fetch(`${ip_api}/alerte/get`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data
}

const alerteDiv = document.getElementById("Alerte")

async function displayAlert() {
    const alerte = await getAlerte()
    if (alerte.length == 1) {
        document.body.style.overflow = "hidden"; // Désactive la barre de défilement

        alerteDiv.setAttribute("style", "display:block")
        const alerteMessageDiv = document.getElementById("AlerteMessage");
        alerteMessageDiv.innerHTML = `<p> ${data[0].text}</p>`

        const closeButton = document.querySelector(".close-button")
        closeButton.addEventListener("click", function () {
            document.body.style.overflow = "auto"; // Active la barre de défilement
            alerteDiv.setAttribute("style", "display:hidden")
        })
    }
}
displayAlert()
*/