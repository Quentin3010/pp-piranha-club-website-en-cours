console.log("GALERIE.JS A ETE LOAD")

import './other/header-section.js';
import './other/footer-section.js';
import '../style/other/footer-section.css';
import '../style/other/header-section.css';

import fondTop from '../images/fond_top.png';
import fondRepeat from '../images/fond_repeat.png';

document.body.style = `background-image: url("${fondTop}"), url("${fondRepeat}");`;


import '../style/other/general.css';
import '../style/other/font-size.css';

import '../style/galerie.css';

// ADD VISITE
import './other/add_visite.js';

// Définition de la constante album à partir de l'URL
const album = new URLSearchParams(window.location.search).get('album');
const nbFile = 32;
let idx = 0;
let nb_file_added = 0;

// Fonction pour charger les previews des albums
async function loadPreviews() {
    const response = await fetch('/photos/album/getPreviews');
    if (response.status == 200) {
        const previews = await response.json();
        renderPreviews(previews);
    } else {
        renderEmptyPreviews();
    }
}

// Fonction pour afficher les previews des albums
function renderEmptyPreviews() {
    const listeAlbums = document.getElementById("liste-albums");
    listeAlbums.innerHTML += `
    <div class="div3"><h1>Galerie</h1></div>
    <div class="div3">
    <p class="center">
    Vous retrouverez ici les photos prises lors de différents événements !</br>Si vous avez des photos à nous faire parvenir, vous pouvez nous les envoyer à piranhaclubarques@outlook.fr</p>
    </div>`;
    addClickHandlers();
}

// Fonction pour afficher les previews des albums
function renderPreviews(previews) {
    const listeAlbums = document.getElementById("liste-albums");
    listeAlbums.innerHTML += `
        <div class="div3"><h1>Galerie</h1></div>
        <div class="div3">
            <p class="center">
            Vous retrouverez ici les photos prises lors de différents événements !</br>Si vous avez des photos à nous faire parvenir, vous pouvez nous les envoyer à piranhaclubarques@outlook.fr</p>
            <div class="gallery">${createAlbumPreviews(previews)}</div>
        </div>
    `;
    addClickHandlers();
}

// Fonction pour créer les previews des albums
function createAlbumPreviews(previews) {
    let html = "";
    previews.forEach(preview => {
        html += `
            <div class="div3 album-div" value="${preview.folderName}">
                <img alt="${preview.folderName}" src="/uploads/albums/${preview.folderName}/${preview.firstImage}">
                <p class="album_name">${preview.folderName.replace(/_/g, " ")}</p> 
            </div>
        `;
    });
    return html;
}

// Ajout des gestionnaires de clics aux previews d'albums
function addClickHandlers() {
    const listeAlbums = document.getElementById("liste-albums");
    listeAlbums.addEventListener('click', (event) => {
        const albumDiv = event.target.closest('div[value]');
        if (albumDiv) {
            const albumName = albumDiv.getAttribute('value');
            window.location.href = `/galerie?album=${albumName}`;
        }
    });
}

// Fonction pour charger plus de photos d'un album spécifique
async function loadMorePhotos(folderName, nbFile, startIdx) {
    const response = await fetch(`/photos/album/getPhotos/${folderName}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ nbFile, idx: startIdx })
    });

    if (!response.ok) {
        window.location.href = `/galerie`;
    }

    const data = await response.json();
    idx += data.nbPhoto;
    nb_file_added = data.nbPhoto;

    return data.photos;
}

// Fonction pour ajouter les photos d'un album au DOM
function    addPhotosToDOM(photosList, folderName) {
    const photosSection = document.getElementById("photos-section");
    let photosHTML = '';

    photosList.forEach(photo => {
        const photoPath = `/uploads/albums/${folderName.replace(/ /g, "_")}/${photo}`;
        photosHTML += `
            <div class="div3 image_fullscreen" value="${photoPath}">
                <img alt="${photoPath}" src="${photoPath}">
            </div>
        `;
    });

    photosSection.innerHTML += photosHTML;

    // Gestion de l'ouverture/fermeture en plein écran des images
    const imageFullscreenList = document.querySelectorAll(".image_fullscreen");
    imageFullscreenList.forEach(image => {
        image.addEventListener("click", () => {
            const fullscreenCloseContainer = document.getElementById('fullscreenCloseContainer');
            const fullscreenDownloadContainer = document.getElementById('fullscreenDownloadContainer');
            if (!image.classList.contains('fullscreen')) {
                image.classList.add('fullscreen');
                fullscreenCloseContainer.style.display = 'block';
                fullscreenDownloadContainer.style.display = 'block';
            }
        });
    });
}

// Fonction pour charger et afficher un album spécifique
async function loadAlbum(albumName) {
    const photosDiv = document.getElementById('photos');
    photosDiv.innerHTML += `
        <div class="div3"><h1>Album : ${albumName.replace(/_/g, " ")}</h1></div>
        <div class="div3">
            <div id="photos-section"></div>
            <button id="load-more-button">Charger plus de photo</button>
        </div>
    `;
    const loadMoreButton = document.getElementById("load-more-button")
    // Ajoutez un gestionnaire d'événement au bouton
    loadMoreButton.addEventListener('click', async () => {
        loadMoreButton.setAttribute("style","display:none");
        await addPhotosToDOM(await loadMorePhotos(album, nbFile, idx), album);
        console.log(nb_file_added, nbFile)
        if(nb_file_added==nbFile){
            loadMoreButton.setAttribute("style","display:block"); 
        }
    });
    addFullscreenCloseHandler();
}

// Gestionnaire de clic pour fermer l'image en plein écran
function addFullscreenCloseHandler() {
    const fullscreenCloseContainer = document.getElementById('fullscreenCloseContainer');
    fullscreenCloseContainer.addEventListener('click', () => {
        const fullscreenImage = document.querySelector('.image_fullscreen.fullscreen');
        if (fullscreenImage) {
            fullscreenImage.classList.remove('fullscreen');
            fullscreenCloseContainer.style.display = 'none';
            fullscreenDownloadContainer.style.display = 'none';
        }
    });

    const fullscreenDownloadContainer = document.getElementById('fullscreenDownloadContainer');
    fullscreenDownloadContainer.addEventListener('click', () => {
        const fullscreenImage = document.querySelector('.image_fullscreen.fullscreen');
        if (fullscreenImage) {
            const imageSrc = fullscreenImage.getAttribute("value");
            const downloadLink = document.createElement('a');
            downloadLink.href = imageSrc;
            downloadLink.download = 'PCA_download.jpg'; // Nom du fichier à télécharger
            downloadLink.style.display = 'none';
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
    });

}

// Fonction d'initialisation principale
async function init() {
    if (album) {
        // Chargement initial de l'album
        await loadAlbum(album);
        await addPhotosToDOM(await loadMorePhotos(album, nbFile, idx), album);
        if(nb_file_added!=nbFile){
            document.getElementById("load-more-button").setAttribute("style","display:none"); 
        }
    } else {
        // Si aucun album n'est sélectionné, chargez les aperçus
        loadPreviews();
    }
}


// Lancer l'initialisation
init();
