console.log("ESPACE_NAGEURS.JS A ETE LOAD");

import './other/header-section.js';
import './other/footer-section.js';
import '../style/other/footer-section.css';
import '../style/other/header-section.css';

document.body.style = 'background-image: url("../../images/fond_top.png"), url("../../images/fond_repeat.png");';

import '../style/other/general.css';
import '../style/other/font-size.css';
import '../style/espace_club.css';

document.querySelector(".nos-nageurs").setAttribute("style", 'background-image: url("../../images/fond_top.png");');
document.querySelector(".resultats").setAttribute("style", 'background-image: url("../../images/fond_top.png");');

// ADD VISITE
import './other/add_visite.js';

////////////////////
// SCRIPT SECTION //
////////////////////

const categories = ["Mini-Poussin", "Poussin", "Benjamin", "Minime", "Cadet", "Junior", "Senior", "Master"];
const categoriesDsecription = ["8ans et moins", "9-10ans","11-12ans","13-14ans","15-16ans","17-18ans","19-24ans","25ans et plus"];
const dists = [50,100,200,400,800,1500]

const nosNageursDiv = document.querySelector('.nos-nageurs');
const resultatsDiv = document.querySelector('.resultats');    

nosNageursDiv.addEventListener('click', function() {
    window.location.href = './nos_nageurs'; 
});

resultatsDiv.addEventListener('click', function() {
    window.location.href = './resultats'; 
});

async function getRecords(nage) {
    let data = null;
    try {
        const response = await fetch(`/api/records/getRecords/${nage.replace(" ", "_")}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }
    return data;
}

function getAnnee(a) {
    if (a == 1900) return "?";
    else return a;
}

function generateHeader(nage) {
    const headerRow = document.createElement('tr');

    // Ajouter la colonne "Catégorie" avec la classe "largeur-colonne2"
    const categoryHeader = document.createElement('th');
    categoryHeader.textContent = 'Catégorie';
    categoryHeader.classList.add('largeur-colonne2');
    headerRow.appendChild(categoryHeader);

    // Ajouter les distances des femmes avec la classe "entete_femme"
    dists.forEach((dist) => {
        const th = document.createElement('th');
        th.textContent = `${dist}m`;
        th.className = "entete";
        headerRow.appendChild(th);
    });

    return headerRow;
}

function generateRow(nage, categorie, categorieDescription, dataFemme, dataHomme, rowIndex) {
    const row1 = document.createElement('tr');
    const row2 = document.createElement('tr');

    // Ajouter la cellule pour la "Catégorie" avec la classe "largeur-colonne2"
    const tdCategorie = document.createElement('td');
    tdCategorie.className = 'cat_name';
    tdCategorie.rowSpan = '2';
    tdCategorie.innerHTML = `${categorieDescription}`;
    row1.appendChild(tdCategorie);

    // Appliquer les classes "odd" ou "even" en fonction de l'index de la ligne
    row1.className = rowIndex % 2 === 0 ? 'even' : 'odd';
    row2.className = rowIndex % 2 === 0 ? 'even' : 'odd';

    // Créer les cellules pour les records des femmes
    dists.forEach((dist) => {
        const td1 = document.createElement('td');
        td1.className = "td-femme"
        let record = dataFemme[categorie][dist];
        if (record) {
            td1.innerHTML = `<div class="record-div-data">
            <a class="redirection_profil femme" href="/profil_nageur?id=${record.id_nageur}">${mettrePremiereLettreEnMajuscule(record.prenom)} ${record.nom[0].toUpperCase()}.</a>
            <br>
            ${record.temps.replace(/:/g, "'")}
            <br>
            <span class="age-specification">${getAnnee(record.annee)}</span>
            </div>`;
        }else{
            td1.innerHTML = `<div class="record-div-data"></br>-</br></br></div>`
        }
        row1.appendChild(td1);

        const td2 = document.createElement('td');
        record = dataHomme[categorie][dist];
        if (record) {
            td2.innerHTML += `<div class="record-div-data">
            <a class="redirection_profil homme" href="/profil_nageur?id=${record.id_nageur}">${mettrePremiereLettreEnMajuscule(record.prenom)} ${record.nom[0].toUpperCase()}.</a>
            <br>
            ${record.temps.replace(/:/g, "'")}
            <br>
            <span class="age-specification">${getAnnee(record.annee)}</span>
            </div>`;
        }else{
            td2.innerHTML = `<div class="record-div-data"></br>-</br></br></div>`
        }
        row2.appendChild(td2);
    });
    
    return [row1, row2];
}

async function generateTable(nage) {
    const data = await getRecords(nage);
    const tableau = document.createElement('table');
    tableau.classList.add("resultats-table");

    // Generate Header
    const header = generateHeader(nage);
    tableau.appendChild(header);

    // Generate Rows
    categories.forEach((categorie, index) => {
        const row = generateRow(nage, categorie, categoriesDsecription[index], data["Femme"], data["Homme"], index)
        tableau.appendChild(row[0]);
        tableau.appendChild(row[1]);
    });

    // Append the generated table to the div
    const divTableau = document.getElementById('div-tableau-records');
    divTableau.innerHTML = ''; // Clear previous table
    divTableau.appendChild(tableau);
    
    const centerMessage = document.createElement('p');
    centerMessage.className = 'center';
    centerMessage.textContent = 'Les catégories ne sont pas forcément en accord avec celles établies par l\'UFOLEP.';
    divTableau.appendChild(centerMessage);
}

function mettrePremiereLettreEnMajuscule(mot) {
    if (!mot) {
        return ''; // Retourne une chaîne vide si le mot est null ou undefined
    }
    return mot.charAt(0).toUpperCase() + mot.slice(1).toLowerCase();
}

function generate_selector_event_handler(){
    const selector = document.getElementById("style_de_nage")
    selector.addEventListener("change", (event) => {
        const selectedValue = event.target.value;
        generateTable(selectedValue);
    });
}

// Initial table generation
generate_selector_event_handler()
generateTable("Nage Libre");
