console.log("NOS_NAGEURS.JS A ETE LOAD")

import '../scripts/other/header-section.js';
import '../scripts/other/footer-section.js';
import '../style/other/footer-section.css';
import '../style/other/header-section.css';

document.body.style = 'background-image: url("../../images/fond_top.png"), url("../../images/fond_repeat.png");'

import '../style/other/general.css';
import '../style/other/font-size.css';

import '../style/nos_nageurs.css';

// ADD VISITE
import '../scripts/other/add_visite.js';

////////////////////
// SCRIPT SECTION //
////////////////////
function genererCaseNageur(id, nom, prenom, photo, anniversaire) {
    let img;
    if (photo == null || photo === "") img = "../images/default_profil.jpg";
    else img = `${photo}/${id}.jpeg`;

    // Vérification si c'est l'anniversaire aujourd'hui
    const today = new Date();
    const birthDate = new Date(anniversaire);
    const isBirthday = (today.getDate() === birthDate.getDate() && today.getMonth() === birthDate.getMonth());

    // Ajouter la classe 'birthday' si c'est l'anniversaire
    const birthdayClass = isBirthday ? ' birthday' : '';

    return `
    <div class="case-nageur${birthdayClass}">
        <a href="./profil_nageur?id=${id}"> 
            <div class="photo-container">
                <img class="photo-nageur" src="${img}">
                ${isBirthday ? '<img class="birthday-cake" src="../images/cake.png" alt="Joyeux Anniversaire!">' : ''}
            </div>
            <div class="nom-prenom">
                ${prenom} ${nom.substring(0, 1)}.
            </div>
        </a>
    </div>
    `;
}

/*
const cases_nageurs = document.querySelectorAll(".case-nageur");
cases_nageurs.forEach(case_nageur => {
    case_nageur.innerHTML = genererCaseNageur(1, "NOM", "Prenom", null);
});
*/

async function generateAllCard(annee) {
    await generateGroupeSection("Debutant", annee);
    await generateGroupeSection("Moyen", annee);
    await generateGroupeSection("Grand", annee);
}

async function generateGroupeSection(groupe, annee) {
    let data = null
    try {
        const response = await fetch(`/api/nageur/getAll/${groupe}/${annee}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }

    console.log(data)

    const section = document.getElementById(`${groupe}-section`);
    section.innerHTML = "";
    if (data.length>0) {
        for (let i = 0; i < data.length; i++) {
            section.innerHTML += genererCaseNageur(data[i]["id_nageur"], data[i]["nom"], data[i]["prenom"], data[i]["photo"], data[i]["date_naiss"])
        }   
    } else {
        section.innerHTML += '<p class="italic">Pas de donnée</p>'
    }

    return data
}

async function generateFirstTime() {
    let data = null
    try {
        const response = await fetch(`/api/groupe/getAllYear`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }

    const annee_selector = document.getElementById("annee-selector");
    for (let i = 0; i < data.length; i++) {
        annee_selector.innerHTML += `<option class="option" value="${data[i].split("-")[0]}">${data[i]}</option>`;
    }
    generateEventHandler();
    generateAllCard(data[0].split("-")[0]);
    generateLoisirSection();
}

function generateEventHandler() {
    const monSelect = document.getElementById('annee-selector');

    monSelect.addEventListener('change', function() {
        generateAllCard(monSelect.value);
        generateLoisirSection();
    });
}

async function generateLoisirSection() {
    const monSelect = document.getElementById('annee-selector');
    const annee = monSelect.value

    let data = null
    try {
        const response = await fetch(`/api/loisir/getPhotos/${annee}`);
        data = await response.json();
    } catch (erreur) {
        console.error('Erreur lors de la requête fetch :', erreur);
    }

    const loisir_section = document.getElementById("section-loisir")
    if (data.length!=0) {
        loisir_section.setAttribute("style", "display:block");

        let res = `<h2>Groupe Loisir</h2>
            <div id="carouselExampleIndicators" class="carousel slide">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>`

                    for (let i = 1; i < data.length; i++){
                        res += `<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="${i}" aria-label="Slide ${i+1}"></button>`
                    }
                res += `
                </div>
                <div class="carousel-inner">`
                    for (let i = 0; i < data.length; i++){
                        if (i == 0) {
                            res += `<div class="carousel-item active"><img src="${data[i].text}" class="d-block carousel-image" alt="${data[i].text}"></div>`
                        } else {
                            res += `<div class="carousel-item"><img src="${data[i].text}" class="d-block carousel-image" alt="${data[i].text}"></div>`
                        }
                        
                    }
                res += `
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>`
        
        loisir_section.innerHTML = res;
    } else {
        loisir_section.setAttribute("style", "display:none");
    }
}

//PREMIER INITIALISATION DE LA PAGE
generateFirstTime()
