## Taches planifié

Utilisez la commande suivante pour ouvrir la liste des tâches planifiées existantes pour l'utilisateur courant :
```
crontab -e
```

Si vous utilisez cette commande pour la première fois, on vous demandera de choisir un éditeur de texte par défaut pour éditer la tâche cron. Choisissez l'éditeur que vous préférez (par exemple, nano ou vim).

Dans l'éditeur de texte, ajoutez une ligne pour planifier l'exécution de votre script toutes les 24 heures. La syntaxe de base pour une tâche cron est la suivante :
```
Minute Heure Jour Mois JourDeLaSemaine Commande
```

Pour exécuter votre script toutes les 24 heures, vous pourriez utiliser quelque chose comme :
```
0 0 * * * /chemin/vers/votre/script.sh
```

Cela exécutera votre script tous les jours à minuit (00:00).

Enregistrez les modifications et fermez l'éditeur.

Assurez-vous que le script a les permissions d'exécution nécessaires en utilisant la commande suivante :
```
chmod +x /chemin/vers/votre/script.sh
```

Votre script sera maintenant exécuté automatiquement toutes les 24 heures grâce à la tâche cron que vous avez configurée.