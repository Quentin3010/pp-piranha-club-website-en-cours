#!/bin/bash

# Vérifier si les paramètres ont été passés
if [ $# -ne 3 ]; then
    echo "Utilisation : $0 <utilisateur_mysql> <mot_de_passe_mysql> <nom_base_de_donnees>"
    exit 1
fi

# Paramètres passés en arguments
db_user="$1"
db_password="$2"
db_name="$3"
backup_dir="/var/www/html/PiranhaClub/bdd/back-up"
max_backups=5

# Créer un répertoire de sauvegarde s'il n'existe pas
mkdir -p $backup_dir

# Nom de fichier avec la date et l'heure actuelles
backup_file="$backup_dir/backup-$(date +\%Y\%m\%d-\%H\%M\%S).sql"

# Créer la sauvegarde MySQL
mysqldump -u $db_user -p$db_password $db_name > $backup_file

# Supprimer les sauvegardes les plus anciennes si nécessaire
while [ $(ls $backup_dir -N1 | wc -l) -gt $max_backups ]; do
    oldest_backup=$(ls $backup_dir -N1 | sort | head -n 1)
    rm "$backup_dir/$oldest_backup"
done

